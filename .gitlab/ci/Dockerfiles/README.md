# CI - Docker images of prerequisites

## Docker images

Docker images of prerequisites usign by **Gitlab CI**:
- [Ubuntu 22.04, PHP 8.1 (Ubuntu packages), Composer, Symfony-CLI, Xdebug and Make](php-8.1_composer/)
- [Ubuntu 22.04, PHP 8.2 (ppa:ondrej/php), Composer, Symfony-CLI, Xdebug and Make](php-8.2_composer/)
- [Ubuntu 24.04, PHP 8.3 (Ubuntu packages), Composer, Symfony-CLI, Xdebug and Make](php-8.3_composer/)
- [Ubuntu 24.04, PHP 8.4 (ppa:ondrej/php), Composer, Symfony-CLI, Xdebug and Make](php-8.4_composer/)

## How to build and push all images to Gitlab container registry?

### Configuration

see: 
- [`config_all-images.sh`](config_all-images.sh)
- [`config_gitlab-repository.sh`](config_gitlab-repository.sh)


### Build all images

```bash
cd .gitlab/ci/Dockerfiles
./10_build_all-images.sh
```

### Push all images to Gitlab container registry

```bash
cd .gitlab/ci/Dockerfiles
./30_push_all-images.sh
```
