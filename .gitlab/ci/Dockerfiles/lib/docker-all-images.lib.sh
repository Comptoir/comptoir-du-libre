#!/usr/bin/env bash
# shellcheck disable=SC2317  # Don't warn about unreachable commands in this file
# shellcheck disable=SC1091  # Don't warn about "not following: (...) no such file" in this file
##############################################################################################

# Check all images configuration
check_all_images_configuration () {
    echo  ""
    echo  "-------------- CHECK CONFIGURATION -------------------------"
    for DOCKERFILE_DIRECTORY in "${DOCKERFILE_DIRECTORIES[@]}"
    do
         (    # executed in a subshell
              echo  "---> Check [ ${DOCKERFILE_DIRECTORY} ] configuration"
              cd "${DOCKERFILE_DIRECTORY}" || exit 255
              source "config_docker-image.sh"
              if [ "${DOCKERFILE_DIRECTORY}" != "${DOCKER_IMAGE_NAME}" ]; then
                  echo "EROOR :"
                  echo "[ ${DOCKERFILE_DIRECTORY} ] directory and [ ${DOCKER_IMAGE_NAME} ] image are not identicatl."
                  false
              fi
         )
    done
}

# Remove all images
remove_all_images () {
    echo  ""
    echo  "-------------- REMOVE ALL IMAGES -------------------------"
    for DOCKERFILE_DIRECTORY in "${DOCKERFILE_DIRECTORIES[@]}"
    do
        echo  "---> Remove [ ${DOCKERFILE_DIRECTORY} ] image"
        cd "${DOCKERFILE_DIRECTORY}" || exit 255
        source "config_docker-image.sh"
        remove_docker-image && cd .. || exit 255
    done
}


# Build all images
build_all_images () {
    echo  ""
    echo  "-------------- BUILD ALL IMAGES -------------------------"
    for DOCKERFILE_DIRECTORY in "${DOCKERFILE_DIRECTORIES[@]}"
    do
        echo  ""
        echo  "-------------> Build [ ${DOCKERFILE_DIRECTORY} ] image"
        cd "${DOCKERFILE_DIRECTORY}" || exit 255
        source "config_docker-image.sh"
        build_docker-image_without-cache && cd .. || exit 255
    done
}


# Push all images
push_all_imagess () {
    echo  ""
    echo  "-------------- PUSH ALL IMAGES -------------------------"
    for DOCKERFILE_DIRECTORY in "${DOCKERFILE_DIRECTORIES[@]}"
    do
        echo  ""
        echo  "-------------> Push [ ${DOCKERFILE_DIRECTORY} ] image"
        cd "${DOCKERFILE_DIRECTORY}" || exit 255
        source "config_docker-image.sh"
        push_docker-image && cd .. || exit 255
    done
}

resume_build () {
    echo  ""
    echo  "-------------- RESUME BUILD -------------------------"
    for DOCKERFILE_DIRECTORY in "${DOCKERFILE_DIRECTORIES[@]}"
    do
        docker images | grep "${DOCKERFILE_DIRECTORY}" || (
            echo "ERROR: Image [ ${DOCKERFILE_DIRECTORY} ] does not exist" && false
        )
    done
}


