#!/usr/bin/env bash
# shellcheck disable=SC2317  # Don't warn about unreachable commands in this file
##############################################################################################

# Function to handle errors
handle_error() {
    echo "----------------------------------------------------------"
    echo "----> FAIL : an ERROR occurred"
    exit 1
}

# Function to handle script exit
handle_exit() {
    echo "----------------------------------------------------------"
    echo "----> EXIT : script is finished or script has been interrupted by user or by an error"
    echo ""
    exit 0
}

# Buil Docker image with cache (useful for debugging)
build_docker-image_with-cache () {
# docker images | grep "${DOCKER_IMAGE_NAME}"
  docker build --progress plain -t "${GITLAB_URI}/${GITLAB_REPOSITORY}/${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_VERSION}" .
  docker images | grep "${DOCKER_IMAGE_NAME}"
}

# Build Docker image without cache (must be used to push a new Docker image or update Docker image)
build_docker-image_without-cache () {
  docker pull "${DOCKER_IMAGE_FROM}"
  docker image remove "${GITLAB_URI}/${GITLAB_REPOSITORY}/${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_VERSION}"  || true
  docker build --no-cache --progress plain -t "${GITLAB_URI}/${GITLAB_REPOSITORY}/${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_VERSION}" .
  docker images | grep "${DOCKER_IMAGE_NAME}"
}

# Remvoe Docker image
remove_docker-image () {
  docker image remove "${GITLAB_URI}/${GITLAB_REPOSITORY}/${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_VERSION}"  || true
}

# Login to Gitlab and push image to Gitlab container registry
push_docker-image () {
  docker login "${GITLAB_URI}"
  docker push "${GITLAB_URI}/${GITLAB_REPOSITORY}/${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_VERSION}"
}