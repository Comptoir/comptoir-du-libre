#!/usr/bin/env bash
# shellcheck disable=SC1091  # Don't warn about "not following: (...) no such file" in this file
##############################################################################################
set   -o errexit    # enable exit if one of command fails
set   -o pipefail   # enable exit if one of pipe command fails
set   -o nounset    # enable exit if any of variables is not set
# set   -o xtrace     # all executed commands are printed to the terminal
##############################################################################################
source config_all-images.sh
source config_gitlab-repository.sh
source lib/docker-all-images.lib.sh
source lib/docker-image.lib.sh
##############################################################################################
trap   handle_error ERR  # Set the error handler function
trap   handle_exit  EXIT # Set the exit handler function
##############################################################################################
(check_all_images_configuration) # executed in a subshell
(remove_all_images)              # executed in a subshell
(build_all_images)               # executed in a subshell
(resume_build)                   # executed in a subshell
##############################################################################################
exit 0
