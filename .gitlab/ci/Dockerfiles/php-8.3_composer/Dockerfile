FROM ubuntu:24.04

# System prerequisites
RUN apt-get update                           && \
    DEBIAN_FRONTEND=noninteractive              \
    apt-get -y --no-install-recommends          \
            install git                         \
                    make                        \
                    gpg-agent                   \
                    software-properties-common  \
                    ca-certificates             \
                    curl                        \
                    zip                         \
                    unzip                       \
                    gpg                      && \
    rm -rf /var/lib/apt/lists/*

# Allow to use another version of PHP than Ubuntu PHP version
# RUN add-apt-repository ppa:ondrej/php

# Webapp prerequisites - Install PHP and PHP extensions
RUN apt-get update                           && \
    DEBIAN_FRONTEND=noninteractive              \
    apt-get -y --no-install-recommends          \
            install php8.3                      \
                    php8.3-bcmath               \
                    php8.3-curl                 \
                    php8.3-intl                 \
                    php8.3-mbstring             \
                    php8.3-pgsql                \
                    php8.3-sqlite3              \
                    php8.3-xdebug               \
                    php8.3-xml                  \
                    php8.3-zip               && \
    rm -rf /var/lib/apt/lists/*
    # PHP Xdebug  extension is mandatory to use Infection tool

# Webapp prerequisites - Install COMPOSER
RUN curl -sSL https://getcomposer.org/installer --output composer-setup.php     && \
    php -r "if (hash_file('sha384', 'composer-setup.php') === 'dac665fdc30fdd8ec78b38b9800061b4150413ff2e3b6f88543c636f7cd84f6db9189d43a81e5503cda447da73c7e5b6') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" && \
    php composer-setup.php                                                      && \
    rm -v composer-setup.php                                                    && \
    chmod +x composer.phar                                                      && \
    mv -v composer.phar /usr/local/bin/composer                                 && \
    composer  -V

# QA prerequisites - Install Symfony CLI
SHELL ["/bin/bash", "-o", "pipefail", "-c"]
RUN curl -sS https://get.symfony.com/cli/installer | /bin/bash  && \
    mv /root/.symfony5/bin/symfony /usr/local/bin/symfony       && \
    symfony -V

# Caching of all PHP dependencies declared in composer.json files
WORKDIR "/php/"
RUN REPO=https://gitlab.adullact.net/Comptoir/comptoir-du-libre.git                            && \
    pwd                                                                                        && \
    git clone ${REPO}  source-code                                                             && \
    cd /php/source-code/webapp/                                                                && \
    composer install --no-progress                                                                \
                     --no-interaction                                                             \
                     --no-scripts                                                              && \
    cd /php/source-code/bin/tools_via_composer/                                                && \
    composer install --no-progress                                                                \
                     --no-interaction                                                             \
                     --no-scripts                                                              && \
    mkdir -p /php/composer-cache/tools/                                                        && \
    mkdir -p /php/composer-cache/webapp/                                                       && \
    cp    /php/source-code/bin/tools_via_composer/composer.json   /php/composer-cache/tools/   && \
    cp    /php/source-code/bin/tools_via_composer/composer.lock   /php/composer-cache/tools/   && \
    cp -r /php/source-code/bin/tools_via_composer/vendor          /php/composer-cache/tools/   && \
    cd /php/composer-cache/tools/                                                              && \
    tar -czf vendor.tar.gz      vendor/                                                        && \
    cp -r /php/source-code/webapp/composer.json                   /php/composer-cache/webapp/  && \
    cp -r /php/source-code/webapp/composer.lock                   /php/composer-cache/webapp/  && \
    cp -r /php/source-code/webapp/vendor                          /php/composer-cache/webapp/  && \
    cd /php/composer-cache/webapp/                                                             && \
    tar -czf vendor.tar.gz      vendor/                                                        && \
    rm -rf /php/composer-cache/webapp/vendor/                                                  && \
    rm -rf /php/composer-cache/tools/vendor/                                                   && \
    rm -rf /php/source-code/

# Display software versions
RUN echo "-------------------"  >>  /php/software-infos.txt   && \
    cat /etc/os-release         >>  /php/software-infos.txt   && \
    echo "-------------------"  >>  /php/software-infos.txt   && \
    php --version               >>  /php/software-infos.txt   && \
    echo "-------------------"  >>  /php/software-infos.txt   && \
    php -m                      >>  /php/software-infos.txt   && \
    echo "-------------------"  >>  /php/software-infos.txt   && \
    composer  -V                >>  /php/software-infos.txt   && \
    echo "-------------------"  >>  /php/software-infos.txt   && \
    symfony -V                  >>  /php/software-infos.txt   && \
    echo "-------------------"  >>   /php/software-infos.txt  && \
    cat /php/software-infos.txt

#        # DEBUG
#        RUN ls -l /php/                                               && \
#            ls -l /php/composer-cache/                                && \
#            ls -l /php/composer-cache/webapp                          && \
#            cd /php/composer-cache/webapp && tar -xzf vendor.tar.gz   && \
#            ls -l /php/composer-cache/webapp                          && \
#            ls -l /php/composer-cache/webapp/vendor                   && \
#            ls -l /php/composer-cache/tools                           && \
#            cd /php/composer-cache/tools  && tar -xzf vendor.tar.gz   && \
#            ls -l /php/composer-cache/tools                           && \
#            ls -l /php/composer-cache/tools/vendor
#
#        # DEBUG Check cache
#        RUN du -hs  /root/.cache/composer/                            && \
#            du -hs  /php/composer-cache/*

# Set Docker LABEL
LABEL version="1.0.0"  \
      description="CI Prerequisites : Ubuntu 24.04, PHP 8.3 (Ubuntu packages), Composer, Symfony-CLI, Xdebug and Make"
