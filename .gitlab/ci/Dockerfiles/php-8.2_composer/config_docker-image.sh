#!/usr/bin/env bash
# shellcheck disable=SC2034  # Don't warn about unused variables in this file
##############################################################################################
DOCKER_IMAGE_FROM="ubuntu:22.04"
DOCKER_IMAGE_NAME="php-8.2_composer"
DOCKER_IMAGE_VERSION="1.0.0" # must be identical to LABEL.version in Dockerfile
##############################################################################################
