#!/usr/bin/env bash
# shellcheck disable=SC2034  # Don't warn about unused variables in this file
##############################################################################################
DOCKERFILE_DIRECTORIES=(
    'php-8.1_composer'
    'php-8.2_composer'
    'php-8.3_composer'
    'php-8.4_composer'
)
##############################################################################################
