#!/usr/bin/env bash
# shellcheck disable=SC1091  # Don't warn about "not following: (...) no such file" in this file
##############################################################################################
set   -o errexit    # enable exit if one of command fails
set   -o pipefail   # enable exit if one of pipe command fails
set   -o nounset    # enable exit if any of variables is not set
# set   -o xtrace     # all executed commands are printed to the terminal
##############################################################################################
source config_gitlab-repository.sh
##############################################################################################
docker login "${GITLAB_URI}"
##############################################################################################
exit 0
