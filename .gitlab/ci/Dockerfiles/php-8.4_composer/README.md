# CI Prerequisites : Ubuntu 24.04, PHP 8.4 (ppa:ondrej/php), Composer, Symfony-CLI, Xdebug and Make

**Table of contents**

[Toc]

-------------------------------------------------------

## CI Prerequisites

Docker image of CI Prerequisites : 
- Ubuntu 24.04
- PHP **8.4** (`ppa:ondrej/php`)
- Composer
- Symfony-CLI
- Xdebug 
- Make

-------------------------------------------------------

## To update Docker image

### Edit Dockerfile

Edit [Dockerfile](Dockerfile) according to your needs
and in addition you must to change in this file `LABEL version="1.0.0"`
following [semantic versioning](http://semver.org/) recommendations:

 ```shell script
 MAJOR.MINOR.PATCH
    # MAJOR ---> a breaking change (incompatible API changes)
    # MINOR ---> add a new feature
    # PATCH ---> fix a bug
```

```dockerfile
# Set Docker LABEL and display software versions
LABEL version="1.0.0"  \
      description="CI Prerequisites : Ubuntu 24.04, PHP 8.4 (ppa:ondrej/php), Composer, Symfony-CLI, Xdebug and Make"
```


### Build Docker image and push to container registry

Build new Docker image and push to [Gitlab container registry](https://gitlab.adullact.net/Comptoir/comptoir-du-libre/container_registry)

#### Configure variables in particular DOCKER_IMAGE_VERSION

```bash
# Configure variables
# in particular DOCKER_IMAGE_VERSION
# which must be identical to LABEL.version in Dockerfile
GITLAB_URI="gitlab.adullact.net:4567"
GITLAB_REPOSITORY="comptoir/comptoir-du-libre"
DOCKER_IMAGE_NAME="php-8.4_composer"
DOCKER_IMAGE_VERSION="v1.0.0" # must be identical to LABEL.version in Dockerfile
```

#### Build new Docker image (option 1: without docker cache)

Must be used to push a new Docker image (without Docker cache):
```bash
# Build new Docker image
docker images | grep "${DOCKER_IMAGE_NAME}"
docker build --no-cache --progress plain -t "${GITLAB_URI}/${GITLAB_REPOSITORY}/${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_VERSION}" .
docker images | grep "${DOCKER_IMAGE_NAME}"
```

#### Build new Docker image (option 2: with docker cache)

Useful for debugging (with Docker cache):
```bash
# Build new Docker image
docker build --progress plain -t "${GITLAB_URI}/${GITLAB_REPOSITORY}/${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_VERSION}" .
docker images | grep "${DOCKER_IMAGE_NAME}"
```

#### Push to Gitlab container registry

```bash
# Login to Gitlab
docker login "${GITLAB_URI}"

# Push to Gitlab container registry
docker push "${GITLAB_URI}/${GITLAB_REPOSITORY}/${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_VERSION}"

# Logout to remove Gitlab credentials from $HOME/.docker/config.json file
docker logout "${GITLAB_URI}"
```
