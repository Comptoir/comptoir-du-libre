app_health_check
----------------

- Path: /health-check
- Path Regex: {^/health\-check$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|HEAD
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\HealthCheckController::healthCheck
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_admin_config_phpinfo
------------------------

- Path: /admin/configuration/phpinfo
- Path Regex: {^/admin/configuration/phpinfo$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\HomeController::displayPhpInfo
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_admin_config_send_testing_email
-----------------------------------

- Path: /admin/configuration/check-sending-email
- Path Regex: {^/admin/configuration/check\-sending\-email$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\HomeController::checkSendingEmail
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_home
--------

- Path: /
- Path Regex: {^/$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|HEAD
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\HomeController::homePage
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_home_i18n
-------------

- Path: /{_locale}/
- Path Regex: {^/(?P<_locale>en|fr)/$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|HEAD
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\HomeController::i18nHomePage
- Requirements: 
    - `_locale`: en|fr
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_page_accessibility
----------------------

- Path: /{_locale}/pages/accessibility
- Path Regex: {^/(?P<_locale>[^/]++)/pages/accessibility$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|HEAD
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\HomeController::accessibilityPage
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_page_legal
--------------

- Path: /{_locale}/pages/legal
- Path Regex: {^/(?P<_locale>[^/]++)/pages/legal$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|HEAD
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\HomeController::legalPage
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_page_about
--------------

- Path: /{_locale}/pages/about
- Path Regex: {^/(?P<_locale>[^/]++)/pages/about$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|HEAD
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\HomeController::aboutPage
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_page_opendata
-----------------

- Path: /{_locale}/pages/opendata
- Path Regex: {^/(?P<_locale>[^/]++)/pages/opendata$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|HEAD
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\HomeController::opendataPage
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_page_contact
----------------

- Path: /{_locale}/pages/contact
- Path Regex: {^/(?P<_locale>[^/]++)/pages/contact$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|HEAD
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\HomeController::contactPage
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_anonymous_licenses_display_all_allow_to_change_locale_in_url.fr
-------------------------------------------------------------------

- Path: /{_locale}/licenses/
- Path Regex: {^/fr/licenses/$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|HEAD
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_canonical_route`: app_anonymous_licenses_display_all_allow_to_change_locale_in_url
    - `_controller`: App\Controller\Licenses\LicensesDisplayController::allLicensesPageAllowToChangeLocaleInUrl
    - `_locale`: fr
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_anonymous_licenses_display_all_allow_to_change_locale_in_url.en
-------------------------------------------------------------------

- Path: /{_locale}/licences/
- Path Regex: {^/en/licences/$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|HEAD
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_canonical_route`: app_anonymous_licenses_display_all_allow_to_change_locale_in_url
    - `_controller`: App\Controller\Licenses\LicensesDisplayController::allLicensesPageAllowToChangeLocaleInUrl
    - `_locale`: en
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_anonymous_licenses_display_all_fix_url_typo.en
--------------------------------------------------

- Path: /{_locale}/license/
- Path Regex: {^/en/license/$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|HEAD
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_canonical_route`: app_anonymous_licenses_display_all_fix_url_typo
    - `_controller`: App\Controller\Licenses\LicensesDisplayController::allLicensesPageFixUrlTypo
    - `_locale`: en
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_anonymous_licenses_display_all_fix_url_typo.fr
--------------------------------------------------

- Path: /{_locale}/licence/
- Path Regex: {^/fr/licence/$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|HEAD
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_canonical_route`: app_anonymous_licenses_display_all_fix_url_typo
    - `_controller`: App\Controller\Licenses\LicensesDisplayController::allLicensesPageFixUrlTypo
    - `_locale`: fr
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_anonymous_licenses_display_all.en
-------------------------------------

- Path: /{_locale}/licenses/
- Path Regex: {^/en/licenses/$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|HEAD
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_canonical_route`: app_anonymous_licenses_display_all
    - `_controller`: App\Controller\Licenses\LicensesDisplayController::displayAllLicenses
    - `_locale`: en
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_anonymous_licenses_display_all.fr
-------------------------------------

- Path: /{_locale}/licences/
- Path Regex: {^/fr/licences/$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|HEAD
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_canonical_route`: app_anonymous_licenses_display_all
    - `_controller`: App\Controller\Licenses\LicensesDisplayController::displayAllLicenses
    - `_locale`: fr
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_anonymous_licenses_display_one_allow_to_change_locale_in_url.fr
-------------------------------------------------------------------

- Path: /{_locale}/license/{slug}/
- Path Regex: {^/fr/license/(?P<slug>[^/]++)/$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|HEAD
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_canonical_route`: app_anonymous_licenses_display_one_allow_to_change_locale_in_url
    - `_controller`: App\Controller\Licenses\LicensesDisplayController::displayLicensePageAllowToChangeLocaleInUrl
    - `_locale`: fr
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_anonymous_licenses_display_one_allow_to_change_locale_in_url.en
-------------------------------------------------------------------

- Path: /{_locale}/licence/{slug}/
- Path Regex: {^/en/licence/(?P<slug>[^/]++)/$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|HEAD
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_canonical_route`: app_anonymous_licenses_display_one_allow_to_change_locale_in_url
    - `_controller`: App\Controller\Licenses\LicensesDisplayController::displayLicensePageAllowToChangeLocaleInUrl
    - `_locale`: en
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_anonymous_licenses_display_one_license.en
---------------------------------------------

- Path: /{_locale}/license/{slug}/
- Path Regex: {^/en/license/(?P<slug>[^/]++)/$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|HEAD
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_canonical_route`: app_anonymous_licenses_display_one_license
    - `_controller`: App\Controller\Licenses\LicensesDisplayController::displayLicense
    - `_locale`: en
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_anonymous_licenses_display_one_license.fr
---------------------------------------------

- Path: /{_locale}/licence/{slug}/
- Path Regex: {^/fr/licence/(?P<slug>[^/]++)/$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|HEAD
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_canonical_route`: app_anonymous_licenses_display_one_license
    - `_controller`: App\Controller\Licenses\LicensesDisplayController::displayLicense
    - `_locale`: fr
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_legacy_all_software_url
---------------------------

- Path: /softwares
- Path Regex: {^/softwares$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|HEAD
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\OldUrlsController::legacyUrlDisplayAllSoftware
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_legacy_all_software_i18n_url
--------------------------------

- Path: /{_locale}/softwares
- Path Regex: {^/(?P<_locale>[^/]++)/softwares$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|HEAD
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\OldUrlsController::legacyI18nUrlDisplayAllSoftware
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_legacy_software_url
-----------------------

- Path: /softwares/{old_comptoir_id}
- Path Regex: {^/softwares/(?P<old_comptoir_id>[^/]++)$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|HEAD
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\OldUrlsController::legacyUrlDisplayOneSoftware
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_legacy_software_i18_url
---------------------------

- Path: /{_locale}/softwares/{old_comptoir_id}
- Path Regex: {^/(?P<_locale>en|fr)/softwares/(?P<old_comptoir_id>[^/]++)$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|HEAD
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\OldUrlsController::legacyI18nUrlDisplayOneSoftware
- Requirements: 
    - `_locale`: en|fr
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_anonymous_organization_display_one_organization.en
------------------------------------------------------

- Path: /{_locale}/organization/{id}/{slug}/
- Path Regex: {^/en/organization/(?P<id>[^/]++)/(?P<slug>[^/]++)/$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|HEAD
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_canonical_route`: app_anonymous_organization_display_one_organization
    - `_controller`: App\Controller\Organization\OrganizationDisplayController::displayOneOrganization
    - `_locale`: en
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_anonymous_organization_display_one_organization.fr
------------------------------------------------------

- Path: /{_locale}/organisation/{id}/{slug}/
- Path Regex: {^/fr/organisation/(?P<id>[^/]++)/(?P<slug>[^/]++)/$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|HEAD
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_canonical_route`: app_anonymous_organization_display_one_organization
    - `_controller`: App\Controller\Organization\OrganizationDisplayController::displayOneOrganization
    - `_locale`: fr
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_anonymous_organization_display_all_organization.en
------------------------------------------------------

- Path: /{_locale}/organizations/
- Path Regex: {^/en/organizations/$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|HEAD
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_canonical_route`: app_anonymous_organization_display_all_organization
    - `_controller`: App\Controller\Organization\OrganizationDisplayController::displayAllOrganization
    - `_locale`: en
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_anonymous_organization_display_all_organization.fr
------------------------------------------------------

- Path: /{_locale}/organisations/
- Path Regex: {^/fr/organisations/$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|HEAD
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_canonical_route`: app_anonymous_organization_display_all_organization
    - `_controller`: App\Controller\Organization\OrganizationDisplayController::displayAllOrganization
    - `_locale`: fr
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_anonymous_organization_display_all_org_company.en
-----------------------------------------------------

- Path: /{_locale}/organizations/companies/
- Path Regex: {^/en/organizations/companies/$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|HEAD
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_canonical_route`: app_anonymous_organization_display_all_org_company
    - `_controller`: App\Controller\Organization\OrganizationDisplayController::displayAllCompanies
    - `_locale`: en
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_anonymous_organization_display_all_org_company.fr
-----------------------------------------------------

- Path: /{_locale}/organisations/entreprises/
- Path Regex: {^/fr/organisations/entreprises/$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|HEAD
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_canonical_route`: app_anonymous_organization_display_all_org_company
    - `_controller`: App\Controller\Organization\OrganizationDisplayController::displayAllCompanies
    - `_locale`: fr
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_anonymous_organization_display_all_org_public_sector.en
-----------------------------------------------------------

- Path: /{_locale}/organizations/public-sector/
- Path Regex: {^/en/organizations/public\-sector/$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|HEAD
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_canonical_route`: app_anonymous_organization_display_all_org_public_sector
    - `_controller`: App\Controller\Organization\OrganizationDisplayController::displayAllPublicSector
    - `_locale`: en
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_anonymous_organization_display_all_org_public_sector.fr
-----------------------------------------------------------

- Path: /{_locale}/organisations/administration/
- Path Regex: {^/fr/organisations/administration/$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|HEAD
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_canonical_route`: app_anonymous_organization_display_all_org_public_sector
    - `_controller`: App\Controller\Organization\OrganizationDisplayController::displayAllPublicSector
    - `_locale`: fr
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_anonymous_organization_display_all_org_no_profit.en
-------------------------------------------------------

- Path: /{_locale}/organizations/non-profit/
- Path Regex: {^/en/organizations/non\-profit/$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|HEAD
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_canonical_route`: app_anonymous_organization_display_all_org_no_profit
    - `_controller`: App\Controller\Organization\OrganizationDisplayController::displayAllNoProfitOrg
    - `_locale`: en
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_anonymous_organization_display_all_org_no_profit.fr
-------------------------------------------------------

- Path: /{_locale}/organisations/association/
- Path Regex: {^/fr/organisations/association/$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|HEAD
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_canonical_route`: app_anonymous_organization_display_all_org_no_profit
    - `_controller`: App\Controller\Organization\OrganizationDisplayController::displayAllNoProfitOrg
    - `_locale`: fr
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_sysamdin_migration_index
----------------------------

- Path: /sysamdin/migration/
- Path Regex: {^/sysamdin/migration/$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\RecupOldDataController::index
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_sysamdin_migration_make_cache_data_of_all_old_data
------------------------------------------------------

- Path: /sysamdin/migration/make_cache_data_of_all_old_data
- Path Regex: {^/sysamdin/migration/make_cache_data_of_all_old_data$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\RecupOldDataController::makeCacheOfAllOldData
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_sysamdin_migration_recup_old_data_of_tags
---------------------------------------------

- Path: /sysamdin/migration/recup_old_data_of_tags
- Path Regex: {^/sysamdin/migration/recup_old_data_of_tags$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\RecupOldDataController::importOldDataTagsFromCakePhpApi
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_sysamdin_migration_import_old_data_of_screenshots
-----------------------------------------------------

- Path: /sysamdin/migration/import_old_data_of_screenshots
- Path Regex: {^/sysamdin/migration/import_old_data_of_screenshots$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\RecupOldDataController::importOldDataScreenshotsFromCakePhpApi
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_sysamdin_migration_import_old_data_of_software_logo
-------------------------------------------------------

- Path: /sysamdin/migration/import_old_data_of_software_logo
- Path Regex: {^/sysamdin/migration/import_old_data_of_software_logo$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\RecupOldDataController::importSoftwareLogos
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_sysamdin_migration_recup_old_data_user_photos
-------------------------------------------------

- Path: /sysamdin/migration/recup_old_data_user_photos
- Path Regex: {^/sysamdin/migration/recup_old_data_user_photos$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\RecupOldDataController::getOldDataUserAvatar
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_sysamdin_migration_recup_old_data_of_software_logo
------------------------------------------------------

- Path: /sysamdin/migration/recup_old_data_of_software_logo
- Path Regex: {^/sysamdin/migration/recup_old_data_of_software_logo$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\RecupOldDataController::getOldDataSofwareLogos
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_sysamdin_migration_recup_old_data_of_screenshots
----------------------------------------------------

- Path: /sysamdin/migration/recup_old_data_of_screenshots
- Path Regex: {^/sysamdin/migration/recup_old_data_of_screenshots$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\RecupOldDataController::getOldDataScreenshotsFromCakePhpApi
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_sysamdin_migration_recup_old_data_of_all_software
-----------------------------------------------------

- Path: /sysamdin/migration/recup_old_data_of_all_software
- Path Regex: {^/sysamdin/migration/recup_old_data_of_all_software$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\RecupOldDataController::importOldDataSoftwareFromOpenData
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_sysamdin_migration_recup_old_data_users_accounts
----------------------------------------------------

- Path: /sysamdin/migration/recup_old_data_users_accounts
- Path Regex: {^/sysamdin/migration/recup_old_data_users_accounts$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\RecupOldDataController::getOldDataUsersAccount
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_sysamdin_migration_recup_old_data_organizations
---------------------------------------------------

- Path: /sysamdin/migration/recup_old_data_organizations
- Path Regex: {^/sysamdin/migration/recup_old_data_organizations$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\RecupOldDataController::getOldDataOrganizations
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_sysamdin_migration_import_old_data_of_user_photo
----------------------------------------------------

- Path: /sysamdin/migration/import_old_data_of_user_photo
- Path Regex: {^/sysamdin/migration/import_old_data_of_user_photo$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\RecupOldDataController::getImportOldUserPhoto
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_anonymous_software_display_one_software.en
----------------------------------------------

- Path: /{_locale}/software/{id}/{slug}/
- Path Regex: {^/en/software/(?P<id>[^/]++)/(?P<slug>[^/]++)/$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|HEAD
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_canonical_route`: app_anonymous_software_display_one_software
    - `_controller`: App\Controller\Software\SoftwareDisplayController::displayOneSoftware
    - `_locale`: en
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_anonymous_software_display_one_software.fr
----------------------------------------------

- Path: /{_locale}/logiciel/{id}/{slug}/
- Path Regex: {^/fr/logiciel/(?P<id>[^/]++)/(?P<slug>[^/]++)/$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|HEAD
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_canonical_route`: app_anonymous_software_display_one_software
    - `_controller`: App\Controller\Software\SoftwareDisplayController::displayOneSoftware
    - `_locale`: fr
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_anonymous_software_display_all_software.en
----------------------------------------------

- Path: /{_locale}/software/
- Path Regex: {^/en/software/$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|HEAD
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_canonical_route`: app_anonymous_software_display_all_software
    - `_controller`: App\Controller\Software\SoftwareDisplayController::displayAllSoftware
    - `_locale`: en
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_anonymous_software_display_all_software.fr
----------------------------------------------

- Path: /{_locale}/logiciels/
- Path Regex: {^/fr/logiciels/$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|HEAD
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_canonical_route`: app_anonymous_software_display_all_software
    - `_controller`: App\Controller\Software\SoftwareDisplayController::displayAllSoftware
    - `_locale`: fr
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_tags_display_all_fix_url_typo
---------------------------------

- Path: /{_locale}/tag/
- Path Regex: {^/(?P<_locale>[^/]++)/tag/$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|HEAD
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\Tags\TagsDisplayController::allTagsPageFixUrlTypo
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_tags_display_all
--------------------

- Path: /{_locale}/tags/
- Path Regex: {^/(?P<_locale>[^/]++)/tags/$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|HEAD
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\Tags\TagsDisplayController::displayAllTags
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_tags_display_one_tag
------------------------

- Path: /{_locale}/tag/{id}/{slug}/
- Path Regex: {^/(?P<_locale>[^/]++)/tag/(?P<id>[^/]++)/(?P<slug>[^/]++)/$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|HEAD
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\Tags\TagsDisplayController::displayOneTag
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_account_login
-----------------

- Path: /{_locale}/account/login
- Path Regex: {^/(?P<_locale>en|fr)/account/login$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|POST
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\User\LoginController::login
- Requirements: 
    - `_locale`: en|fr
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_account_logout
------------------

- Path: /{_locale}/account/logout
- Path Regex: {^/(?P<_locale>en|fr)/account/logout$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\User\LoginController::logout
- Requirements: 
    - `_locale`: en|fr
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_account_forgot_password_request
-----------------------------------

- Path: /{_locale}/account/forgot-password
- Path Regex: {^/(?P<_locale>en|fr)/account/forgot\-password$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|POST
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\User\LoginController::forgotPassword
- Requirements: 
    - `_locale`: en|fr
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_account_forgot_password_new-password
----------------------------------------

- Path: /{_locale}/account/forgot-password/t/{tokenKey}
- Path Regex: {^/(?P<_locale>en|fr)/account/forgot\-password/t/(?P<tokenKey>[^/]++)$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|POST
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\User\LoginController::forgotPasswordCheckToken
- Requirements: 
    - `_locale`: en|fr
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_account_signup_init
-----------------------

- Path: /{_locale}/account/signup
- Path Regex: {^/(?P<_locale>en|fr)/account/signup$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\User\SignupController::signupInit
- Requirements: 
    - `_locale`: en|fr
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_account_signup
------------------

- Path: /{_locale}/account/signup/t/{signupAntispamToken}
- Path Regex: {^/(?P<_locale>en|fr)/account/signup/t/(?P<signupAntispamToken>[^/]++)$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|POST
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\User\SignupController::signup
- Requirements: 
    - `_locale`: en|fr
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_user_account
----------------

- Path: /{_locale}/user/account
- Path Regex: {^/(?P<_locale>en|fr)/user/account$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\User\UserProfilController::userDisplayMyProfile
- Requirements: 
    - `_locale`: en|fr
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_user_account_change_password
--------------------------------

- Path: /{_locale}/user/account/password
- Path Regex: {^/(?P<_locale>en|fr)/user/account/password$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|POST
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\User\UserProfilController::userChangeMyPassowrd
- Requirements: 
    - `_locale`: en|fr
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


nelmio_security
---------------

- Path: /csp/report
- Path Regex: {^/csp/report$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: POST
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: nelmio_security.csp_reporter_controller::indexAction
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true

