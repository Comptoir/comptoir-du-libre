
# Ressources POST IT

## IMPORT 

### missing file extension
```
362 - CodiMD
    files/Softwares/CodiMD/avatar
    afdc568e-e8ea-4866-8c74-8a15388bfec0
    200 https://comptoir-du-libre.org/img/files/Softwares/CodiMD/avatar/afdc568e-e8ea-4866-8c74-8a15388bfec0
    DEBUG ----> Content Type
```

## Legal notice examples

### FR

- https://www.multi.coop/legal?locale=fr
- ...


## UI - Popin modale

Popin modale responsive et accessible
- https://www.kortic.com/popin-modale-accessible-et-responsive-compatible-mobile-et-desktop.html


## UI - HTML `<detail>` examples

- https://www.sitepoint.com/style-html-details-element/
- https://freefrontend.com/html-details-summary-css/
- https://www.alsacreations.com/article/lire/1335-html5-details-summary.html

## UI - Tag selector 

**Selectize** (need `JQuery`)
- https://selectize.dev
- https://github.com/selectize/selectize.js
- example: https://selectize.dev/docs/demos/tagging

**Select2** (need `JQuery`)
- https://select2.org
- https://github.com/select2/select2
- example: https://select2.org/tagging

**Choices.js** (vanilla `Javascript`)
- https://choices-js.github.io/Choices/
- https://github.com/Choices-js/Choices
- example: https://select2.org/tagging

**Select-a11y** (still maintained?)
- https://pidila.gitlab.io/scampi/documentation/select-a11y.html
- https://gitlab.com/pidila/select-a11y
- example: https://pidila.gitlab.io/select-a11y/

jQuery accessible autocomplete list (still maintained?)
- https://a11y.nicolas-hoffmann.net/autocomplete-list/
- https://github.com/nico3333fr/jquery-accessible-autocomplete-list-aria
- example:

alphagov / Accessible autocomplete
- https://github.com/alphagov/accessible-autocomplete
- example : https://alphagov.github.io/accessible-autocomplete/examples/

see also :
- [10 Best Multiple Select Plugins In JavaScript (2025 Update)](https://www.jqueryscript.net/blog/best-multiple-select.html#vanilla)


## UI - HTML tables

- https://datatables.net/
- https://github.com/DataTables/DataTables

## UI - Accessible modal window

Van11y accessible modal window system, using ARIA
- https://van11y.net/accessible-modal/
- https://github.com/nico3333fr/van11y-accessible-modal-window-aria
- example: https://van11y.net/downloads/modal/demo/

## UI - Accessible tab panel

Van11y accessible tabs panel system, using ARIA
- https://van11y.net/accessible-tab-panel/
- https://github.com/nico3333fr/van11y-accessible-tab-panel-aria
- example: https://van11y.net/downloads/tab-panel/demo/

Tabs Pattern by ARIA Authoring Practices Guide (APG)
- https://www.w3.org/WAI/ARIA/apg/patterns/tabs/
- example 1 : https://www.w3.org/WAI/ARIA/apg/patterns/tabs/examples/tabs-automatic/
- example 2 : https://www.w3.org/WAI/ARIA/apg/patterns/tabs/examples/tabs-manual/


## UI - Accessibility

ARIA Authoring Practices Guide (APG)
- https://www.w3.org/WAI/ARIA/apg/patterns/
  - https://www.w3.org/WAI/ARIA/apg/patterns/menubar/
  - https://www.w3.org/WAI/ARIA/apg/patterns/tabs/


## UI - CSS charts

- Library: https://chartscss.org/
- https://css-tricks.com/css-charts-grid-custom-properties/

Warning:
- use the `style` attribute instead of the `data` attribute to manage data values
- cannot be used with current CSP rules



## Bootstrap

- https://getbootstrap.com/docs/5.3/components/scrollspy/
- 

https://getbootstrap.com/docs/5.3/examples/
- https://getbootstrap.com/docs/5.3/examples/features/
- https://getbootstrap.com/docs/5.3/examples/album/
- https://getbootstrap.com/docs/5.3/examples/footers/
- https://getbootstrap.com/docs/5.3/examples/headers/