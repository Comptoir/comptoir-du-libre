# CHANGELOG

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

--------------------------------


## 3.0.0-alpha.0.1.0     (unreleased)

### Added

### Changed

#### Breaking change

### Fixed

### Dependancies

### Security


--------------------------------

## Template

```markdown

## <major>.<minor>.patch_DEV     (unreleased)

### Added

### Changed

#### Breaking change

### Fixed

### Dependancies

### Security


<details>
<summary>

Update `symfony/*` (v6.x.x => v6.x.x) : 0 installs, xx updates, 0 removals

see: https://symfony.com/blog/

</summary>

Lock file operations: 0 installs, xx updates, 0 removals
- Upgrading ...

</details>


--------------------------------

```
