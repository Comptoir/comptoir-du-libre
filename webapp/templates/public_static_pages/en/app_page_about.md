# About Comptoir du Libre

The **Comptoir du Libre** lists the **free software**s useful 
to the public services as well as their users and providers.

## Collaborative platform

As a collaborative platform, you can easily **share** your experiences, 
**find** your tools, and **identify** your partners.
