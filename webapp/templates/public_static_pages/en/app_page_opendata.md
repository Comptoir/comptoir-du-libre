# Open data

A part of <strong lang="fr">Comptoir du Libre</strong> are published in open data.

## Comptoir du Libre open data

Currently, all free software present on <em lang="fr">Comptoir du Libre</em> website
are available in a JSON format export, with for each software:

- license,
- name of software,
- source code repository,
- website,
- external resources (Wikipedia, Wikidata, Framalibre and SILL)
- declared providers.

You can consult this open data and associated documentation
on [data.gouv.fr](https://www.data.gouv.fr/fr/datasets/logiciels-libres-sur-le-comptoir-du-libre-org/).
