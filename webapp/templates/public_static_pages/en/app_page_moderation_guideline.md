## Moderation guideline

## Criteria for adding a software

The criteria for adding software to the _Comptoir du libre_ are :
- the **license** must be one of the [free licenses](https://opensource.org/licenses) approved by the OSI.
- the **source code** must be available in a public repository of a software forge.

**TODO**
> - software with several licenses
> - software with multiple repositories
> - minimum quality to add a project

### Source code

The software source code must be published :

- under one of the [free licenses](https://opensource.org/licenses) approved by the OSI.
- the license file must be present at the root of the project (e.g. in a `LICENSE` file)
- without any delay between the publication of the source code and that of the executable files.
- in a public repository without authentication.

### Repository URL

In software addition form, criteria to be taken into account
concerning **URL** of **source code** repository field:

- This field is mandatory.
- The URL to be provided is that of the publicly accessible repository.
- This URL must correspond to the software source code.
- This URL must allow a `git clone` to be made.
  (or an equivalent command for other source code managers).
