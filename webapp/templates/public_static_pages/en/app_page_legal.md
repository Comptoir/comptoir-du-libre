# Legal

## Editor

[ADULLACT](https://adullact.org/) <br>
5 rue du plan du palais <br>
34000 Montpellier, FRANCE


## Publication director

_Mathieu FAURE_, General Director of ADULLACT


## Hosting

**OVH**  <br>
2 rue Kellermann <br>
59100 Roubaix, FRANCE
