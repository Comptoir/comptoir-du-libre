# Déclaration d'accessibilité

Cette déclaration d’accessibilité s’applique <br>
au site web **Comptoir du Libre** : `https://comptoir-du-libre.org`



## État de conformité

Le site web **Comptoir du Libre** est **non conforme** 
avec le <abbr title="Référentiel Général d'Accessibilité des Administrations">RGAA</abbr></span>.

Le site web n’a encore pas été audité.


## Établissement de cette déclaration d’accessibilité

Cette déclaration a été établie le 18 février 2025.


## Résultat des tests

En l’absence d’audit de conformité il n’y a pas de résultats de tests.


## Contenus non accessibles

### Non conformité

En l’absence d’audit tous les contenus seront considérés comme non accessibles par hypothèse.

### Dérogations pour charge disproportionnée

En l’absence d’audit aucune dérogation n’a été établie.

### Contenus non soumis à l’obligation d’accessibilité

En l’absence d’audit aucun contenu n’a été identifié comme n’entrant pas dans le champ de la législation applicable.

### Agents utilisateurs, technologies d’assistance et outils utilisés pour vérifier l’accessibilité

En l’absence d’audit aucun agent utilisateur et aucune technologie d’assistance n’ont été utilisés.

### Pages du site ayant fait l’objet de la vérification de conformité

En l’absence d’audit aucune page n’a fait l’objet de la vérification de conformité.


## Amélioration et contact

Si vous n’arrivez pas à accéder à un contenu ou à un service,
vous pouvez [contacter le responsable du *Comptoir du Libre*](/fr/pages/contact)
pour être orienté vers une alternative accessible ou obtenir le contenu sous une autre forme.


## Voie de recours

Cette procédure est à utiliser dans le cas suivant :
vous avez signalé au responsable du site internet un défaut d’accessibilité
qui vous empêche d’accéder à un contenu ou à un des services du portail
et vous n’avez pas obtenu de réponse satisfaisante.

Vous pouvez :

- Écrire un message au [Défenseur des droits](https://formulaire.defenseurdesdroits.fr/)
- Contacter le [délégué du Défenseur des droits dans votre région](https://www.defenseurdesdroits.fr/saisir/delegues)
- Envoyer un courrier par la poste (gratuit, ne pas mettre de timbre) : <br>
  Défenseur des droits <br>
  Libre réponse 71120 <br>
  75342 Paris CEDEX 07