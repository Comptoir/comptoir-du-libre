# À Propos du Comptoir du Libre

Le **Comptoir du Libre** recense les **logiciels libres** métiers 
utiles aux services publics ainsi que leurs utilisateurs et prestataires.

## Plateforme collaborative

Plate-forme collaborative, il permet de **partager** vos expériences, 
**trouver** vos outils, et **identifier** vos partenaires. 
