# Mentions légales

## Éditeur

[Association ADULLACT](https://adullact.org/) <br>
5 rue du plan du palais <br>
34000 Montpellier, FRANCE


## Directeur de publication

_Mathieu FAURE_, Délégué Général de l'ADULLACT


## Hébergement

**OVH**  <br>
2 rue Kellermann <br>
59100 Roubaix, FRANCE


## TODO : ressources

- <https://comptoir-du-libre.org/fr/pages/legal>
- <https://adullact.org/index.php/mentions-legales>
- <https://faq.adullact.org/mentions-legales/>