# Open data : données ouvertes

Une partie des données du **Comptoir du Libre** sont publiées en open data.

## Les données ouvertes du Comptoir du Libre

Actuellement, tous les logiciels libres présents sur le site web _Comptoir du Libre_
sont mis à votre disposition dans un export au format JSON, avec pour chaque logiciel : 

- la licence, 
- le nom du logiciel, 
- le dépôt du code source, 
- le site web, 
- les ressources externes (Wikipédia, Wikidata, Framalibre et SILL) 
- les prestataires déclarés.

Vous pouvez consulter ces données ouvertes et la documentation associée 
sur [data.gouv.fr](https://www.data.gouv.fr/fr/datasets/logiciels-libres-sur-le-comptoir-du-libre-org/).
