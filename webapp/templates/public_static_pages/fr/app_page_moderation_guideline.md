## Charte de modération du contenu

## Critères d'ajout d'un logiciel

Les critères à prendre en compte pour l'ajout d'un logiciel sur le _Comptoir du libre_ sont :
- la **licence** doit être une des [licences libres](https://opensource.org/licenses) approuvées par l'OSI.
- le **code source** doit être disponible dans un dépôt public d'une forge logiciel.

> **TODO**
> - cas des logiciels à plusieurs licences
> - cas des logiciels à plusieurs dépots
> - qualité minimale pour ajouter un projet

### Code source

Le code source du logiciel doit être publié :

- sous l'une des [licences libres](https://opensource.org/licenses) approuvées par l'OSI.
- le fichier de la licence doit être présent à la racine du projet (par exemple dans un fichier `LICENSE`)
- sans délai entre la publication du code source et celle des fichiers exécutables.
- dans un dépôt public sans authentification.

### URL du dépôt

Dans le formulaire d'ajout du logiciel, critères à prendre en compte
concernant le champ **URL** du **dépôt** :

- Ce champ est obligatoire.
- L'URL à fournir est celle du dépôt consultable publiquement.
- Cette URL doit correspondre au code source du logiciel.
- Cette URL doit permettre de faire un `git clone`
  (ou une commande équivalente pour d'autre gestionnaire de code source).

