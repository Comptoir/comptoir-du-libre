############################################################################################################
######### Symfony framework ################################################################################

# Use the front controller as index file. It serves as a fallback solution when
# every other rewrite/redirect fails (e.g. in an aliased environment without
# mod_rewrite). Additionally, this reduces the matching process for the
# start page (path "/") because otherwise Apache will apply the rewriting rules
# to each configured DirectoryIndex file (e.g. index.php, index.html, index.pl).
DirectoryIndex index.php

# By default, Apache does not evaluate symbolic links if you did not enable this
# feature in your server configuration. Uncomment the following line if you
# install assets as symlinks or if you experience problems related to symlinks
# when compiling LESS/Sass/CoffeScript assets.
# Options +SymLinksIfOwnerMatch

# Disabling MultiViews prevents unwanted negotiation, e.g. "/index" should not resolve
# to the front controller "/index.php" but be rewritten to "/index.php/index".
<IfModule mod_negotiation.c>
    Options -MultiViews
</IfModule>

<IfModule mod_rewrite.c>
    # This Option needs to be enabled for RewriteRule, otherwise it will show an error like
    # 'Options FollowSymLinks or SymLinksIfOwnerMatch is off which implies that RewriteRule directive is forbidden'
    Options +SymLinksIfOwnerMatch

    RewriteEngine On

    # Determine the RewriteBase automatically and set it as environment variable.
    # If you are using Apache aliases to do mass virtual hosting or installed the
    # project in a subdirectory, the base path will be prepended to allow proper
    # resolution of the index.php file and to redirect to the correct URI. It will
    # work in environments without path prefix as well, providing a safe, one-size
    # fits all solution. But as you do not need it in this case, you can comment
    # the following 2 lines to eliminate the overhead.
    RewriteCond %{REQUEST_URI}::$0 ^(/.+)/(.*)::\2$
    RewriteRule .* - [E=BASE:%1]

    # Sets the HTTP_AUTHORIZATION header removed by Apache
    RewriteCond %{HTTP:Authorization} .+
    RewriteRule ^ - [E=HTTP_AUTHORIZATION:%0]

    # Redirect to URI without front controller to prevent duplicate content
    # (with and without `/index.php`). Only do this redirect on the initial
    # rewrite by Apache and not on subsequent cycles. Otherwise we would get an
    # endless redirect loop (request -> rewrite to front controller ->
    # redirect -> request -> ...).
    # So in case you get a "too many redirects" error or you always get redirected
    # to the start page because your Apache does not expose the REDIRECT_STATUS
    # environment variable, you have 2 choices:
    # - disable this feature by commenting the following 2 lines or
    # - use Apache >= 2.3.9 and replace all L flags by END flags and remove the
    #   following RewriteCond (best solution)
    RewriteCond %{ENV:REDIRECT_STATUS} =""
    RewriteRule ^index\.php(?:/(.*)|$) %{ENV:BASE}/$1 [R=301,L]

    # If the requested filename exists, simply serve it.
    # We only want to let Apache serve files and not directories.
    # Rewrite all other queries to the front controller.
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteRule ^ %{ENV:BASE}/index.php [L]
</IfModule>

<IfModule !mod_rewrite.c>
    <IfModule mod_alias.c>
        # When mod_rewrite is not available, we instruct a temporary redirect of
        # the start page to the front controller explicitly so that the website
        # and the generated links can still be used.
        RedirectMatch 307 ^/$ /index.php/
        # RedirectTemp cannot be used instead
    </IfModule>
</IfModule>


############################################################################################################
######### Comptoir-du-Libre software  ######################################################################

<IfModule mod_headers.c>

    #### Webperf #############################################

    # HTTP "Cache-Control: immutable" headers
    # for CSS and JS files to use the browser's cache to the maximum (cache busting is mandatory)
    #   - For Firefox, thhe HTTP "Cache-Control: immutable" header will be honored only if the content is served in HTTPS.
    #   - Chrome does not actually support immutable at the moment, but has its own new heuristics
    #   - MDN documenation                  https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Cache-Control
    #   - Blog post                         https://www.nicolas-hoffmann.net/source/1710-Deployer-cache-control-immutable-avec-du-cache-busting.html
    <FilesMatch "\.(js|css)$">
        Header set Cache-Control "public, max-age=31536000, immutable"
    </FilesMatch>


    #### Security #############################################

    # HTTP Permissions-Policy header
    #    - W3C Specification                https://www.w3.org/TR/permissions-policy/
    #    - W3C Permissions Policy Explainer https://github.com/w3c/webappsec-permissions-policy/blob/main/permissions-policy-explainer.md
    #    - W3C Policy Controlled Features   https://github.com/w3c/webappsec-permissions-policy/blob/main/features.md
    #    - MDN documenation                 https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Permissions-Policy
    #    - Chrome documenation              https://developer.chrome.com/docs/privacy-sandbox/permissions-policy/
    #    - Permissions-Policy Generator     https://www.permissionspolicy.com
    Header set Permissions-Policy "accelerometer=(), ambient-light-sensor=(), autoplay=(), battery=(), camera=(), cross-origin-isolated=(), display-capture=(), document-domain=(), encrypted-media=(), execution-while-not-rendered=(), execution-while-out-of-viewport=(), fullscreen=(), geolocation=(), gyroscope=(), keyboard-map=(), magnetometer=(), microphone=(), midi=(), navigation-override=(), payment=(), picture-in-picture=(), publickey-credentials-get=(), screen-wake-lock=(), sync-xhr=(), usb=(), web-share=(), xr-spatial-tracking=(), clipboard-read=(), clipboard-write=()"

    # HTTP Cross-Origin-Opener-Policy (COOP) header
    #    - WHATWG Specification             https://html.spec.whatwg.org/multipage/browsers.html#cross-origin-opener-policies
    #    - MDN documenation                 https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Cross-Origin-Opener-Policy
    #    - Chrome documenation              https://web.dev/security-headers/#coop
    #    - Chrome documenation              https://web.dev/coop-coep/
    #    - Scott Helme blog post            https://scotthelme.co.uk/coop-and-coep/
    #    - Pentesting guide                 https://book.hacktricks.xyz/network-services-pentesting/pentesting-web/special-http-headers#coop
    Header set Cross-Origin-Opener-Policy "same-origin"

    # HTTP Cross-Origin-Resource-Policy (CORP) header
    #    - WHATWG Specification             https://fetch.spec.whatwg.org/#cross-origin-resource-policy-header
    #    - MDN documenation                 https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Cross-Origin-Resource-Policy
    #    - Chrome documenation              https://web.dev/security-headers/#corp
    #    - Consider deploying CORP          https://resourcepolicy.fyi
    #    - Scott Helme blog post            https://scotthelme.co.uk/coop-and-coep/
    #    - Pentesting guide                 https://book.hacktricks.xyz/network-services-pentesting/pentesting-web/special-http-headers#corp
    Header set Cross-Origin-Resource-Policy "same-origin"

    # HTTP Cross-Origin-Embedder-Policy (COEP) header
    #    - WHATWG Specification             https://html.spec.whatwg.org/multipage/browsers.html#coep
    #    - MDN documenation                 https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Cross-Origin-Embedder-Policy
    #    - Chrome documenation              https://web.dev/security-headers/#coep
    #    - Chrome documenation              https://web.dev/coop-coep/
    #    - Scott Helme blog post            https://scotthelme.co.uk/coop-and-coep/
    #    - Pentesting guide                 https://book.hacktricks.xyz/network-services-pentesting/pentesting-web/special-http-headers#coep
    Header set Cross-Origin-Embedder-Policy "require-corp"

</IfModule>
############################################################################################################
