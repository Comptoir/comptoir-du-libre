<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241003095120 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add [ tag_software ] table';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE tag_software (tag_id INT NOT NULL, software_id INT NOT NULL, PRIMARY KEY(tag_id, software_id))');
        $this->addSql('CREATE INDEX IDX_41E11314BAD26311 ON tag_software (tag_id)');
        $this->addSql('CREATE INDEX IDX_41E11314D7452741 ON tag_software (software_id)');
        $this->addSql('ALTER TABLE tag_software ADD CONSTRAINT FK_41E11314BAD26311 FOREIGN KEY (tag_id) REFERENCES tag (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE tag_software ADD CONSTRAINT FK_41E11314D7452741 FOREIGN KEY (software_id) REFERENCES software (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE tag_software DROP CONSTRAINT FK_41E11314BAD26311');
        $this->addSql('ALTER TABLE tag_software DROP CONSTRAINT FK_41E11314D7452741');
        $this->addSql('DROP TABLE tag_software');
    }
}
