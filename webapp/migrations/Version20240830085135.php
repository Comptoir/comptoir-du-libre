<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240830085135 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add [ software_screenshot ] table';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE software_screenshot_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE software_screenshot (id INT NOT NULL, software_id INT NOT NULL, file_name VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_7921ABBBD7452741 ON software_screenshot (software_id)');
        $this->addSql('COMMENT ON COLUMN software_screenshot.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE software_screenshot ADD CONSTRAINT FK_7921ABBBD7452741 FOREIGN KEY (software_id) REFERENCES software (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE software_screenshot_id_seq CASCADE');
        $this->addSql('ALTER TABLE software_screenshot DROP CONSTRAINT FK_7921ABBBD7452741');
        $this->addSql('DROP TABLE software_screenshot');
    }
}
