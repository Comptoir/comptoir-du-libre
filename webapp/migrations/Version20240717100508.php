<?php

/*
 * This file is part of the Comptoir-du-Libre software.
 * <https://gitlab.adullact.net/Comptoir/comptoir-du-libre>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Comptoir-du-Libre is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240717100508 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add a first user with ROLE_SUPERADMIN';
    }

    public function up(Schema $schema): void
    {
        $this->addSql(
            'INSERT INTO "user" ( id,
                                      email,
                                      roles,
                                      password )
                         VALUES    (  1,
                                      \'superadmin_webapp@example.org\',
                                      \'["ROLE_SUPERADMIN"]\',
                                      \'$2y$13$7C4MvPgCNUzWP3kiS/fqPeUr2BBjrEut1lbKqhXxGnBji71l0YE1e\'  )'
        );
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
    }
}
