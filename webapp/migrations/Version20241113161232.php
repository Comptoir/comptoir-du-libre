<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241113161232 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add [ organization_i18n ] table';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE organization_i18n_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE organization_i18n (id INT NOT NULL, organization_id INT NOT NULL, locale VARCHAR(2) NOT NULL, text TEXT NOT NULL, type INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_C65F260B32C8A3DE ON organization_i18n (organization_id)');
        $this->addSql('ALTER TABLE organization_i18n ADD CONSTRAINT FK_C65F260B32C8A3DE FOREIGN KEY (organization_id) REFERENCES organization (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE organization_i18n_id_seq CASCADE');
        $this->addSql('ALTER TABLE organization_i18n DROP CONSTRAINT FK_C65F260B32C8A3DE');
        $this->addSql('DROP TABLE organization_i18n');
    }
}
