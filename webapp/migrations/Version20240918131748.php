<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240918131748 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add [ license ] and [ license_external_resources ] tables';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE license_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE license_external_resources_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE license (id INT NOT NULL, name VARCHAR(255) NOT NULL, short_name VARCHAR(255) DEFAULT NULL, spdx VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, logo VARCHAR(255) DEFAULT NULL, is_osi_approved BOOLEAN NOT NULL, is_fsf_libre BOOLEAN NOT NULL, osi_notes TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE license_external_resources (id INT NOT NULL, license_id INT NOT NULL, url VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_6A0428ED460F904B ON license_external_resources (license_id)');
        $this->addSql('ALTER TABLE license_external_resources ADD CONSTRAINT FK_6A0428ED460F904B FOREIGN KEY (license_id) REFERENCES license (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE license_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE license_external_resources_id_seq CASCADE');
        $this->addSql('ALTER TABLE license_external_resources DROP CONSTRAINT FK_6A0428ED460F904B');
        $this->addSql('DROP TABLE license');
        $this->addSql('DROP TABLE license_external_resources');
    }
}
