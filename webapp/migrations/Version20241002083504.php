<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241002083504 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add [ software_i18n ] table';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE software_i18n_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE software_i18n (id INT NOT NULL, software_id INT NOT NULL, locale VARCHAR(2) NOT NULL, type INT NOT NULL, text TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_DA53126AD7452741 ON software_i18n (software_id)');
        $this->addSql('ALTER TABLE software_i18n ADD CONSTRAINT FK_DA53126AD7452741 FOREIGN KEY (software_id) REFERENCES software (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE software_i18n_id_seq CASCADE');
        $this->addSql('ALTER TABLE software_i18n DROP CONSTRAINT FK_DA53126AD7452741');
        $this->addSql('DROP TABLE software_i18n');
    }
}
