<?php

/*
 * This file is part of the Comptoir-du-Libre software.
 * <https://gitlab.adullact.net/Comptoir/comptoir-du-libre>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Comptoir-du-Libre is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Tests\Functional;

use App\DataFixtures\AppUserFixtures;
use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\MockFileSessionStorage;
use Symfony\Component\DomCrawler\Crawler;

use function PHPUnit\Framework\isNull;

trait TestHelperTrait
{
    /**
     * Get example list of not allowed locale
     * @return string[]
     */
    public function getNotAllowedLocaleExample(): array
    {
        return ['de', 'it'];
    }

    /**
     * Get list of allowed locale
     * @return string[]
     */
    public function getAllowedLocale(): array
    {
        return ['fr', 'en'];
    }

    /**
     * Get a list of emails in invalid format
     * @return string[]
     */
    public function getListOfEmailsInInvalidFormat(): array
    {
        return [
            'this_is_not_a_valid_email',
            'this_is_not_a_valid_email@example',
            'this_is_not_a_valid_email@example.',
            '@example.org',
            'john @example.org',
        ];
    }

    public function generateRandomString(int $length = 10): string
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-!(!.;:/,)]@_(';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[\random_int(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function getKernelBrowserWithConnectedUser(
        string $userEmail,
        KernelBrowser|null $kernelBrowser = null
    ): KernelBrowser {
        if (is_null($kernelBrowser)) {
            $kernelBrowser = static::createClient();
        }
        $userRepository = static::getContainer()->get(UserRepository::class);
        $testUser = $userRepository->findOneByEmail("$userEmail");

        $kernelBrowser->loginUser($testUser);
        $this->commonCheckerUserIsConnected($kernelBrowser, "$userEmail");
        return $kernelBrowser;
    }



    /**
     * @param KernelBrowser $client
     * @param string $currentUrl
     * @param string $expectedNewUrl
     * @param string $expectedCurrentRoute
     * @param string $expectedNewRoute
     * @param int $expectedRedirectCode Expected redirect HTTP code, default = 308 (Response::HTTP_PERMANENTLY_REDIRECT)
     * @return void
     */
    public function checkRedirectionToNewUrl(
        KernelBrowser $client,
        string $currentUrl,
        string $expectedNewUrl,
        string $expectedCurrentRoute,
        string $expectedNewRoute,
        int $expectedRedirectCode = Response::HTTP_PERMANENTLY_REDIRECT,
    ) {
        // Check current URL
        $crawler = $client->request('GET', "$currentUrl");
        $this->assertRouteSame("$expectedCurrentRoute", [], "Current URL --> $currentUrl");
        $this->assertResponseStatusCodeSame($expectedRedirectCode);
        $this->assertResponseHeaderSame("Location", "$expectedNewUrl");

        // Check new URL
        $crawler = $client->request('GET', "$expectedNewUrl");
        $this->assertRouteSame("$expectedNewRoute");
        $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200
    }

    /**
     * @todo refactor
     * @group refactor
     */
    public function checkAttribute(
        Crawler $crawler,
        string $cssFilter,
        array $attributesExpected = ['_text' => '', '_name' => 'a', 'href' => '#'],
        array $notAllowedAttributes = [],
    ): void {
        // Expected attributes
        $attributeKeys = array_keys($attributesExpected);
        $attributesCount = count($attributeKeys);
        $attributes = $crawler
            ->filter("$cssFilter")
            ->extract($attributeKeys);
        $errorMsg = "No match for the following CSS filter : [ $cssFilter ]";
        $this->assertEquals(1, count($attributes), "$errorMsg");
        foreach ($attributeKeys as $keyIndex => $keyName) {
            $expected = $attributesExpected["$keyName"];
            if ($keyName === '_text') {
                $this->assertSelectorTextSame("$cssFilter", "$expected");
            } else {
                if ($attributesCount === 1) {
                    $value = $attributes[$keyIndex];
                } else {
                    $value = $attributes[0][$keyIndex];
                }
                $this->assertEquals(
                    expected: "$expected",
                    actual: "$value",
                    message: "Invalid value for [ $keyName ] atttribut, \non [ $cssFilter ] element"
                );
            }
        }


        // Not allowed attributes
        if (count($notAllowedAttributes) >= 1) {
            $notAllowedAttributesExtractor = $crawler
                ->filter("$cssFilter")
                ->extract($notAllowedAttributes);
            $errorMsg = "At least one of not allowed attributes is present\n";
            $errorMsg .= "for the following CSS filter : [ $cssFilter ]\n";
            $errorMsg .= "\nNot allowed attributs:\n" . print_r($notAllowedAttributes, true);
            $errorMsg .= "\nCSS extractor:\n" . print_r($notAllowedAttributesExtractor, true);
            $this->assertEquals(1, count($notAllowedAttributesExtractor),);
            if (count($notAllowedAttributes) === 1) {
                $this->assertEquals('', $notAllowedAttributesExtractor[0], "$errorMsg");
            } else {
                foreach ($notAllowedAttributesExtractor[0] as $key => $value) {
                    $this->assertEquals('', $value, "$errorMsg");
                }
            }
        }
    }

    public function createSession(KernelBrowser $client): Session
    {
        $container = $client->getContainer();
        $sessionSavePath = $container->getParameter('session.save_path');
        $sessionStorage = new MockFileSessionStorage($sessionSavePath);

        $session = new Session($sessionStorage);
        $session->start();
        $session->save();

        $sessionCookie = new Cookie(
            $session->getName(),
            $session->getId(),
            null,
            null,
            'localhost',
        );
        $client->getCookieJar()->set($sessionCookie);

        return $session;
    }


    /// ///////////////////////////////////////////////////////////////////////////////////////////
    public function commonDisplayUserAccountSettingPage(
        string $connnectedUserEmail,
        KernelBrowser|null $kernelBrowser = null,
        bool $returnKernelBrowser = false,
    ): KernelBrowser|Crawler {
        if (\is_null($kernelBrowser)) {
            $kernelBrowser =  $this->getKernelBrowserWithConnectedUser($connnectedUserEmail);
        } else {
            $kernelBrowser =  $this->getKernelBrowserWithConnectedUser($connnectedUserEmail, $kernelBrowser);
        }
        $crawler = $kernelBrowser->request('GET', '/en/user/account');
        $this->assertRouteSame("app_user_account");
        $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200
        $this->assertSelectorTextSame("#qa_user-account_user-email", "$connnectedUserEmail");
        if ($returnKernelBrowser === true) {
            return $kernelBrowser;
        }
        return $crawler;
    }
    /// ///////////////////////////////////////////////////////////////////////////////////////////
    /**
     * @todo FIXME
     */
    public function commonCheckerUserIsConnected(
        KernelBrowser $kernelBrowser,
        string $userEmail,
        string $checkedRoute = 'app_home'
    ): Crawler {

        $crawler = $kernelBrowser->request('GET', '/en/');
//      $this->assertRouteSame('app_home');
        $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200
        $this->assertSelectorTextSame('.connected_user_email', "$userEmail");
        return $crawler;
    }
    /// ///////////////////////////////////////////////////////////////////////////////////////////
    /// ///////////////////////////////////////////////////////////////////////////////////////////
    private function commonCheckkerUserExistInDatabaseWithExpectedRoles(
        string $userEmail,
        string $expectedPrincipalUserRole
    ): void {
        // New user is add into database with expected principal ROLE
        $userRepository = static::getContainer()->get(UserRepository::class);
        $testUser = $userRepository->findOneByEmail("$userEmail");
        $this->assertInstanceOf('App\Entity\User', $testUser);
        $expectedRoles = ["$expectedPrincipalUserRole", 'ROLE_USER'];
        $this->assertSame($expectedRoles, $testUser->getRoles());
        $this->assertSame("$userEmail", $testUser->getEmail());
    }
    /// ///////////////////////////////////////////////////////////////////////////////////////////
    /// ///////////////////////////////////////////////////////////////////////////////////////////
    public function getDatabaseUserByEmail(
        string $userEmail
    ): User {
        $userRepository = static::getContainer()->get(UserRepository::class);
        $user = $userRepository->findOneByEmail("$userEmail");
        $this->assertInstanceOf('App\Entity\User', $user);
        return $user;
    }
    /// ///////////////////////////////////////////////////////////////////////////////////////////
    /// ///////////////////////////////////////////////////////////////////////////////////////////
    public function commonCheckerUserCanNotLogin(
        string $mail,
        string $password = '',
        string $locale = 'en',
        KernelBrowser $kernelBrowser = null
    ): void {
        if (\is_null($kernelBrowser)) {
            $kernelBrowser = static::createClient();
        }
        $crawler = $kernelBrowser->request('GET', "/$locale/account/login");
        $this->assertRouteSame('app_account_login');

        // https://symfony.com/doc/current/testing.html#submitting-forms
        $crawler = $kernelBrowser->submitForm('login-form-button-submit', [
            '_username' => "$mail",
            '_password' => "$password",
        ]);
        $this->assertResponseStatusCodeSame(Response::HTTP_FOUND); // HTTP status code = 302
        $this->assertResponseHasHeader("Location");
        $this->assertResponseHeaderSame("Location", "http://localhost/$locale/account/login");

        $crawler = $kernelBrowser->request('GET', "/$locale/account/login");
        $this->assertSelectorTextSame('div.alert-warning', "Invalid credentials.");
    }


    public function commonCheckerUserCanLogin(
        string $mail,
        string $password = AppUserFixtures::PASSWORD_1_PLAINTEXT,
        string $role = 'ROLE_USER',
        string $locale = 'en',
    ): void {
        $kernelBrowser = static::createClient();
        $crawler = $kernelBrowser->request('GET', "/$locale/account/login");
        $this->assertRouteSame('app_account_login');

        // https://symfony.com/doc/current/testing.html#submitting-forms
        $crawler = $kernelBrowser->submitForm('login-form-button-submit', [
            '_username' => "$mail",
            '_password' => "$password",
        ]);
        $this->assertResponseStatusCodeSame(Response::HTTP_FOUND); // HTTP status code = 302
        $this->assertResponseHasHeader("Location");
        $this->assertResponseHeaderSame("Location", "http://localhost/$locale/");

        $crawler = $kernelBrowser->request('GET', "http://localhost/$locale/");
        $this->assertRouteSame('app_home_i18n');
        $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200
        $this->assertSelectorTextSame('.connected_user_email', "$mail");
    }


    public function commonCheckerUserCanLoginV2(
        KernelBrowser $kernelBrowser,
        string $mail,
        string $password = AppUserFixtures::PASSWORD_1_PLAINTEXT,
        string $role = 'ROLE_USER',
        string $locale = 'en',
    ): void {
        $crawler = $kernelBrowser->request('GET', "/$locale/account/login");
        $this->assertRouteSame('app_account_login');

        // https://symfony.com/doc/current/testing.html#submitting-forms
        $crawler = $kernelBrowser->submitForm('login-form-button-submit', [
            '_username' => "$mail",
            '_password' => "$password",
        ]);
        $this->assertResponseStatusCodeSame(Response::HTTP_FOUND); // HTTP status code = 302
        $this->assertResponseHasHeader("Location");
        $this->assertResponseHeaderSame("Location", "http://localhost/$locale/");

        $crawler = $kernelBrowser->request('GET', "http://localhost/$locale/");
        $this->assertRouteSame('app_home_i18n');
        $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200
        $this->assertSelectorTextSame('.connected_user_email', "$mail");
    }

    /**
     * @todo no used yet
     */
    public function commonCheckerUserCanLogout(
        string $mail,
        string $locale = 'en',
    ): void {
        $kernelBrowser =  $this->getKernelBrowserWithConnectedUser($mail);
        $crawler = $kernelBrowser->request('GET', "/$locale/");

        $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200
        $this->assertSelectorTextSame('.connected_user_email', "$mail");
        $this->assertSelectorTextContains('body', "$mail");

        $crawler = $kernelBrowser->request('GET', "/$locale/account/logout");
        $this->assertResponseStatusCodeSame(Response::HTTP_FOUND); // HTTP status code = 302
        $this->assertResponseHasHeader("Location");
        $this->assertResponseHeaderSame("Location", "http://localhost/$locale/");

        $crawler = $kernelBrowser->request('GET', "/$locale/");
        $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200
        $this->assertSelectorNotExists('.connected_user_email');
        $this->assertSelectorTextNotContains('body', "$mail");

        // HTML content checks footer
        $this->checkFooter(crawler: $crawler, locale: $locale, currentPageUrl: "/$locale/");
    }

    /// ///////////////////////////////////////////////////////////////////////////////////////////
    /// ///////////////////////////////////////////////////////////////////////////////////////////
    public function commonCheckerHasExpectedSecurityHttpHeaders(): void
    {
        // Basic HTTP Header checks
//      $this->assertResponseHasHeader("Cache-Control");
//      $this->assertResponseHasHeader("Content-Security-Policy");
//      $this->assertResponseHasHeader("Content-Type");
//      $this->assertResponseHasHeader("Referrer-Policy");
//      $this->assertResponseHasHeader("X-Content-Type-Options");
//      $this->assertResponseHasHeader("X-Frame-Options");
//      $this->assertResponseHasHeader("X-Robots-Tag");
//      $this->assertResponseHasHeader("X-Xss-Protection");

        // HTTP Header checks
        $this->assertResponseHeaderSame("Cache-Control", 'max-age=0, must-revalidate, private');
        $this->assertResponseHeaderSame(
            "Content-Security-Policy",
            'report-uri /csp/report'
        );  // see:  NelmioSecurityBundle
        $this->assertResponseHeaderSame("Content-Type", 'text/html; charset=UTF-8');
        $this->assertResponseHeaderSame(
            "Referrer-Policy",
            'no-referrer, strict-origin-when-cross-origin'
        );
        $this->assertResponseHeaderSame("X-Content-Type-Options", 'nosniff');
        $this->assertResponseHeaderSame("X-Frame-Options", 'DENY');
        $this->assertResponseHeaderSame("X-Robots-Tag", 'noindex');
    //  $this->assertResponseHeaderSame("X-Xss-Protection", '1; mode=block');
    }
}
