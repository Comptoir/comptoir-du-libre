<?php

/*
 * This file is part of the Comptoir-du-Libre software.
 * <https://gitlab.adullact.net/Comptoir/comptoir-du-libre>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Comptoir-du-Libre is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Tests\Functional;

use App\Tests\Functional\TestHelperTrait;
use Symfony\Component\DomCrawler\Crawler;

trait TestHelperBreadcrumbTrait
{
    use TestHelperTrait;

    /**
     * Check that web page has a valid breadcrumb
     *
     * @param Crawler $crawler
     * @param array $breadcrumb  example: [ '/en/legal' => 'Legal', '/en/legal/accessibility' => 'Accessibility', ]
     * @param string $locale example: 'en' or 'fr'
     * @param string $breadcrumbCssSelector default: 'ol.breadcrumb'
     * @return void
     */
    private function checkHasValidBreadcrumb(
        Crawler $crawler,
        array $breadcrumb,
        string $locale = 'en',
        bool $lastItemMustUseArriaCurrentAttribut = true,
        string $breadcrumbCssSelector = 'ol.breadcrumb',
    ): void {
        // Check the number of items in breadcrumb
        $this->assertEquals(
            expected: count($breadcrumb) + 1,
            actual: $crawler->filter("$breadcrumbCssSelector > li")->count(),
            message: 'Number of links in breadcrumb is different from expected.'
        );

        // Check that all breadcrumb URLs are in the same language as the page.
        foreach ($breadcrumb as $itemUrl => $itemText) {
            $localeInUrl = substr("$itemUrl", 1, 2);
            $this->assertEquals(
                expected: $locale,
                actual: $localeInUrl,
                message: "One URL [ $itemUrl ] of links in the breadcrumb is not in  language of the page [ $locale ].",
            );
        }

        // Checks first breadcrumb item (aka: Home)
        $homeLink = [ '_text' => "Home", 'href'  => "/en/" ];
        if ($locale === 'fr') {
            $homeLink = [ '_text' => "Accueil", 'href'  => "/fr/" ];
        }
        $firstLinkSelector = "$breadcrumbCssSelector > li:first-child > a";
        $this->checkAttribute(
            crawler: $crawler,
            cssFilter: "$firstLinkSelector",
            attributesExpected: $homeLink,
        );
        $this->assertNull(
            actual: $crawler->filter("$firstLinkSelector")->attr('aria-current'),
            message: "First breadcrumb link: not allowed [ aria-current ] attribut. (link position: 0)",
        );

        // Checks last breadcrumb item
        if ($lastItemMustUseArriaCurrentAttribut === true) {
            $lastItemKey = array_key_last($breadcrumb);
            $lastItemUrl = $lastItemKey;
            $lastItemText = $breadcrumb["$lastItemKey"];
            $this->checkAttribute(
                crawler: $crawler,
                cssFilter: "$breadcrumbCssSelector > li.active:last-child  > a",
                attributesExpected: [
                    '_text' => "$lastItemText",
                    'href'  => "$lastItemUrl",
                    'aria-current'  => "page",
                ]
            );
            array_pop($breadcrumb); // remove last element
        }

        // Checks other breadcrumb items (not first, not last)
        $breadcrumbKeys = array_keys($breadcrumb);
        foreach ($crawler->filter("$breadcrumbCssSelector")->children() as $position => $node) {
            if ($position !== 0 && $position !== count($breadcrumb) + 1) {
                $humainPosition = $position + 1;
                $xpath = '/' . $node->getNodePath(); // example: //html/body/div/main/nav/ol/li[2]
                $breadcrumbKey = array_shift($breadcrumbKeys);
                $expectedLinkUrl = $breadcrumbKey;
                $expectedLinkText = $breadcrumb["$breadcrumbKey"];
                $this->assertNull(
                    actual: $crawler->filterXPath("$xpath")->filter('a')->attr('aria-current'),
                    message: "Breadcrumb link: not allowed [ aria-current ] attribut. (link position: $humainPosition)",
                );
                $this->assertEquals(
                    expected: $expectedLinkUrl,
                    actual: $crawler->filterXPath("$xpath")->filter('a')->attr('href'),
                    message: "Invalid link URL in breadcrumb (link position: $humainPosition)",
                );
                $this->assertEquals(
                    expected: $expectedLinkText,
                    actual: $crawler->filterXPath("$xpath")->filter('a')->text(),
                    message: "Invalid link text in breadcrumb (link position: $humainPosition)",
                );
            }
        }
    }
}
