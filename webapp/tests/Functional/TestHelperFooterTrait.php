<?php

/*
 * This file is part of the Comptoir-du-Libre software.
 * <https://gitlab.adullact.net/Comptoir/comptoir-du-libre>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Comptoir-du-Libre is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Tests\Functional;

use App\Tests\Functional\TestHelperTrait;
use Symfony\Component\DomCrawler\Crawler;

use function PHPUnit\Framework\isNull;

trait TestHelperFooterTrait
{
    use TestHelperTrait;

    /**
     * @param Crawler $crawler
     * @param string $locale
     * @param string|null $currentPageUrl default is NULL
     * @return void
     */
    public function checkFooter(
        Crawler $crawler,
        string $locale = 'en',
        null|string $currentPageUrl = null ,
    ): void {
        $footerMainNavLinks = [
            '/en/' => 'Home',
            '/en/pages/contact' => 'Contact',
            '/en/pages/about' => 'About',
            '/en/pages/legal' => 'Legal',
            '/en/pages/accessibility' => 'Accessibility',
            '/en/pages/opendata' => 'Open Data',
        ];
        if ($locale === 'fr') {
            $footerMainNavLinks = [
                '/fr/' => 'Accueil',
                '/fr/pages/contact' => 'Contact',
                '/fr/pages/about' => 'À Propos',
                '/fr/pages/legal' => 'Mentions légales',
                '/fr/pages/accessibility' => 'Accessibilité : non conforme',
                '/fr/pages/opendata' => 'Données ouvertes',
            ];
        }

        // Check the number of items in footer main nav
        $footerMainNavCssSelector = "footer#page_footer nav > ul";
        $this->assertEquals(
            expected: count($footerMainNavLinks),
            actual: $crawler->filter("$footerMainNavCssSelector > li")->count(),
            message: 'Number of links in footer main nav is different from expected.'
        );
        // Check each links in footer main nav
        foreach ($footerMainNavLinks as $url => $linkText) {
            $linkSelector = "$footerMainNavCssSelector a[href='$url']";
            $this->checkAttribute(
                crawler: $crawler,
                cssFilter: "$linkSelector",
                attributesExpected:  [ '_text' => "$linkText", 'href'  => "$url" ],
            );
            if ($currentPageUrl === $url) {
                $this->assertEquals(
                    expected: 'page',
                    actual: $crawler->filter("$linkSelector")->attr('aria-current'),
                    message: "Footer: missing [ aria-current ] attribut" .
                             "\n- current page: $currentPageUrl\n- footer link: $url  --> $linkText",
                );
            } else {
                $this->assertNull(
                    actual: $crawler->filter("$linkSelector")->attr('aria-current'),
                    message: "Footer: not allowed [ aria-current ] attribut" .
                             "\n- current page: $currentPageUrl\n- footer link: $url  --> $linkText",
                );
            }
        }
    }
}
