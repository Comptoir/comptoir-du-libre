<?php

/*
 * This file is part of the Comptoir-du-Libre software.
 * <https://gitlab.adullact.net/Comptoir/comptoir-du-libre>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Comptoir-du-Libre is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Tests\Functional;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * @group allow_mutation_testing_by_infection
 */
class FunctionalTestOldUrlsTest extends WebTestCase
{
    use TestHelperTrait;

    /**
     * Old  URL of all software page redirects to new URL
     *
     * @group old_url
     * @group old_url_software
     * @group software
     */
    public function testOldAllSofwareUrlRedirectsToNewUrl(): void
    {
    ///////////////////////////////////////////////////////////////////////////////////////////
    //    Old URLs              Final new URLs              Primary redirection made by Symfony
    //    -------------------------------------------------------------------------------------
    //    /softwares/            /fr/logiciels/             /softwares
    //    /fr/softwares/         /fr/logiciels/             /fr/softwares
    //    /en/softwares/         /en/software/              /en/softwares
    //    /softwares            /fr/logiciels/
    //    /fr/softwares         /fr/logiciels/
    //    /en/softwares         /en/software/
    ///////////////////////////////////////////////////////////////////////////////////////////
        $client = static::createClient();

        // OLD URL with final trailing slash (allowed, but not by default)
        $locales = ['fr', 'en'];
        foreach ($locales as $locale) {
            $currentUrl = "/$locale/softwares/";
            $expectedNewUrl = "http://localhost/$locale/softwares";
            $crawler = $client->request('GET', "$currentUrl");
            $this->assertRouteSame("app_legacy_all_software_i18n_url", [], "Current URL --> $currentUrl");
            $this->assertResponseStatusCodeSame(Response::HTTP_MOVED_PERMANENTLY); // 301 Moved Permanetly
            $this->assertResponseHeaderSame("Location", "$expectedNewUrl");
        }
        $currentUrl = "/softwares/";
        $expectedNewUrl = "http://localhost/softwares";
        $crawler = $client->request('GET', "$currentUrl");
        $this->assertRouteSame("app_legacy_all_software_url", [], "Current URL --> $currentUrl");
        $this->assertResponseStatusCodeSame(Response::HTTP_MOVED_PERMANENTLY); // 301 Moved Permanetly
        $this->assertResponseHeaderSame("Location", "$expectedNewUrl");

        // OLD URL without locale (allowed, but not by default)
        $this->checkRedirectionToNewUrl(  // Check no i18n URL
            client: $client,
            currentUrl: "/softwares",
            expectedNewUrl: "/fr/logiciels/",
            expectedCurrentRoute: 'app_legacy_all_software_url',
            expectedNewRoute: 'app_anonymous_software_display_all_software',
            expectedRedirectCode: Response::HTTP_PERMANENTLY_REDIRECT, // 308 Permanent Redirect
        );

        // OLD french default URL (with locale, without trailing slash)
        $this->checkRedirectionToNewUrl(  // Check i18n URL
            client: $client,
            currentUrl: "/fr/softwares",
            expectedNewUrl: "/fr/logiciels/",
            expectedCurrentRoute: 'app_legacy_all_software_i18n_url',
            expectedNewRoute: 'app_anonymous_software_display_all_software',
            expectedRedirectCode: Response::HTTP_PERMANENTLY_REDIRECT, // 308 Permanent Redirect
        );

        // OLD english default URL (with locale, without trailing slash)
        $this->checkRedirectionToNewUrl(  // Check i18n URL
            client: $client,
            currentUrl: "/en/softwares",
            expectedNewUrl: "/en/software/",
            expectedCurrentRoute: 'app_legacy_all_software_i18n_url',
            expectedNewRoute: 'app_anonymous_software_display_all_software',
            expectedRedirectCode: Response::HTTP_PERMANENTLY_REDIRECT, // 308 Permanent Redirect
        );
    }


    /**
     * Old  URL of software redirects to new URL
     *
     * @group old_url
     * @group old_url_software
     * @group software
     */
    public function testOldOneSofwareUrlRedirectsToNewUrlIfSoftwareExists(): void
    {
        ////////////////////////////////////////////////////////////////////////////////
        //    Old Software URLs     New Software URLs
        //    -----------------------------------------------------
        //    /softwares/418        /fr/software/1/apache-superset/
        //    /fr/softwares/418     /fr/software/1/apache-superset/
        //    /en/softwares/418     /en/software/1/apache-superset/
        ////////////////////////////////////////////////////////////////////////////////
        $client = static::createClient();
        $softwareList = [
            ['oldId' => 418, 'newId' => 1, 'slug' => 'apache-superset'],
            ['oldId' => 629, 'newId' => 2, 'slug' => 'api-platform'],
        ];
        foreach ($softwareList as $software) {
            $newId = $software['newId'];
            $oldId = $software['oldId'];
            $slug = $software['slug'];
            $this->checkRedirectionToNewUrl(  // Check no i18n URL
                client: $client,
                currentUrl: "/softwares/$oldId",
                expectedNewUrl: "/fr/logiciel/$newId/$slug/",
                expectedCurrentRoute: 'app_legacy_software_url',
                expectedNewRoute: 'app_anonymous_software_display_one_software',
            );
            foreach ($this->getAllowedLocale() as $lang) { // Check i18n URLs
                $newUrl = "/$lang/software/$newId/$slug/";
                if ($lang === 'fr') {
                    $newUrl = "/$lang/logiciel/$newId/$slug/";
                }
                $this->checkRedirectionToNewUrl(
                    client: $client,
                    currentUrl: "/$lang/softwares/$oldId",
                    expectedNewUrl: "$newUrl",
                    expectedCurrentRoute: 'app_legacy_software_i18_url',
                    expectedNewRoute: 'app_anonymous_software_display_one_software',
                );
            }
        }
    }
}
