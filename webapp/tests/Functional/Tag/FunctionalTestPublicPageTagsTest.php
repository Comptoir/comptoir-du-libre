<?php

/*
 * This file is part of the Comptoir-du-Libre software.
 * <https://gitlab.adullact.net/Comptoir/comptoir-du-libre>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Comptoir-du-Libre is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Tag;

use App\DataFixtures\AppTagFixtures;
use App\Tests\Functional\TestHelperBreadcrumbTrait;
use App\Tests\Functional\TestHelperTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class FunctionalTestPublicPageTagsTest extends WebTestCase
{
    use TestHelperTrait;
    use TestHelperBreadcrumbTrait;

    public function testAllTagsPageIsDisplayed(): void
    {
        $locale = 'en';
        $tagsTotal = AppTagFixtures::TAG_COUNT;
        $client = static::createClient();
        $crawler = $client->request('GET', "/$locale/tags/");
        $this->assertRouteSame('app_tags_display_all');
        $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200
        $this->assertSelectorTextSame('h1', 'Tags');
        $this->assertPageTitleSame('Tags - Comptoir du Libre TEST');
        $this->assertSelectorTextSame('#tags_count', "$tagsTotal tags");
        $this->assertSelectorCount(5, 'a.tag_link');
        $this->checkAttribute(
            crawler: $crawler,
            cssFilter: '#tag_base-de-donnees a.tag_link',
            attributesExpected: [
                '_name' => 'a',
                '_text' => 'Base de données',
                'href'  => "/$locale/tag/3/base-de-donnees/"
            ]
        );

        // HTML content checks breadcrumb
        $breadcrumbLinks = [
            "/$locale/tags/" => "Tags",
        ];
        $this->checkHasValidBreadcrumb($crawler, $breadcrumbLinks, "$locale");
    }

    public function testAllTagsPageIsDisplayedForFrenchLocale(): void
    {
        $locale = 'fr';
        $tagsTotal = AppTagFixtures::TAG_COUNT;
        $client = static::createClient();
        $crawler = $client->request('GET', "/$locale/tags/");
        $this->assertRouteSame('app_tags_display_all');
        $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200
        $this->assertSelectorTextSame('h1', 'Étiquettes');
        $this->assertPageTitleSame('Étiquettes - Comptoir du Libre TEST');

        // HTML content checks breadcrumb
        $breadcrumbLinks = [
            "/$locale/tags/" => "Étiquettes",
        ];
        $this->checkHasValidBreadcrumb($crawler, $breadcrumbLinks, "$locale");
    }

    public function testOneTagPageIsDisplayed(): void
    {
        $locale = 'en';
        $client = static::createClient();
        $crawler = $client->request('GET', "/$locale/tag/3/base-de-donnees/");
        $this->assertRouteSame('app_tags_display_one_tag');
        $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200
        $this->assertSelectorTextSame('h1', 'Tag: Base de données');
        $this->assertPageTitleSame('Tag: Base de données - Comptoir du Libre TEST');

        // HTML content checks breadcrumb
        $breadcrumbLinks = [
            "/$locale/tags/" => "Tags",
            "/$locale/tag/3/base-de-donnees/" => "Base de données",
        ];
        $this->checkHasValidBreadcrumb($crawler, $breadcrumbLinks, "$locale");
    }

    public function testOneTagPageIsDisplayedForFrenchLocale(): void
    {
        $locale = 'fr';
        $client = static::createClient();
        $crawler = $client->request('GET', "/$locale/tag/3/base-de-donnees/");
        $this->assertRouteSame('app_tags_display_one_tag');
        $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200
        $this->assertSelectorTextSame('h1', 'Étiquette : Base de données');
        $this->assertPageTitleSame('Étiquette : Base de données - Comptoir du Libre TEST');

        // HTML content checks breadcrumb
        $breadcrumbLinks = [
            "/$locale/tags/" => "Étiquettes",
            "/$locale/tag/3/base-de-donnees/" => "Base de données",
        ];
        $this->checkHasValidBreadcrumb($crawler, $breadcrumbLinks, "$locale");
    }

    public function testOneTagPageWithBadSlugIsRedirectToValidUrl(): void
    {
        $client = static::createClient();
        $this->checkRedirectionToNewUrl(
            client: $client,
            currentUrl: "/en/tag/3/db/",
            expectedNewUrl: "/en/tag/3/base-de-donnees/",
            expectedCurrentRoute: 'app_tags_display_one_tag',
            expectedNewRoute: 'app_tags_display_one_tag',
        );
        $this->checkRedirectionToNewUrl(
            client: $client,
            currentUrl: "/en/tag/3/sgbd/",
            expectedNewUrl: "/en/tag/3/base-de-donnees/",
            expectedCurrentRoute: 'app_tags_display_one_tag',
            expectedNewRoute: 'app_tags_display_one_tag',
        );
    }

    public function testOneTagPageAllowToRemoveSlugAndIdInUrlToRedirectToAllTagsPage(): void
    {
        $client = static::createClient();
        $this->checkRedirectionToNewUrl(
            client: $client,
            currentUrl: "/en/tag/",
            expectedNewUrl: "/en/tags/",
            expectedCurrentRoute: 'app_tags_display_all_fix_url_typo',
            expectedNewRoute: 'app_tags_display_all',
        );
        $this->checkRedirectionToNewUrl(
            client: $client,
            currentUrl: "/fr/tag/",
            expectedNewUrl: "/fr/tags/",
            expectedCurrentRoute: 'app_tags_display_all_fix_url_typo',
            expectedNewRoute: 'app_tags_display_all',
        );
    }
}
