<?php

/*
 * This file is part of the Comptoir-du-Libre software.
 * <https://gitlab.adullact.net/Comptoir/comptoir-du-libre>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Comptoir-du-Libre is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Tests\Functional\PublicPages;

use App\Tests\Functional\TestHelperFooterTrait;
use App\Tests\Functional\TestHelperTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * @group allow_mutation_testing_by_infection
 */
class FunctionalTestPublicPageHomepageTest extends WebTestCase
{
    use TestHelperFooterTrait;
    use TestHelperTrait;

    // Accessing Internal Objects
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    // $crawler = $client->getCrawler();     //  Crawler instance
    // $history = $client->getHistory();     // client history
    // $cookieJar = $client->getCookieJar(); // client cookie jar
    // $requestHttpKernel = $client->getRequest();           // HttpKernel request instance
    // $requestBrowserKit = $client->getInternalRequest();   // BrowserKit request instance
    // $responseHttpKernel = $client->getResponse();         // HttpKernel response instance
    // $responseBrowserKit = $client->getInternalResponse(); // BrowserKit response instance
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    // dump($crawler);
    // dump($history);
    // dump($cookieJar);
    // dump($requestHttpKernel);
    // dump($requestBrowserKit);
    // dump($responseHttpKernel);
    // dump($responseBrowserKit);
    // dump($requestBrowserKit->getCookies());
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    public function testHomePageRedirectsToHomePageInDefaultLanguage(): void
    {
        $client = static::createClient();
        $session = $this->createSession($client);
        $crawler = $client->request('GET', '/');
        $this->assertRouteSame('app_home');
        $this->assertResponseStatusCodeSame(Response::HTTP_PERMANENTLY_REDIRECT); // HTTP status code = 308
        $this->assertResponseHeaderSame("Location", '/fr/');
        $this->assertResponseHeaderSame("X-Comptoir-Test-Software-Webapp", '1');
    }


    public function testEnglishHomePageIsDisplayed(): void
    {
        $locale = 'en';
        $url = "/$locale/";
        $client = static::createClient();
        $session = $this->createSession($client);
        $crawler = $client->request('GET', "$url");

        // Basic checks
        $this->assertRouteSame('app_home_i18n');
        $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200
//      $this->assertResponseIsSuccessful(); // HTTP status code >= 200 and HTTP status code < 300;
        $this->assertResponseHeaderSame("X-Comptoir-Test-Software-Webapp", '1');

        // HTML content checks
        $this->assertSelectorTextSame('h1', 'Comptoir du Libre TEST');
        $this->assertPageTitleSame('Comptoir du Libre TEST');

        // HTML content checks footer
        $this->checkFooter(crawler: $crawler, locale: $locale, currentPageUrl: $url);

        // Cookies checks
        //////////////////////////////////////////////////////////////////////////////////////////////////////
//      $this->assertBrowserHasCookie('MOCKSESSID');
//      $this->assertResponseHasCookie('MOCKSESSID'); // ---> FAIL ---> @@@TODO fixme

        // HTTP Header checks
        // ---> already tested in [ testAnonymousCanBrowsePublicUrls() ] method
        //      using [ commonSecurityHttpHeadersChecker() ]  method
        //////////////////////////////////////////////////////////////////////////////////////////////////////
//      $this->assertResponseHeaderSame("Cache-Control", 'max-age=0, must-revalidate, private');
//      $this->assertResponseHeaderSame(
//          "Content-Security-Policy",
//          'report-uri /csp/report'
//      );  // see:  NelmioSecurityBundle
//      $this->assertResponseHeaderSame("Content-Type", 'text/html; charset=UTF-8');
//      $this->assertResponseHeaderSame(
//          "Referrer-Policy",
//          'no-referrer, strict-origin-when-cross-origin'
//      );
//      $this->assertResponseHeaderSame("X-Content-Type-Options", 'nosniff');
//      $this->assertResponseHeaderSame("X-Frame-Options", 'DENY');
//      $this->assertResponseHeaderSame("X-Robots-Tag", 'noindex');
//      $this->assertResponseHeaderSame("X-Xss-Protection", '1; mode=block');
    }
}
