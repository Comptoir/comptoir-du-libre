<?php

/*
 * This file is part of the Comptoir-du-Libre software.
 * <https://gitlab.adullact.net/Comptoir/comptoir-du-libre>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Comptoir-du-Libre is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Tests\Functional\PublicPages;

use App\Tests\Functional\TestHelperTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * @group allow_mutation_testing_by_infection
 */
class FunctionalTestPublicPageHealthCheckTest extends WebTestCase
{
    use TestHelperTrait;

    public function testTryHttpPostMethodToHealthCheckRoute(): void
    {
        $client = static::createClient();
        $crawler = $client->request('POST', '/health-check');
        $this->assertResponseStatusCodeSame(Response::HTTP_METHOD_NOT_ALLOWED); // 405 Method Not Allowed
        $this->assertResponseNotHasHeader("X-Comptoir-Test-Software-Database-Status");
    }

    public function testHttpHeadMethodToHealthCheckRoute(): void
    {
        $client = static::createClient();
        $crawler = $client->request('HEAD', '/health-check');
        $this->assertResponseHasHeader("X-Comptoir-Test-Software-Database-Status");
        $statusCode = $client->getResponse()->getStatusCode();
        if ($statusCode !== 200 && $statusCode !== 503) {
            $this->assertTrue(false, "Bad HTTP status code [ $statusCode ] (Allowed is 503 or 200");
        }
    }

    public function testHttpGetMethodToHealthCheckRoute(): void
    {

        $client = static::createClient();
        $crawler = $client->request('GET', '/health-check');

        $this->assertResponseHasHeader("X-Comptoir-Test-Software-Database-Status");
        $this->assertSelectorTextContains('#webapp-database-status', 'DB_CONNECTION_');

        $statusCode = $client->getResponse()->getStatusCode();
        $dbStatus = $crawler->filter('#webapp-database-status')->innerText();
        if ($statusCode !== 200 && $statusCode !== 503) {
            $this->assertTrue(false, "Bad HTTP status code [ $statusCode ] (Allowed is 503 or 200");
        } elseif ($statusCode === 200) {
            $this->assertSelectorTextSame('#webapp-database-status', 'DB_CONNECTION_SUCCESSFUL');
            $this->assertResponseHeaderSame("X-Comptoir-Test-Software-Database-Status", 'DB_CONNECTION_SUCCESSFUL');
        } elseif (
            $statusCode === 503 && $dbStatus === 'DB_CONNECTION_FAILED'
        ) {
            // Check database status
            if ($dbStatus === 'DB_CONNECTION_FAILED') {
                $this->assertResponseHeaderSame("X-Comptoir-Test-Software-Database-Status", 'DB_CONNECTION_FAILED');
            } elseif ($dbStatus === 'DB_CONNECTION_SUCCESSFUL') {
                $this->assertResponseHeaderSame("X-Comptoir-Test-Software-Database-Status", 'DB_CONNECTION_SUCCESSFUL');
            } else {// Bad database status code
                $this->assertTrue(false, "Bad database status code [ $dbStatus ]");
            }
        } else { // HTTP status code 503, but not FAILED database status
            $msg = "Bad HTTP status code [ $statusCode ] with ";
            $msg .= "database status code [ $dbStatus ] ";
            $this->assertTrue(false, "$msg");
        }
    }
}
