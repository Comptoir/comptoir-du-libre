<?php

/*
 * This file is part of the Comptoir-du-Libre software.
 * <https://gitlab.adullact.net/Comptoir/comptoir-du-libre>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Comptoir-du-Libre is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Tests\Functional\PublicPages;

use App\Tests\Functional\TestHelperBreadcrumbTrait;
use App\Tests\Functional\TestHelperFooterTrait;
use App\Tests\Functional\TestHelperTrait;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Response;

/**
 * @group allow_mutation_testing_by_infection
 */
class FunctionalTestStaticPagesTest extends WebTestCase
{
    use TestHelperBreadcrumbTrait;
    use TestHelperFooterTrait;
    use TestHelperTrait;

    /**
     * Check that main content contains (default <main> tag)
     * has an HTML element (see: $cssSelector)
     * whose content is partially defined (see: $content)
     */
    private function checkMainContentHasOneSingleElementWithPartialContent(
        Crawler $crawler,
        string $content,
        string $contentCssSelector,
        string $mainContentCssSelector = 'main',
    ): void {
        $cssSelector = "$mainContentCssSelector > $contentCssSelector:contains('$content')";
        $this->assertEquals(
            expected: 1,
            actual: $crawler->filter($cssSelector)->count(),
            message: "no HTML element [ $cssSelector ] found"
        );
    }


    /**
     * Check that main content (<main> tag by default)
     * contains at least one HTML element (define by $cssSelector)
     */
    private function checkMainContentHasAtLeastOneHtmlElement(
        Crawler $crawler,
        string $cssSelector,
        string $mainContentCssSelector = 'main',
    ): void {
        $cssSelector = "$mainContentCssSelector > $cssSelector";
        $this->assertTrue(
            condition: 1 <= $crawler->filter("$cssSelector")->count(),
            message: "no HTML element [ $cssSelector ] found"
        );
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////



    /**
     * @param KernelBrowser $client
     * @param string $locale
     * @param string $url
     * @param string $mainTitle
     * @param string $localizedSubtitle
     * @param array $breadcrumb example: [ '/en/legal' => 'Legal', '/en/legal/accessibility' => 'Accessibility', ]
     * @return void
     */
    public function checkI18nStaticPageIsDisplayed(
        KernelBrowser $client,
        string $locale,
        string $route,
        string $url,
        string $mainTitle,
        string $localizedSubtitle,
        array $breadcrumb,
    ): void {
        $crawler = $client->request('GET', "$url");

        // Basic checks + basic HTML content checks
        $this->assertRouteSame("$route");
        $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200
        $this->assertSelectorTextSame('h1', "$mainTitle");
        $this->assertPageTitleSame("$mainTitle - Comptoir du Libre TEST");

        // HTML content checks breadcrumb
        $this->checkHasValidBreadcrumb($crawler, $breadcrumb, "$locale");

        // Check that content transformation (from Markdown to HTML) is effective
        $this->checkMainContentHasAtLeastOneHtmlElement($crawler, 'h2');
        $this->checkMainContentHasAtLeastOneHtmlElement($crawler, 'p');

        // Check that content is displayed in current language
        $this->checkMainContentHasOneSingleElementWithPartialContent($crawler, $localizedSubtitle, 'h2',);

        // HTML content checks footer
        $this->checkFooter(crawler: $crawler, locale: $locale, currentPageUrl: $url);
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @group static_page
     * @group static_page_legal
     * @group i18n
     * @group i18n_english
     */
    public function testLegalPageIsDisplayed(): void
    {
        $client = static::createClient();
        $this->checkI18nStaticPageIsDisplayed(
            client: $client,
            locale: 'en',
            route: 'app_page_legal',
            url: '/en/pages/legal',
            mainTitle: 'Legal',
            localizedSubtitle: 'Publication director',
            breadcrumb: [
                '/en/pages/legal' => 'Legal',
            ],
        );
    }

    /**
     * @group static_page
     * @group static_page_legal
     * @group i18n
     * @group i18n_french
    */
    public function testLegalPageIsDisplayedForFrenchLocale(): void
    {
        $client = static::createClient();
        $this->checkI18nStaticPageIsDisplayed(
            client: $client,
            locale: 'fr',
            route: 'app_page_legal',
            url: '/fr/pages/legal',
            mainTitle: 'Mentions légales',
            localizedSubtitle: 'Directeur de publication',
            breadcrumb: [
                '/fr/pages/legal' => 'Mentions légales',
            ],
        );
    }


    //////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////


    /**
     * @group static_page
     * @group static_page_a11y
     * @group i18n
     * @group i18n_english
     */
    public function testAccessibilityPageIsDisplayed(): void
    {
        $client = static::createClient();
        $this->checkI18nStaticPageIsDisplayed(
            client: $client,
            locale: 'en',
            route: 'app_page_accessibility',
            url: '/en/pages/accessibility',
            mainTitle: 'Accessibility',
            localizedSubtitle: 'Accessibility conformance',
            breadcrumb: [
                '/en/pages/legal' => 'Legal',
                '/en/pages/accessibility' => "Accessibility",
            ],
        );
    }


    /**
     * @group static_page
     * @group static_page_a11y
     * @group i18n
     * @group i18n_french
     */
    public function testAccessibilityPageIsDisplayedForFrenchLocale(): void
    {
        $client = static::createClient();
        $this->checkI18nStaticPageIsDisplayed(
            client: $client,
            locale: 'fr',
            route: 'app_page_accessibility',
            url: '/fr/pages/accessibility',
            mainTitle: "Déclaration d'accessibilité",
            localizedSubtitle: 'Contenus non accessibles',
            breadcrumb: [
                '/fr/pages/legal' => 'Mentions légales',
                '/fr/pages/accessibility' => "Déclaration d'accessibilité",
            ],
        );
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////



    /**
     * @group static_page
     * @group static_page_privacy
     * @group i18n
     * @group i18n_english
     */
    public function testPrivacyPageIsDisplayed(): void
    {
        // @@@TODO - Stop here and mark this test as incomplete.
        $this->markTestIncomplete('This test has not been implemented yet.');

        $client = static::createClient();
        $this->checkI18nStaticPageIsDisplayed(
            client: $client,
            locale: 'en',
            route: 'app_page_privacy',
            url: '/en/pages/privacy',
            mainTitle: 'Privacy Policy',
            localizedSubtitle: 'Legal basis we have for processing your personal data',
            breadcrumb: [
                '/en/pages/legal' => 'Legals',
                '/en/pages/privacy' => "Privacy Policy",
            ],
        );
    }


    /**
     * @group static_page
     * @group static_page_privacy
     * @group i18n
     * @group i18n_french
     */
    public function testPrivacyPageIsDisplayedForFrenchLocale(): void
    {
        // @@@TODO - Stop here and mark this test as incomplete.
        $this->markTestIncomplete('This test has not been implemented yet.');

        $client = static::createClient();
        $this->checkI18nStaticPageIsDisplayed(
            client: $client,
            locale: 'fr',
            route: 'app_page_privacy',
            url: '/fr/pages/privacy',
            mainTitle: "Politique de confidentialité",
            localizedSubtitle: 'Base juridique sur laquelle nous nous appuyons pour traiter vos données personnelles',
            breadcrumb: [
                '/fr/pages/legal' => 'Mentions légales',
                '/fr/pages/privacy' => "Politique de confidentialité",
            ],
        );
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @group static_page
     * @group static_page_about
     * @group i18n
     * @group i18n_english
     */
    public function testAboutPageIsDisplayed(): void
    {
        $client = static::createClient();
        $this->checkI18nStaticPageIsDisplayed(
            client: $client,
            locale: 'en',
            route: 'app_page_about',
            url: '/en/pages/about',
            mainTitle: "About Comptoir du Libre",
            localizedSubtitle: 'Collaborative platform',
            breadcrumb: [
                '/en/pages/about' => 'About',
            ],
        );
    }

    /**
     * @group static_page
     * @group static_page_about
     * @group i18n
     * @group i18n_french
     */
    public function testAboutPageIsDisplayedForFrenchLocale(): void
    {
        $client = static::createClient();
        $this->checkI18nStaticPageIsDisplayed(
            client: $client,
            locale: 'fr',
            route: 'app_page_about',
            url: '/fr/pages/about',
            mainTitle: "À Propos du Comptoir du Libre",
            localizedSubtitle: 'Plateforme collaborative',
            breadcrumb: [
                '/fr/pages/about' => 'À Propos',
            ],
        );
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////


    /**
     * @group static_page
     * @group static_page_opendata
     * @group i18n
     * @group i18n_english
     */
    public function testOpenDataPageIsDisplayed(): void
    {
        $client = static::createClient();
        $this->checkI18nStaticPageIsDisplayed(
            client: $client,
            locale: 'en',
            route: 'app_page_opendata',
            url: '/en/pages/opendata',
            mainTitle: "Open data",
            localizedSubtitle: 'Comptoir du Libre open data',
            breadcrumb: [
                '/en/pages/about' => 'About',
                '/en/pages/opendata' => 'Open data',
            ],
        );
    }


    /**
     * @group static_page
     * @group static_page_opendata
     * @group i18n
     * @group i18n_french
     */
    public function testOpenDataPageIsDisplayedForFrenchLocale(): void
    {
        $client = static::createClient();
        $this->checkI18nStaticPageIsDisplayed(
            client: $client,
            locale: 'fr',
            route: 'app_page_opendata',
            url: '/fr/pages/opendata',
            mainTitle: "Open data : données ouvertes",
            localizedSubtitle: 'Les données ouvertes du Comptoir du Libre',
            breadcrumb: [
                '/fr/pages/about' => 'À Propos',
                '/fr/pages/opendata' => 'Open data',
            ],
        );
    }


    //////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @group static_page
     * @group static_page_moderation_guidline
     * @group i18n
     * @group i18n_english
     */
    public function testContentModerationGuidelinePageIsDisplayed(): void
    {
        // @@@TODO - Stop here and mark this test as incomplete.
        $this->markTestIncomplete('This test has not been implemented yet.');

        $client = static::createClient();
        $this->checkI18nStaticPageIsDisplayed(
            client: $client,
            locale: 'en',
            route: 'app_page_moderation_guideline',
            url: '/en/pages/moderation-guideline',
            mainTitle: "Moderation guideline",
            localizedSubtitle: 'Criteria for adding a software',
            breadcrumb: [
                '/en/pages/about' => 'About',
                '/en/pages/partners' => 'Moderation guideline',
            ],
        );
    }

    /**
     * @group static_page
     * @group static_page_moderation_guidline
     * @group i18n
     * @group i18n_french
     */
    public function testContentModerationGuidelinePageIsDisplayedForFrenchLocale(): void
    {
        // @@@TODO - Stop here and mark this test as incomplete.
        $this->markTestIncomplete('This test has not been implemented yet.');

        $client = static::createClient();
        $this->checkI18nStaticPageIsDisplayed(
            client: $client,
            locale: 'fr',
            route: 'app_page_moderation_guideline',
            url: '/fr/pages/moderation-guideline',
            mainTitle: "Charte de modération du contenu",
            localizedSubtitle: "Critères d'ajout d'un logiciel",
            breadcrumb: [
                '/fr/pages/about' => 'À Propos',
                '/fr/pages/partners' => 'Modération du contenu',
            ],
        );
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @group static_page
     * @group static_page_partners
     * @group i18n
     * @group i18n_english
     */
    public function notYetTestPartnersPageIsDisplayed(): void
    {
        // @@@TODO - Stop here and mark this test as incomplete.
        $this->markTestIncomplete('This test has not been implemented yet.');

        $client = static::createClient();
        $this->checkI18nStaticPageIsDisplayed(
            client: $client,
            locale: 'en',
            route: 'app_page_partners',
            url: '/en/pages/partners',
            mainTitle: "Partners",
            localizedSubtitle: 'Partners of Comptoir du Libre',
            breadcrumb: [
                '/en/pages/about' => 'About',
                '/en/pages/partners' => 'Partners',
            ],
        );
    }

    /**
     * @group static_page
     * @group static_page_partners
     * @group i18n
     * @group i18n_french
     */
    public function notYetTestPartnersPageIsDisplayedForFrenchLocale(): void
    {
        // @@@TODO - Stop here and mark this test as incomplete.
        $this->markTestIncomplete('This test has not been implemented yet.');

        $client = static::createClient();
        $this->checkI18nStaticPageIsDisplayed(
            client: $client,
            locale: 'fr',
            route: 'app_page_partners',
            url: '/fr/pages/partners',
            mainTitle: "Partenaires",
            localizedSubtitle: 'Les partenaires du Comptoir du Libre',
            breadcrumb: [
                '/fr/pages/about' => 'À Propos',
                '/fr/pages/partners' => 'Partenaires',
            ],
        );
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////
}
