<?php

/*
 * This file is part of the Comptoir-du-Libre software.
 * <https://gitlab.adullact.net/Comptoir/comptoir-du-libre>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Comptoir-du-Libre is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Tests\Functional\PublicPages;

use App\DataFixtures\AppLicenceFixtures;
use App\Tests\Functional\TestHelperBreadcrumbTrait;
use App\Tests\Functional\TestHelperTrait;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Response;

class FunctionalTestPublicPageLicensesTest extends WebTestCase
{
    use TestHelperTrait;
    use TestHelperBreadcrumbTrait;

    public function testAllLicensesPageAllowToChangeLocaleInUrl(): void
    {
        $client = static::createClient();
        $this->checkRedirectionToNewUrl(
            client: $client,
            currentUrl: "/fr/licenses/",
            expectedNewUrl: "/fr/licences/",
            expectedCurrentRoute: 'app_anonymous_licenses_display_all_allow_to_change_locale_in_url',
            expectedNewRoute: 'app_anonymous_licenses_display_all',
        );
        $this->checkRedirectionToNewUrl(  // Check no i18n URL
            client: $client,
            currentUrl: "/en/licences/",
            expectedNewUrl: "/en/licenses/",
            expectedCurrentRoute: 'app_anonymous_licenses_display_all_allow_to_change_locale_in_url',
            expectedNewRoute: 'app_anonymous_licenses_display_all',
        );
    }

    public function testOneLicensePageAllowToChangeLocaleInUrl(): void
    {
        $client = static::createClient();
        $this->checkRedirectionToNewUrl(
            client: $client,
            currentUrl: "/fr/license/agpl-3.0-or-later/",
            expectedNewUrl: "/fr/licence/agpl-3.0-or-later/",
            expectedCurrentRoute: 'app_anonymous_licenses_display_one_allow_to_change_locale_in_url',
            expectedNewRoute: 'app_anonymous_licenses_display_one_license',
        );
        $this->checkRedirectionToNewUrl(
            client: $client,
            currentUrl: "/en/licence/agpl-3.0-or-later/",
            expectedNewUrl: "/en/license/agpl-3.0-or-later/",
            expectedCurrentRoute: 'app_anonymous_licenses_display_one_allow_to_change_locale_in_url',
            expectedNewRoute: 'app_anonymous_licenses_display_one_license',
        );
    }

    public function testOneLicensePageAllowToRemoveLicenceSlugInUrlToRedirectToAllLicensesPage(): void
    {
        $client = static::createClient();
        $this->checkRedirectionToNewUrl(
            client: $client,
            currentUrl: "/en/license/",
            expectedNewUrl: "/en/licenses/",
            expectedCurrentRoute: 'app_anonymous_licenses_display_all_fix_url_typo',
            expectedNewRoute: 'app_anonymous_licenses_display_all',
        );
        $this->checkRedirectionToNewUrl(
            client: $client,
            currentUrl: "/fr/licence/",
            expectedNewUrl: "/fr/licences/",
            expectedCurrentRoute: 'app_anonymous_licenses_display_all_fix_url_typo',
            expectedNewRoute: 'app_anonymous_licenses_display_all',
        );
    }


    public function testAllLicensesPageIsDisplayed(): void
    {
        $licencesTotal = AppLicenceFixtures::LICENSE_COUNT;
        $client = static::createClient();
        $crawler = $client->request('GET', "/en/licenses/");
        $this->assertRouteSame('app_anonymous_licenses_display_all');
        $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200
        $this->assertSelectorTextSame('h1', 'Free and open source software licenses');
        $this->assertPageTitleSame('Open-source software licenses - Comptoir du Libre TEST');
        $this->assertSelectorTextSame('#licences_count', "$licencesTotal licenses");
        $locale = 'en';
        $expectedLicences = [
            AppLicenceFixtures::LICENSE_0 => 'GNU Affero General Public License v3.0 or later',
            AppLicenceFixtures::LICENSE_1 => 'GNU Affero General Public License v3.0 only',
            AppLicenceFixtures::LICENSE_2 => 'MIT License',
            AppLicenceFixtures::LICENSE_3 => 'PostgreSQL License',
        ];
        foreach ($expectedLicences as $licenseSlug => $textLink) {
            $this->commonCheckLicenseLink(
                crawler: $crawler,
                licenseSlug: "$licenseSlug",
                textLink: "$textLink",
                locale: "$locale",
            );
        }

        // HTML content checks breadcrumb
        $this->checkHasValidBreadcrumb($crawler, ['/en/licenses/' => 'Licenses'], "en");
    }

    public function testAllLicensesPageIsDisplayedForFrenchLocale(): void
    {
        $licencesTotal = AppLicenceFixtures::LICENSE_COUNT;
        $client = static::createClient();
        $crawler = $client->request('GET', "/fr/licences/");
        $this->assertRouteSame('app_anonymous_licenses_display_all');
        $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200
        $this->assertSelectorTextSame('h1', 'Licences Libres');
        $this->assertPageTitleSame('Licences libres et open source - Comptoir du Libre TEST');
        $this->assertSelectorTextSame('#licences_count', "$licencesTotal licences");

        // HTML content checks breadcrumb
        $this->checkHasValidBreadcrumb($crawler, ['/fr/licences/' => 'Licences'], "fr");
    }

    public function testLicensePageIsDisplayed(): void
    {
        $client = static::createClient();
        $this->commonCheckOffLicensePageIsDisplayed(
            client: $client,
            licenseSlug: AppLicenceFixtures::LICENSE_1,
            licenseFullName: 'GNU Affero General Public License v3.0 only',
            licenseSpdxId: 'AGPL-3.0-only',
            locale: 'en',
        );
        $this->commonCheckOffLicensePageIsDisplayed(
            client: $client,
            licenseSlug: AppLicenceFixtures::LICENSE_2,
            licenseFullName: 'MIT License',
            licenseSpdxId: 'MIT',
            locale: 'en',
        );
    }

    public function testLicensePageIsDisplayedForFrenchLocale(): void
    {
        $client = static::createClient();
        $this->commonCheckOffLicensePageIsDisplayed(
            client: $client,
            licenseSlug: AppLicenceFixtures::LICENSE_1,
            licenseFullName: 'GNU Affero General Public License v3.0 only',
            licenseSpdxId: 'AGPL-3.0-only',
            locale: 'fr',
        );
        $this->commonCheckOffLicensePageIsDisplayed(
            client: $client,
            licenseSlug: AppLicenceFixtures::LICENSE_2,
            licenseFullName: 'MIT License',
            licenseSpdxId: 'MIT',
            locale: 'fr',
        );
    }

    private function commonCheckLicenseLink(
        Crawler $crawler,
        string $licenseSlug,
        string $textLink,
        string $locale = 'en',
    ): void {
        $licenseI18n = 'license';
        if ($locale === 'fr') {
            $licenseI18n = 'licence';
        }
        $licenseCssId = "licence_" . \str_replace('.', '-', $licenseSlug);
        $this->checkAttribute(
            crawler: $crawler,
            cssFilter: "#$licenseCssId a.license_link",
            attributesExpected: [
                '_name' => 'a',
                '_text' => $textLink,
                'href'  => "/$locale/$licenseI18n/$licenseSlug/"
            ]
        );
    }

    private function commonCheckOffLicensePageIsDisplayed(
        KernelBrowser $client,
        string $licenseSlug,
        string $licenseFullName,
        string $licenseSpdxId,
        bool $licenseIsOsiApproved = true,
        bool $licenseIsFsfLibre = false,
        string $locale = 'en',
    ): void {
        $singleLicenseI18nSlug = 'license';
        $singleLicenseI18nTitle = 'License:';
        $licensesI18nSlug = 'licenses';
        $licensesI18nTitle = 'Licenses';
        if ($locale === 'fr') {
            $singleLicenseI18nSlug = 'licence';
            $singleLicenseI18nTitle = 'Licence :';
            $licensesI18nSlug = 'licences';
            $licensesI18nTitle = 'Licences';
        }

        $crawler = $client->request('GET', "/$locale/$singleLicenseI18nSlug/$licenseSlug/");
        $this->assertRouteSame('app_anonymous_licenses_display_one_license');
        $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200

        // HTML content checks
        $this->assertSelectorTextSame('h1', "$singleLicenseI18nTitle $licenseSpdxId");
        $this->assertPageTitleSame("$singleLicenseI18nTitle $licenseSpdxId - Comptoir du Libre TEST");
        $this->assertSelectorTextSame('.license_spdx_id', "$licenseSpdxId");
        $this->assertSelectorTextSame('.license_full_name', "$licenseFullName");
//        $this->assertSelectorTextSame('.license_isOsiApproved', "$licenseIsOsiApproved");
//        $this->assertSelectorTextSame('.license_isFsfLibre', "$licenseIsFsfLibre");

        // HTML content checks breadcrumb
        $breadcrumbLinks = [
            "/$locale/$licensesI18nSlug/" => "$licensesI18nTitle",
            "/$locale/$singleLicenseI18nSlug/$licenseSlug/" => "$licenseSpdxId",
        ];
        $this->checkHasValidBreadcrumb($crawler, $breadcrumbLinks, "$locale");
    }
}
