<?php

/*
 * This file is part of the Comptoir-du-Libre software.
 * <https://gitlab.adullact.net/Comptoir/comptoir-du-libre>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Comptoir-du-Libre is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Tests\Functional\User;

use App\DataFixtures\AppUserFixtures;
use App\Tests\Functional\TestHelperTrait;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class FunctionalTestConnectedUserCanNotDisplayAnonymousPagesTest extends WebTestCase
{
    use TestHelperTrait;

    private string $user1Email;

    protected function setUp(): void
    {
        $this->user1Email = AppUserFixtures::USER_REFERENCE_1;
    }

    /**
     * @param string $connectedUserEmail
     * @param string $testedUrl
     * @param string $expectedNewUrl
     * @param int $expectedHttpCode by default: Response::HTTP_SEE_OTHER = 303
     * @param KernelBrowser|null $kernelBrowser
     * @return KernelBrowser
     */
    private function commonCheckerConnectedUserIsRedirectedToExpectedUrl(
        string $connectedUserEmail,
        string $testedUrl,
        string $expectedNewUrl,
        int $expectedHttpCode = Response::HTTP_SEE_OTHER,
        KernelBrowser|null $kernelBrowser = null,
    ): KernelBrowser {
        if (\is_null($kernelBrowser)) {
            $kernelBrowser =  $this->getKernelBrowserWithConnectedUser($connectedUserEmail);
        } else {
            $kernelBrowser =  $this->getKernelBrowserWithConnectedUser($connectedUserEmail, $kernelBrowser);
        }
        $kernelBrowser->request('GET', "$testedUrl");
        $this->assertResponseStatusCodeSame(expectedCode: $expectedHttpCode, message: 'Bad HTTP code response');
        $this->assertResponseHeaderSame(
            headerName: "Location",
            expectedValue: "$expectedNewUrl",
            message: "Bad redirect location. Tested URL: $testedUrl"
        );
        return $kernelBrowser;
    }

    /**
     * @group ConnectedUser
     * @group ConnectedUser_canNotDisplayAnonymousPages
     * @group CreateAccount
     */
    public function testConnectedUserCanNotDisplaySignupPage(): void
    {
        $kernelBrowser = $this->commonCheckerConnectedUserIsRedirectedToExpectedUrl(
            connectedUserEmail: $this->user1Email,
            testedUrl: "/en/account/signup",
            expectedNewUrl: "/en/user/account",
            expectedHttpCode: Response::HTTP_SEE_OTHER,
        );
        $this->commonCheckerConnectedUserIsRedirectedToExpectedUrl(
            connectedUserEmail: $this->user1Email,
            testedUrl: "/en/account/signup/t/nQfHAZtxFXh-B6TqIBV_vMpwZPshOVkfHt_LSilcZXI",
            expectedNewUrl: "/en/user/account",
            expectedHttpCode: Response::HTTP_SEE_OTHER,
            kernelBrowser: $kernelBrowser,
        );
    }

    /**
     * @group ConnectedUser
     * @group ConnectedUser_canNotDisplayAnonymousPages
     * @group LoginPage
     */
    public function testConnectedUserCanNotDisplayLoginPage(): void
    {
        $this->commonCheckerConnectedUserIsRedirectedToExpectedUrl(
            connectedUserEmail: $this->user1Email,
            testedUrl: "/en/account/login",
            expectedNewUrl: "/en/user/account",
            expectedHttpCode: Response::HTTP_SEE_OTHER,
        );
    }

    /**
     * @group ConnectedUser
     * @group ConnectedUser_canNotDisplayAnonymousPages
     * @group ForgotPasswordPage
     */
    public function testConnectedUserCanNotDisplayForgotPasswordFormPage(): void
    {
        $this->commonCheckerConnectedUserIsRedirectedToExpectedUrl(
            connectedUserEmail: $this->user1Email,
            testedUrl: "/en/account/forgot-password",
            expectedNewUrl: "/en/user/account",
            expectedHttpCode: Response::HTTP_SEE_OTHER,
        );
    }

    /**
     * @group ConnectedUser
     * @group ConnectedUser_canNotDisplayAnonymousPages
     * @group ForgotPasswordPage
     */
    public function testConnectedUserCanNotDisplayForgotPasswordResetLinkPage(): void
    {
        $this->commonCheckerConnectedUserIsRedirectedToExpectedUrl(
            connectedUserEmail: $this->user1Email,
            testedUrl: "/en/account/forgot-password/t/tU8zGN2dHB3XH7LYCN0ZzEMTlsr7XaGvgWLHP1pDNzM",
            expectedNewUrl: "/en/user/account",
            expectedHttpCode: Response::HTTP_SEE_OTHER,
        );
    }
}
