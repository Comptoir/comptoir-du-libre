<?php

/*
 * This file is part of the Comptoir-du-Libre software.
 * <https://gitlab.adullact.net/Comptoir/comptoir-du-libre>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Comptoir-du-Libre is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Tests\Functional\User;

use App\DataFixtures\AppUserFixtures;
use App\Tests\Functional\TestHelperTrait;
use App\Tests\Functional\TestHelperFormTrait;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Response;

class FunctionalTestConnectedUserCanDisplayAccountSettingTest extends WebTestCase
{
    use TestHelperTrait;
    use TestHelperFormTrait;

    private string $user1Email;
    private string $user2Email ;


    protected function setUp(): void
    {
        $this->user1Email = AppUserFixtures::USER_REFERENCE_1;
        $this->user2Email = AppUserFixtures::USER_REFERENCE_2;
    }

    /**
     * @group ConnectedUser
     * @group ConnectedUser_UserAccountSettingPage
     */
    public function testConnectedUserCanSeeEmailAccountOnAccountSettingPage(): void
    {
        $kernelBrowser = $this->commonDisplayUserAccountSettingPage($this->user1Email, null, true);
        $kernelBrowser = $this->commonDisplayUserAccountSettingPage($this->user2Email, $kernelBrowser, false);
    }


    /**
     * @group ConnectedUser
     * @group ConnectedUser_UserAccountSettingPage
 */
    public function testConnectedUserCanSeeCreatedDateOnAccountSettingPage(): void
    {
//        $UserAccountCreationDate = 'Wednesday, November 13, 2024 at 3:06 PM';
//        $crawler =  $this->commonDisplayUserAccountSettingPage($this->user1Email);
//        $this->assertSelectorTextSame("#qa_user-account_createdAt", "$UserAccountCreationDate");
    }

    /**
     * @group ConnectedUser
     * @group ConnectedUser_UserAccountSettingPage
     * @group ConnectedUser_Change_Account_Email
     */
    public function testConnectedUserCanSeeUpdateDateOnAccountSettingPage(): void
    {
//        $UserAccountUpdateDate = 'Nov 13, 2024, 3:06 PM';
//        $crawler =  $this->commonDisplayUserAccountSettingPage($this->user1Email);
//        $this->assertSelectorTextSame("#qa_user-account_updateAt", "$UserAccountUpdateDate");
    }

    /**
     * @group ConnectedUser
     * @group ConnectedUser_UserAccountSettingPage
     * @group ConnectedUser_Change_Password
     */
    public function testConnectedUserCanSeeChangePasswordLinkOnAccountSettingPage(): void
    {
        $crawler =  $this->commonDisplayUserAccountSettingPage($this->user1Email);
        $this->checkAttribute(
            $crawler,
            "#user_change-password_link",
            ['_name' => 'a',  '_text' => 'Change your password',  'href'  => "/en/user/account/password"]
        );
    }

    /**
     * @group ConnectedUser
     * @group ConnectedUser_UserAccountSettingPage
     * @group ConnectedUser_Change_Account_Email
     */
    public function testConnectedUserCanSeeChangeEmailLinkOnAccountSettingPage(): void
    {
//        $crawler =  $this->commonDisplayUserAccountSettingPage($this->user1Email);
//        $this->checkAttribute(
//            $crawler,
//            "#user_change-account_link",
//            ['_name' => 'a',  '_text' => 'Change your account email address ',  'href'  => "/en/user/account/email"]
//        );
    }
}
