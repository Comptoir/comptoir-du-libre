<?php

/*
 * This file is part of the Comptoir-du-Libre software.
 * <https://gitlab.adullact.net/Comptoir/comptoir-du-libre>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Comptoir-du-Libre is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\tests\CommonHelpers;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Part\DataPart;
use Symfony\Component\Mime\RawMessage;

trait TestHelperEmailTrait
{
    /**
     * Check that content of html and text versions contain $text
     */
    public function assertEmailBodyContains(RawMessage $email, string $text, string $message = ''): void
    {
        $this->assertEmailHtmlBodyContains(email: $email, text: $text, message: $message);
        $this->assertEmailTextBodyContains(email: $email, text: $text, message: $message);
    }

    /**
     * Check common technical email headers:
     * - X-Priority
     * - X-Auto-Response-Suppress
     */
    public function checkCommonTechnicalEmailHeaders(
        TemplatedEmail|RawMessage $email
    ): void {
        $this->assertEmailHasHeader($email, "X-Priority");
        $this->assertEmailHeaderSame($email, "X-Priority", "2 (High)");

        $this->assertEmailHasHeader($email, "X-Auto-Response-Suppress");
        $this->assertEmailHeaderSame($email, "X-Auto-Response-Suppress", "OOF, DR, RN, NRN, AutoReply");
                // this non-standard header tells compliant autoresponders ("email holiday mode")
                // to not reply to this message because it's an automated email
    }

    /**
     * Check correspondents email headers:
     * - TO and FROM email HEADERS
     * - others correspondents email HEADERS (Bcc, Cc, Reply-To) are missing, except for Bcc if specified
     */
    public function checkCommonCorrespondentsEmailHeaders(
        TemplatedEmail|RawMessage $email,
        string $expectedEmailTo,
        string $expectedEmailFrom,
        bool $bccIsExpected = false,
        string $expectedEmailBcc = '',
    ): void {
        # Check TO and FROM email HEADERS
        $this->assertEmailAddressContains($email, 'From', "$expectedEmailFrom");
        $this->assertEmailAddressContains($email, 'To', "$expectedEmailTo");
        $emailToAddresses = $email->getTo();
        $emailFromAddresses = $email->getFrom();
        $this->assertCount(1, $emailToAddresses);
        $this->assertCount(1, $emailFromAddresses);
        $this->assertInstanceOf(Address::class, $emailToAddresses[0]);
        $this->assertInstanceOf(Address::class, $emailFromAddresses[0]);
        $this->assertSame("$expectedEmailTo", $emailToAddresses[0]->getAddress());
        $this->assertSame("$expectedEmailFrom", $emailFromAddresses[0]->getAddress());

        # Check others correspondents email HEADERS
        $this->assertEmailNotHasHeader($email, "Reply-To");
        $this->assertEmailNotHasHeader($email, "Cc");
        if ($bccIsExpected === false) {
            $this->assertEmailNotHasHeader($email, "Bcc");
        } elseif (empty($expectedEmailBcc)) {
            $this->assertEmailHasHeader($email, "Bcc");
        } else {
            $this->assertEmailHasHeader($email, "Bcc");
            $this->assertEmailAddressContains($email, 'Bcc', "$expectedEmailBcc");
        }
    }
}
