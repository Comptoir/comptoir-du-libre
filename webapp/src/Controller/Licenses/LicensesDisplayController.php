<?php

namespace App\Controller\Licenses;

use App\Entity\License;
use App\Repository\LicenseRepository;
use Symfony\Bridge\Twig\Attribute\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class LicensesDisplayController extends AbstractController
{
    #[Route(
        path: [
            'fr' => '/{_locale}/licenses/',
            'en' => '/{_locale}/licences/',
        ],
        name: 'app_anonymous_licenses_display_all_allow_to_change_locale_in_url',
        methods: ['GET', 'HEAD']
    )]
    public function allLicensesPageAllowToChangeLocaleInUrl(string $_locale): RedirectResponse
    {
        return new RedirectResponse(
            url: $this->generateUrl(
                route: 'app_anonymous_licenses_display_all',
                parameters: ['_locale' => "$_locale"],
            ),
            status: Response::HTTP_PERMANENTLY_REDIRECT,
        );
    }

    #[Route(
        path: [
            'en' => '/{_locale}/license/',
            'fr' => '/{_locale}/licence/',
        ],
        name: 'app_anonymous_licenses_display_all_fix_url_typo',
        methods: ['GET', 'HEAD']
    )]
    public function allLicensesPageFixUrlTypo(string $_locale): RedirectResponse
    {
        return new RedirectResponse(
            url: $this->generateUrl(
                route: 'app_anonymous_licenses_display_all',
                parameters: ['_locale' => "$_locale"],
            ),
            status: Response::HTTP_PERMANENTLY_REDIRECT,
        );
    }

    #[Route(
        path: [
            'en' => '/{_locale}/licenses/',
            'fr' => '/{_locale}/licences/',
        ],
        name: 'app_anonymous_licenses_display_all',
        methods: ['GET', 'HEAD']
    )]
    #[Template('webapp/licenses/display_all_licenses.html.twig')]
    public function displayAllLicenses(string $_locale, LicenseRepository $licenseRepository): array
    {
        $licensesCollection = $licenseRepository->findAllOrderingByName();
        return [
            'lang' => $_locale,
            'licenses_count'  => count($licensesCollection),
            'licenses_collection' => $licensesCollection,
        ];
        // When using the #[Template] attribute, you only need to return an array with the parameters
        // to pass to the template (the attribute is the one which will create and return the Response object).
    }

    ////////////////////////////////////////////////////////////////////////////

    #[Route(
        path: [
            'fr' => '/{_locale}/license/{slug}/',
            'en' => '/{_locale}/licence/{slug}/',
        ],
        name: 'app_anonymous_licenses_display_one_allow_to_change_locale_in_url',
        methods: ['GET', 'HEAD']
    )]
    public function displayLicensePageAllowToChangeLocaleInUrl(string $_locale, License $license): RedirectResponse
    {
        return new RedirectResponse(
            url: $this->generateUrl(
                route: 'app_anonymous_licenses_display_one_license',
                parameters: [
                    '_locale' => "$_locale",
                    'slug' => $license->getSlug(),
                ],
            ),
            status: Response::HTTP_PERMANENTLY_REDIRECT,
        );
    }




    #[Route(
        path: [
            'en' => '/{_locale}/license/{slug}/',
            'fr' => '/{_locale}/licence/{slug}/',
        ],
        name: 'app_anonymous_licenses_display_one_license',
        methods: ['GET', 'HEAD']
    )]
    #[Template('webapp/licenses/display_one_license.html.twig')]
    public function displayLicense(string $_locale, License $license): array
    {
        return [
            'lang' => $_locale,
            'license' => $license,
        ];
        // When using the #[Template] attribute, you only need to return an array with the parameters
        // to pass to the template (the attribute is the one which will create and return the Response object).
    }
}
