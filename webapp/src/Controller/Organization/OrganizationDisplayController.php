<?php

/*
 * This file is part of the Comptoir-du-Libre software.
 * <https://gitlab.adullact.net/Comptoir/comptoir-du-libre>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Comptoir-du-Libre is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Controller\Organization;

use App\Entity\Organization;
use App\Entity\OrganizationType;
use App\Repository\OrganizationI18nRepository;
use App\Repository\OrganizationRepository;
use App\Repository\OrganizationTypeRepository;
use Symfony\Bridge\Twig\Attribute\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\String\Slugger\AsciiSlugger;

#[Route(
    requirements: [
        '_locale' => 'en|fr',
    ],
)]
class OrganizationDisplayController extends AbstractController
{
    #[Route(
        path: [
            'en' => '/{_locale}/organization/{id}/{slug}/',
            'fr' => '/{_locale}/organisation/{id}/{slug}/',
        ],
        name: 'app_anonymous_organization_display_one_organization',
        methods: ['GET', 'HEAD']
    )]
    public function displayOneOrganization(
        string $_locale,
        string $slug,
        Organization $organization,
        OrganizationI18nRepository $organizationI18nRepository,
    ): Response|RedirectResponse {
        if ($slug !== $organization->getSlug()) {
            $newUrl = $this->generateUrl('app_anonymous_organization_display_one_organization', [
                '_locale' => $_locale,
                'id' => $organization->getId(),
                'slug' => $organization->getSlug(),
            ]);
            return new RedirectResponse(
                url: $newUrl,
                status: Response::HTTP_PERMANENTLY_REDIRECT // 308 Permanent Redirect
            );
        }
        $organizationDescription = $organizationI18nRepository->findOneBy([
            'organization' => $organization->getId(),
            'locale' => $_locale,
            'type' => 1
        ]); // @@@TODO improve it: if empty --> get other local

        return $this->render(
            view: 'webapp/organization/display_one_organization.html.twig',
            parameters: [
                '_locale' => $_locale,
                'organization_type' => $organization->getType()->getName(),
                'organization' => $organization,
                'organizationDescription' => $organizationDescription,
            ],
            response: new Response(),
        );
    }


    #[Route(
        path: [
            'en' => '/{_locale}/organizations/',
            'fr' => '/{_locale}/organisations/',
        ],
        name: 'app_anonymous_organization_display_all_organization',
        methods: ['GET', 'HEAD']
    )]
    #[Template('webapp/organization/display_all_organization.html.twig')]
    public function displayAllOrganization(string $_locale, OrganizationRepository $organizationRepository): array
    {
        $organizationCollection = $organizationRepository->findAllOrderingByName();
        return [
            '_locale' => $_locale,
            'organization_collection' => $organizationCollection,
        ];
    }

    #[Route(
        path: [
            'en' => '/{_locale}/organizations/companies/',
            'fr' => '/{_locale}/organisations/entreprises/',
        ],
        name: 'app_anonymous_organization_display_all_org_company',
        methods: ['GET', 'HEAD']
    )]
    public function displayAllCompanies(
        string $_locale,
        OrganizationTypeRepository $organizationTypeRepository
    ): Response|RedirectResponse {
        return $this->displayAllByType(
            _locale: $_locale,
            organizationTypeRepository: $organizationTypeRepository,
            organizationTypeName: 'company',
        );
    }

    #[Route(
        path: [
            'en' => '/{_locale}/organizations/public-sector/',
            'fr' => '/{_locale}/organisations/administration/',
        ],
        name: 'app_anonymous_organization_display_all_org_public_sector',
        methods: ['GET', 'HEAD']
    )]
    public function displayAllPublicSector(
        string $_locale,
        OrganizationTypeRepository $organizationTypeRepository,
    ): Response|RedirectResponse {
        return $this->displayAllByType(
            _locale: $_locale,
            organizationTypeRepository: $organizationTypeRepository,
            organizationTypeName: 'public_sector',
        );
    }


    #[Route(
        path: [
            'en' => '/{_locale}/organizations/non-profit/',
            'fr' => '/{_locale}/organisations/association/',
        ],
        name: 'app_anonymous_organization_display_all_org_no_profit',
        methods: ['GET', 'HEAD']
    )]
    public function displayAllNoProfitOrg(
        string $_locale,
        OrganizationTypeRepository $organizationTypeRepository,
    ): Response|RedirectResponse {
        return $this->displayAllByType(
            _locale: $_locale,
            organizationTypeRepository: $organizationTypeRepository,
            organizationTypeName: 'no_profit',
        );
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    private function displayAllByType(
        string $_locale,
        OrganizationTypeRepository $organizationTypeRepository,
        string $organizationTypeName,
    ): Response|RedirectResponse {
        $organizationType = $organizationTypeRepository->findOneBy(criteria: ['name' => $organizationTypeName]);
        if (\is_null($organizationType)) {
            return new RedirectResponse(
                url: $this->generateUrl(
                    route: 'app_anonymous_organization_display_all_organization',
                    parameters: ['_locale' => $_locale,],
                ),
                status: Response::HTTP_PERMANENTLY_REDIRECT, // 308 Permanent Redirect
            );
        }
        return $this->render(
            view: 'webapp/organization/display_all_organization_by_type.html.twig',
            parameters: [
                '_locale' => $_locale,
                'organization_collection' => $organizationType->getOrganizations(),
                'organization_type' => \strtolower($organizationTypeName),
            ],
            response: new Response(),
        );
    }
}
