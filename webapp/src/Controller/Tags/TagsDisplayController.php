<?php

namespace App\Controller\Tags;

use App\Entity\Tag;
use App\Repository\SoftwareRepository;
use App\Repository\TagRepository;
use Symfony\Bridge\Twig\Attribute\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class TagsDisplayController extends AbstractController
{
    #[Route(
        path: ['/{_locale}/tag/',],
        name: 'app_tags_display_all_fix_url_typo',
        methods: ['GET', 'HEAD']
    )]
    public function allTagsPageFixUrlTypo(string $_locale): RedirectResponse
    {
        return new RedirectResponse(
            url: $this->generateUrl(
                route: 'app_tags_display_all',
                parameters: ['_locale' => "$_locale"],
            ),
            status: Response::HTTP_PERMANENTLY_REDIRECT,
        );
    }

    #[Route(
        path: ['/{_locale}/tags/',],
        name: 'app_tags_display_all',
        methods: ['GET', 'HEAD']
    )]
    #[Template('webapp/tags/display_all_tags.html.twig')]
    public function displayAllTags(string $_locale, TagRepository $tagRepository): array
    {
        $tagsCollection = $tagRepository->findAllOrderingByName();
        return [
            'lang' => $_locale,
            'tags_count'  => count($tagsCollection),
            'tags_collection' => $tagsCollection,
        ];
        // When using the #[Template] attribute, you only need to return an array with the parameters
        // to pass to the template (the attribute is the one which will create and return the Response object).
    }

    #[Route(
        path: ['/{_locale}/tag/{id}/{slug}/',],
        name: 'app_tags_display_one_tag',
        methods: ['GET', 'HEAD']
    )]
    public function displayOneTag(
        string $_locale,
        string $slug,
        Tag $tag,
    ): Response|RedirectResponse {
        if ($slug !== $tag->getSlug()) {
            $newUrl = $this->generateUrl('app_tags_display_one_tag', [
                '_locale' => $_locale,
                'id' => $tag->getId(),
                'slug' => $tag->getSlug(),
            ]);
            return new RedirectResponse(
                url: $newUrl,
                status: Response::HTTP_PERMANENTLY_REDIRECT // 308 Permanent Redirect
            );
        }
        return $this->render(
            view: 'webapp/tags/display_one_tag.html.twig',
            parameters: [
                'lang' => $_locale,
                'tag'  => $tag,
            ],
            response: new Response(),
        );
    }
}
