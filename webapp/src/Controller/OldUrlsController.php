<?php

/*
 * This file is part of the Comptoir-du-Libre software.
 * <https://gitlab.adullact.net/Comptoir/comptoir-du-libre>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Comptoir-du-Libre is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Software;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class OldUrlsController extends AbstractController
{
///////////////////////////////////////////////////////////////////////////////////////////
//    Old URLs              Final new URLs              Primary redirection made by Symfony
//    -------------------------------------------------------------------------------------
//    /softwares/            /fr/logiciels/             /softwares
//    /fr/softwares/         /fr/logiciels/             /fr/softwares
//    /en/softwares/         /en/software/              /en/softwares
//    /softwares            /fr/logiciels/
//    /fr/softwares         /fr/logiciels/
//    /en/softwares         /en/software/
//    /softwares/418        /fr/logiciel/1/apache-superset/
//    /fr/softwares/418     /fr/logiciel/1/apache-superset/
//    /en/softwares/418     /en/software/1/apache-superset/
///////////////////////////////////////////////////////////////////////////////////////////

    #[Route(
        path: '/softwares',
        name: 'app_legacy_all_software_url',
        methods: ['GET', 'HEAD'],
    )]
    public function legacyUrlDisplayAllSoftware(): RedirectResponse
    {
        $defaultLocale = $this->getParameter('app.i18n.default_locale');
        $newUrl = $this->generateUrl('app_anonymous_software_display_all_software', [ '_locale' => "$defaultLocale",]);
        return new RedirectResponse(
            url: $newUrl,
            status: Response::HTTP_PERMANENTLY_REDIRECT // 308 Permanent Redirect
        );
    }

    #[Route(
        path: '/{_locale}/softwares',
        name: 'app_legacy_all_software_i18n_url',
        methods: ['GET', 'HEAD'],
    )]
    public function legacyI18nUrlDisplayAllSoftware($_locale): RedirectResponse
    {
        $newUrl = $this->generateUrl('app_anonymous_software_display_all_software', [ '_locale' => "$_locale",]);
        return new RedirectResponse(
            url: $newUrl,
            status: Response::HTTP_PERMANENTLY_REDIRECT // 308 Permanent Redirect
        );
    }

    /**
     * @param Software $software
     * @return RedirectResponse
     */
    #[Route(
        path: '/softwares/{old_comptoir_id}',
        name: 'app_legacy_software_url',
        methods: ['GET', 'HEAD'],
    )]
    public function legacyUrlDisplayOneSoftware(Software $software): RedirectResponse
    {
        $newUrl = $this->generateUrl('app_anonymous_software_display_one_software', [
            '_locale' => 'fr',
            'id' => $software->getId(),
            'slug' => $software->getSlug(),
        ]);
        return new RedirectResponse(
            url: $newUrl,
            status: Response::HTTP_PERMANENTLY_REDIRECT // 308 Permanent Redirect
        );
    }

    #[Route(
        path: '/{_locale}/softwares/{old_comptoir_id}',
        name: 'app_legacy_software_i18_url',
        requirements: [
            '_locale' => 'en|fr',
        ],
        methods: ['GET', 'HEAD'],
    )]
    public function legacyI18nUrlDisplayOneSoftware(string $_locale, Software $software): RedirectResponse
    {
        $newUrl = $this->generateUrl('app_anonymous_software_display_one_software', [
            '_locale' => $_locale,
            'id' => $software->getId(),
            'slug' => $software->getSlug(),
        ]);
        return new RedirectResponse(
            url: $newUrl,
            status: Response::HTTP_PERMANENTLY_REDIRECT // 308 Permanent Redirect
        );
    }
}
