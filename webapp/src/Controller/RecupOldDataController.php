<?php

/*
 * This file is part of the Comptoir-du-Libre software.
 * <https://gitlab.adullact.net/Comptoir/comptoir-du-libre>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Comptoir-du-Libre is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Organization;
use App\Entity\OrganizationI18n;
use App\Entity\OrganizationType;
use App\Entity\Software;
use App\Entity\SoftwareI18n;
use App\Entity\SoftwareScreenshot;
use App\Entity\Tag;
use App\Entity\User;
use App\Repository\OrganizationI18nRepository;
use App\Repository\OrganizationRepository;
use App\Repository\OrganizationTypeRepository;
use App\Repository\SoftwareI18nRepository;
use App\Repository\SoftwareRepository;
use App\Repository\SoftwareScreenshotRepository;
use App\Repository\TagRepository;
use App\Repository\UserRepository;
use App\Service\Images\PublicDirectory;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpClient\Exception\JsonException;
use Symfony\Component\HttpKernel\Attribute\MapQueryParameter;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\String\Slugger\AsciiSlugger;

use function Symfony\Component\String\u;

class RecupOldDataController extends AbstractController
{
    public const WEBAPP_CONTENT_IMAGES_PUBLIC_DIRECTORY = "public/images/content/";
    public const CACHE_IMPORT_DIRECTORY = "var/IMPORT_OLD";
    public const OLD_COMPTOIR_CAKEPHP_API_URL = "https://comptoir-du-libre.org/api/v1/";
    public const OLD_COMPTOIR_API_OPENDATA_URL =
        "https://comptoir-du-libre.org/public/export/comptoir-du-libre_export_v1.json";


    private HttpClientInterface $httpClient;

//    https://comptoir-du-libre.org/api/v1/screenshots.json
//    https://comptoir-du-libre.org/api/v1/screenshots.json?page=12
//    https://comptoir-du-libre.org/api/v1/SoftwaresTags.json
//    https://comptoir-du-libre.org/api/v1/softwares-tags.json?page=92



    private function getContentImagesDirectoryBasePath(): string
    {
        return $this->getParameter('kernel.project_dir') . '/' . self::WEBAPP_CONTENT_IMAGES_PUBLIC_DIRECTORY ;
    }

    private function getCacheDirectoryBasePath(): string
    {
        return $this->getParameter('kernel.project_dir') . '/' . self::CACHE_IMPORT_DIRECTORY ;
    }

    private function getCacheDirectoryBase(
        string $type
    ): string {
        return $this->getCacheDirectoryBasePath() . '/' . $type . '_data';
    }

    private function getCacheDirectoryJsonPath(
        string $type
    ): string {
        return $this->getCacheDirectoryBase($type) . '/json';
    }

    private function getCacheDirectoryImagePath(
        string $type
    ): string {
        return $this->getCacheDirectoryBase($type) . '/images';
    }

    private function getCacheDirectoryLogoPath(
        string $type
    ): string {
        return $this->getCacheDirectoryBase($type) . '/logos';
    }

    private function cacheOldDataFromCakePhpApi(
        string $type,
        bool $clearCache = false,
    ) {
        if ($type === 'screenshot_via-cakephp_api') {
            $baseUrl = self::OLD_COMPTOIR_CAKEPHP_API_URL
                . 'screenshots.json?sort=Screenshots.software_id&direction=asc&page=';
        } elseif ($type === 'tag_via-cakephp_api') {
            $baseUrl = self::OLD_COMPTOIR_CAKEPHP_API_URL . 'softwares-tags.json?page=';
        } else {
            throw new Exception("Type [ $type] is not allowed");
        }

        $filesystem = new Filesystem();
        $cacheDirectory = $this->getCacheDirectoryJsonPath($type);
        if ($clearCache === true) {
            $filesystem->remove($cacheDirectory);
        }

        if (!is_dir($cacheDirectory)) {
            dump("----> Get " . strtoupper($type) . " data, and cache it.");
            try {
                $filesystem->mkdir("$cacheDirectory", 0770);
            } catch (IOExceptionInterface $exception) {
                echo "An error occurred while creating your directory at " . $exception->getPath();
            }

            $jsonRawCollection = [];
            $jsonDataCollection = [];

            for ($i = 1; $i < 1_000; $i++) {
                $url = $baseUrl . $i;
                $response = $this->httpClient->request('GET', "$url");
                $statusCode = $response->getStatusCode(); // 200
                if ($statusCode === 200) {
                    $contentType = $response->getHeaders()['content-type'][0]; // application/json
                    $jsonDataCollection[$i] = $response->toArray();
                    $jsonRawCollection[$i] = $response->getContent();

                    $filesystem->dumpFile("$cacheDirectory/$type" . "_page-$i.json", $response->getContent());
                    dump("--- $i -----> $statusCode - $url");
                    dump($response->toArray());
                } elseif ($statusCode === 404) {
                    break;
                }
            }
            dump($jsonDataCollection);
        } else {
            dump("----> Use " . strtoupper($type) . " data cache");
        }
    }


    private function cacheOldDataFromCustomApi(
        string $type = 'software_via-opendata_public',
        bool $clearCache = false,
    ) {

        if ($type === 'software_via-opendata_public') {
            $url = self::OLD_COMPTOIR_API_OPENDATA_URL;
        } elseif ($type === 'software_via-opendata_private') {
            $url = $this->getParameter('app.old_export_software_json_url');
        } elseif ($type === 'business-mapping_via-opendata_private') {
            $url = $this->getParameter('app.old_export_business-mapping_json_url');
        } else {
            throw new Exception("Type [ $type] is not allowed");
        }

        $filesystem = new Filesystem();
        $cacheDirectory = $this->getCacheDirectoryJsonPath($type);
        if ($clearCache === true) {
            $filesystem->remove($cacheDirectory);
        }

        if (!is_dir($cacheDirectory)) {
            $debugUrl = parse_url($url, PHP_URL_SCHEME) . '//'
                . parse_url($url, PHP_URL_HOST)
                . parse_url($url, PHP_URL_PATH);
            echo "<br>----> Get " . strtoupper($type) . " data, and cache it. <br><small>$debugUrl</small> <br>";
            try {
                $filesystem->mkdir("$cacheDirectory", 0770);
            } catch (IOExceptionInterface $exception) {
                echo "An error occurred while creating your directory at " . $exception->getPath();
            }
            $response = $this->httpClient->request('GET', $url);
            $statusCode = $response->getStatusCode(); // 200
            $contentType = $response->getHeaders()['content-type'][0]; // application/json
            try {
                if ('' === $jsonRaw = $response->getContent(true)) {
                    throw new JsonException("Response body is empty [ $url]");
                }
                // decode ----> TODO refactor
                try {
                    $jsonData = json_decode(
                        $jsonRaw,
                        true,
                        512,
                        \JSON_BIGINT_AS_STRING | \JSON_THROW_ON_ERROR
                    );
                } catch (\JsonException $e) {
                    throw new JsonException(
                        $e->getMessage() . sprintf(
                            ' for "%s".',
                            $response->getInfo('url')
                        ),
                        $e->getCode()
                    );
                }
                if (!\is_array($jsonData)) {
                    throw new JsonException(sprintf(
                        'JSON content was expected to decode to an array, "%s" returned for "%s".',
                        get_debug_type($jsonData),
                        $this->getInfo('url')
                    ));
                }
            } catch (ClientExceptionInterface $e) {
            } catch (DecodingExceptionInterface $e) {
            } catch (RedirectionExceptionInterface $e) {
            } catch (ServerExceptionInterface $e) {
            } catch (\Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface $e) {
            }
            $filesystem->dumpFile("$cacheDirectory/$type.json", $jsonRaw);
        } else {
            dump("----> Use " . strtoupper($type) . " data cache");
        }
    }


    #[Route(
        path: '/sysamdin/migration/',
        name: 'app_sysamdin_migration_index',
        methods: ['GET']
    )]
    public function index(): Response
    {
        $content = '';
        $content .= '<h1>Import </h1>';
        $content .= '<a href="/sysamdin/migration/make_cache_data_of_all_old_data">
                                Create CACHE if not exist</a><br>';
        $content .= '<a href="/sysamdin/migration/make_cache_data_of_all_old_data?clearCache=true">
                            Regenerate CACHE (force clear)</a><br>';
        $content .= '<hr>';
        $content .= '<a href="/sysamdin/migration/recup_old_data_of_screenshots?clearCache=true">
                        Get all Screenshot files with CACHE clear</a><br>';
        $content .= '<a href="/sysamdin/migration/recup_old_data_of_screenshots">Get all Screenshot files</a><br>';
        $content .= '<a href="/sysamdin/migration/recup_old_data_of_software_logo">Get all Software logos</a><br>';
        $content .= '<hr>';
        $content .= '<a href="/sysamdin/migration/recup_old_data_user_photos">Get all Users logos</a><br>';
        $content .= '<hr>';
        $content .= '<a href="/sysamdin/migration/recup_old_data_of_all_software">Import SOFTWARE</a><br>';
        $content .= '<a href="/sysamdin/migration/recup_old_data_of_all_software?clearCache=true">
                            Import SOFTWARE with CACHE clear</a><br>';
        $content .= '<hr>';
        $content .= '<a href="/sysamdin/migration/recup_old_data_of_tags">Import TAGS</a><br>';
        $content .= '<a href="/sysamdin/migration/recup_old_data_of_tags?clearCache=true">
                            Import TAGS with CACHE clear</a><br>';
        $content .= '<hr>';
        $content .= '<a href="/sysamdin/migration/import_old_data_of_screenshots">Import software screenshots</a><br>';
        $content .= '<a href="/sysamdin/migration/import_old_data_of_software_logo">Import software logos</a><br>';
        $content .= '<a href="/sysamdin/migration/import_old_data_of_user_photo">Import user avatar</a><br>';
        $content .= '<hr>';
        $content .= '<a href="/sysamdin/migration/recup_old_data_users_accounts">Import USER accounts</a><br>';
        $content .= '<a href="/sysamdin/migration/recup_old_data_organizations">Import ORGANIZATIONS</a><br>';
        $content .= '<hr>';


        return new Response(content: $content,);
    }

    #[Route(
        path: '/sysamdin/migration/make_cache_data_of_all_old_data',
        name: 'app_sysamdin_migration_make_cache_data_of_all_old_data',
        methods: ['GET']
    )]
    public function makeCacheOfAllOldData(
        PublicDirectory $publicDirectory,
        Filesystem $filesystem,
        HttpClientInterface $httpClient,
        #[MapQueryParameter] bool $clearCache = false,
    ) {

        $publicDirectory->createPublicDirectory($this->getContentImagesDirectoryBasePath());

        $this->httpClient = $httpClient;
        $this->cacheOldDataFromCustomApi(type: "software_via-opendata_public", clearCache: $clearCache,);
        $this->cacheOldDataFromCustomApi(type: "software_via-opendata_private", clearCache: $clearCache,);
        $this->cacheOldDataFromCustomApi(type: "business-mapping_via-opendata_private", clearCache: $clearCache,);
        $this->cacheOldDataFromCakePhpApi(type: "screenshot_via-cakephp_api", clearCache: $clearCache,);
        $this->cacheOldDataFromCakePhpApi(type: "tag_via-cakephp_api", clearCache: $clearCache,);
        exit();
    }

    #[Route(
        path: '/sysamdin/migration/recup_old_data_of_tags',
        name: 'app_sysamdin_migration_recup_old_data_of_tags',
        methods: ['GET']
    )]
    public function importOldDataTagsFromCakePhpApi(
        Filesystem $filesystem,
        HttpClientInterface $httpClient,
        SoftwareRepository $softwareRepository,
        #[MapQueryParameter] bool $clearCache = false,
    ) {
        $type = 'tag_via-cakephp_api';
        $this->httpClient = $httpClient;
        $this->cacheOldDataFromCakePhpApi(type: "$type", clearCache: $clearCache,);

        $finder = new Finder();
        $cacheJsonDirectory = $this->getCacheDirectoryJsonPath($type);
        $finder->files()->in("$cacheJsonDirectory")->name('*.json')->sortByName(true);
        dump($finder->hasResults());
        foreach ($finder as $file) {
            $absoluteFilePath = $file->getRealPath();
            $fileNameWithExtension = $file->getRelativePathname();
            $data = json_decode($file->getContents());
            dump("---------------------> $fileNameWithExtension");
            // dump($data);
            // ...
        }
        exit();
    }

    // TODO remove target screenshots directory before import screenshot files
    #[Route(
        path: '/sysamdin/migration/import_old_data_of_screenshots',
        name: 'app_sysamdin_migration_import_old_data_of_screenshots',
        methods: ['GET']
    )]
    public function importOldDataScreenshotsFromCakePhpApi(
        PublicDirectory $publicDirectory,
        Filesystem $filesystem,
        HttpClientInterface $httpClient,
        SoftwareRepository $softwareRepository,
        SoftwareScreenshotRepository $screenshotsRepository,
        #[MapQueryParameter] bool $clearCache = false,
    ) {
        $type = 'screenshot_via-cakephp_api';
        $this->httpClient = $httpClient;
        // $this->cacheOldDataFromCakePhpApi(type: "$type", clearCache: $clearCache,);
        $cacheDirectory = $this->getCacheDirectoryBase($type);
        $cacheConsolidateDataFilePath = "$cacheDirectory/$type" . "_consolidate.json";
        $cacheImageDirectoryPath = "$cacheDirectory/images";


        //// /////////////////////////////////////////////////////////////////////////////
        $screenshots = $this->getJsonDataFromFile("$cacheConsolidateDataFilePath");
        $slugger = new AsciiSlugger();
        foreach ($screenshots as $softwareData) {
            dump($softwareData);
            /*     array:3 [▼
                  "software_id" => 2
                  "software_name" => "Pastell"
                  "screenshots" => array:15 [▼
                    0 => array:8 [▼
                      "id" => 178
                      "software_id" => 2
                      "url_directory" => "files/Screenshots"
                      "created" => "2016-06-14T11:58:52+00:00"
                      "modified" => "2016-06-14T11:58:52+00:00"
                      "name" => "Screenshot_pastell_rechercheavancée.jpg"
                      "photo" => "1465898332_Screenshot_pastell_rechercheavancée.jpg"     */

            $softwareId = $softwareData['software_id'];
            $publicDirectory->createPublicDirectory($this->getContentImagesDirectoryBasePath() . "software/");
            foreach ($softwareData['screenshots'] as $screenshotData) {
                $imageFile = $screenshotData['photo'];
                $sourceFilePath = "$cacheImageDirectoryPath/$imageFile";
                $imageInfo =  pathinfo($sourceFilePath);

                $slug = $slugger->slug($imageFile);
                $newImageName = $slugger->slug($imageInfo['filename'])->toString() . '.' . $imageInfo['extension'] ;
                dump($imageFile); // 1465898332_Screenshot_pastell_rechercheavancée.jpg
                dump($slug);      // 1465898332-Screenshot-pastell-rechercheavancee-jpg
                dump($imageInfo);      // 1465898332-Screenshot-pastell-rechercheavancee-jpg
                dump($newImageName);      // 1465898332-Screenshot-pastell-rechercheavancee-jpg
                // $filesystem->dumpFile("$cacheImageDirectoryPath/$imageFile", $response->getContent());

                $software = $softwareRepository->findOneBy(['id' => $softwareId]);

                if (
                    \is_null(
                        $screenshotsRepository->findOneBy(
                            ['software' => $softwareId, 'fileName' => "$newImageName"]
                        )
                    )
                ) {
                    $screenshot = new SoftwareScreenshot();
                    $screenshot->setFileName("$newImageName");
                    $screenshot->setSoftware($software);
                    $screenshot->setCreatedAt(new \DateTimeImmutable());
                    $screenshotsRepository->save($screenshot, true);
                }

                $targetDirectory = $this->getContentImagesDirectoryBasePath() . "software/$softwareId/screenshots" ;
                $publicDirectory->createPublicDirectory(
                    $this->getContentImagesDirectoryBasePath() . "software/$softwareId/"
                );
                $publicDirectory->createPublicDirectory(pathDirectory: "$targetDirectory");
                // $publicDirectory->createPublicDirectory(pathDirectory: "$targetDirectory", clean: true);
                // TODO remove target screenshots directory before import screenshot files


                $filesystem->copy(
                    originFile: "$cacheImageDirectoryPath/$imageFile",
                    targetFile: "$targetDirectory/$newImageName"
                );
            }
        }
        exit();
    }



    // TODO normaliser les extenstions
    // TODO vérifier le type MINE
    // TODO problème logo du logiciel "GestSup" (au moment de la récupération)
    //
    #[Route(
        path: '/sysamdin/migration/import_old_data_of_software_logo',
        name: 'app_sysamdin_migration_import_old_data_of_software_logo',
        methods: ['GET']
    )]
    public function importSoftwareLogos(
        PublicDirectory $publicDirectory,
        Filesystem $filesystem,
        HttpClientInterface $httpClient,
        SoftwareRepository $softwareRepository,
        #[MapQueryParameter] bool $clearCache = false,
    ) {
        $slugger = new AsciiSlugger();
        $publicDirectory->createPublicDirectory($this->getContentImagesDirectoryBasePath() . "software/");

        $this->httpClient = $httpClient;
        $type = "software_via-opendata_private";
        $this->cacheOldDataFromCustomApi(type: $type, clearCache: $clearCache,);

        $cacheDirectory = $this->getCacheDirectoryBase($type);
        $cacheImageDirectoryPath = "$cacheDirectory/logos";
        echo $cacheImageDirectoryPath . "<hr>";
        print_r($cacheImageDirectoryPath);
        $jsonFilePath = $this->getCacheDirectoryJsonPath($type) . "/$type.json";
        $data = $this->getJsonDataFromFile($jsonFilePath);
        foreach ($data['softwares'] as $softwareData) {
            $softwareId = $softwareData['id'];
            $softwareName = $softwareData['name'];
            $srcLogoFile = $softwareData['logo_file'];
            $srcLogoPath = "$cacheImageDirectoryPath/$softwareId/$srcLogoFile";
            $srcLogoInfo =  pathinfo($srcLogoPath);
            echo "<hr>$softwareId - $softwareName<br>";
            echo "<pre>" . print_r($srcLogoInfo, true) . "</pre>";

            if (!isset($srcLogoInfo['extension'])) {  // TODO fixme
                continue;
            } elseif (!is_file($srcLogoPath)) {
                continue;
            }

            $software = $softwareRepository->findOneBy(['id' => $softwareId]);
            if (\is_object($software) && !empty($srcLogoFile)) {
                $publicDirectory->createPublicDirectory(
                    $this->getContentImagesDirectoryBasePath() . "software/$softwareId/"
                );
                $targetDirectory = $this->getContentImagesDirectoryBasePath() . "software/$softwareId/logo";
                $publicDirectory->createPublicDirectory(pathDirectory: "$targetDirectory", clean: true);

                $slug = $slugger->slug($srcLogoFile);
                $newImageName = $slugger->slug($srcLogoInfo['filename'])->toString() . '.' . $srcLogoInfo['extension'] ;
                $filesystem->copy(
                    originFile: "$srcLogoPath",
                    targetFile: "$targetDirectory/$newImageName"
                );
                $software->setLogo("$newImageName");
                $softwareRepository->save($software, true);
                echo "<pre>" . print_r(pathinfo("$targetDirectory/$newImageName"), true) . "</pre>";
            }
        }
        exit();
    }


    #[Route(
        path: '/sysamdin/migration/recup_old_data_user_photos',
        name: 'app_sysamdin_migration_recup_old_data_user_photos',
        methods: ['GET']
    )]
    public function getOldDataUserAvatar(
        Filesystem $filesystem,
        HttpClientInterface $httpClient,
        UserRepository $userRepository,
        OrganizationRepository $organizationRepository,
        OrganizationTypeRepository $organizationTypeRepository,
        OrganizationI18nRepository $organizationI18nRepository,
        #[MapQueryParameter] bool $clearCache = false,
    ) {
        $this->httpClient = $httpClient;
        $type = "users_via-opendata_private";

        $jsonFilePath = '../../comptoir_users_dev_2024.10.30_11h00.38.json';
        if (false === $filesystem->exists("$jsonFilePath")) {
            throw new IOException(sprintf('Failed to read "%s": ', $jsonFilePath));
        }
        $data = $this->getJsonDataFromFile($jsonFilePath);

        $imageDirectoryPath = $this->getCacheDirectoryLogoPath($type);
        if (!is_dir("$imageDirectoryPath")) {
            dump("----> Get " . strtoupper($type) . " data, and cache it.");
            try {
                $filesystem->mkdir("$imageDirectoryPath", 0770);
            } catch (IOExceptionInterface $exception) {
                echo "An error occurred while creating your directory at " . $exception->getPath();
            }
        }



        foreach ($data['users'] as $userData) {
//           dump($userData); exit();
                // "user_logo_directory" => "files/Users/photo/1078/avatar"
                // "user_logo_image" => "logo_ville.jfif"
                // "user_logo_url" => "https://comptoir-du-libre.org/img/files/Users/photo/1078/avatar/logo_ville.jfif"

            // Extract data
            $userOldComptoirId = $userData['user_id'];
            $userName = trim($userData['user_name']);
            $logoDirectory = trim($userData['user_logo_directory']);
            $logoFile = trim($userData['user_logo_image']);
            $logoUrl = trim($userData['user_logo_url']);
            if (empty($logoUrl)) {
                continue;
            }
            echo "$userOldComptoirId - $userName<br>";
            echo $logoDirectory . "<br>";
            echo $logoFile . "<br>";

            $response = $this->httpClient->request('GET', "$logoUrl");
            $statusCode = $response->getStatusCode(); // 200
            echo $statusCode . " $logoUrl<br>";
            if ($statusCode === 200) {
                if (isset($response->getHeaders()['content-type'][0])) {
                    $contentType = $response->getHeaders()['content-type'][0]; // application/json
                    echo $contentType . "<br>";
                } else { // TODO add log
                    echo 'DEBUG ----> Content Type' . "<br>";
                }

                if (!is_dir("$imageDirectoryPath/$userOldComptoirId")) {
                    try {
                        $filesystem->mkdir("$imageDirectoryPath/$userOldComptoirId", 0770);
                    } catch (IOExceptionInterface $exception) {
                        echo "An error occurred while creating your directory at " . $exception->getPath();
                    }
                }

                $imagePath = "$imageDirectoryPath/$userOldComptoirId/$logoFile";
                $filesystem->dumpFile("$imagePath", $response->getContent());
                echo $imagePath . "<hr>";
            } elseif ($statusCode === 404) {
            }
            echo "<hr>";
        }
        exit();
    }




    #[Route(
        path: '/sysamdin/migration/recup_old_data_of_software_logo',
        name: 'app_sysamdin_migration_recup_old_data_of_software_logo',
        methods: ['GET']
    )]
    public function getOldDataSofwareLogos(
        Filesystem $filesystem,
        HttpClientInterface $httpClient,
        #[MapQueryParameter] bool $clearCache = false,
    ) {
        $this->httpClient = $httpClient;
        $type = "software_via-opendata_private";
        $this->cacheOldDataFromCustomApi(type: $type, clearCache: $clearCache,);

        $imageDirectoryPath = $this->getCacheDirectoryLogoPath($type);
        if (!is_dir("$imageDirectoryPath")) {
            dump("----> Get " . strtoupper($type) . " data, and cache it.");
            try {
                $filesystem->mkdir("$imageDirectoryPath", 0770);
            } catch (IOExceptionInterface $exception) {
                echo "An error occurred while creating your directory at " . $exception->getPath();
            }
        }

        $jsonFilePath = $this->getCacheDirectoryJsonPath($type) . "/$type.json";
        $data = $this->getJsonDataFromFile($jsonFilePath);
        foreach ($data['softwares'] as $softwareData) {
            $softwareId = $softwareData['id'];
            $softwareName = $softwareData['name'];
            $logoDirectory = $softwareData['logo_directory'];
            $logoFile = $softwareData['logo_file'];
            $logoUrl = $softwareData['logo_url'];

            echo "$softwareId - $softwareName<br>";
            echo $logoDirectory . "<br>";
            echo $logoFile . "<br>";
            $statusCode = 900;
            if (!empty($logoUrl)) {
                $response = $this->httpClient->request('GET', "$logoUrl");
                $statusCode = $response->getStatusCode(); // 200
            }

            echo $statusCode . " $logoUrl<br>";
            if ($statusCode === 200) {
                if (isset($response->getHeaders()['content-type'][0])) {
                    $contentType = $response->getHeaders()['content-type'][0]; // application/json
                    echo $contentType . "<br>";
                } else { // TODO add log
                    echo 'DEBUG ----> Content Type' . "<br>";
                }

                if (!is_dir("$imageDirectoryPath/$softwareId")) {
                    try {
                        $filesystem->mkdir("$imageDirectoryPath/$softwareId", 0770);
                    } catch (IOExceptionInterface $exception) {
                        echo "An error occurred while creating your directory at " . $exception->getPath();
                    }
                }

                $imagePath = "$imageDirectoryPath/$softwareId/$logoFile";
                $filesystem->dumpFile("$imagePath", $response->getContent());
                echo $imagePath . "<hr>";
            } elseif ($statusCode === 900) {
                echo "pas d'image associé à ce logiciel";
            } elseif ($statusCode === 404) {
            }
            echo "<hr>";
        }
        exit();
    }

    #[Route(
        path: '/sysamdin/migration/recup_old_data_of_screenshots',
        name: 'app_sysamdin_migration_recup_old_data_of_screenshots',
        methods: ['GET']
    )]
    public function getOldDataScreenshotsFromCakePhpApi(
        Filesystem $filesystem,
        HttpClientInterface $httpClient,
        #[MapQueryParameter] bool $clearCache = false,
    ) {
        $type = 'screenshot_via-cakephp_api';
        $this->httpClient = $httpClient;
        $this->cacheOldDataFromCakePhpApi(type: "$type", clearCache: $clearCache,);
        $cacheDirectory = $this->getCacheDirectoryBase($type);
        $cacheConsolidateDataFilePath = "$cacheDirectory/$type" . "_consolidate.json";

        //// 1. CONSOLIDATE JSON DATA ///////////////////////////////////////////////////////////////////////////////
        $finder = new Finder();
        $cacheJsonDirectory = $this->getCacheDirectoryJsonPath($type);
        $finder->files()->in("$cacheJsonDirectory")->name('*.json')->sortByName(true);
        $screenshots = [];
        foreach ($finder as $file) {
            $absoluteFilePath = $file->getRealPath();
            $fileNameWithExtension = $file->getRelativePathname();
            $jsonData = $this->getJsonDataFromRawContent(
                jsonRaw: $file->getContents(),
                jsonErrorContext: $absoluteFilePath,
            )['screenshots'];
            // dump("---------------------> $fileNameWithExtension");
            foreach ($jsonData as $screenshotData) {
                // dump($screenshotData);
                $softwareId = $screenshotData['software']['id'];
                $softwareName = $screenshotData['software']['softwarename'];
                $screenshots[$softwareId]['software_id'] = $softwareId;
                $screenshots[$softwareId]['software_name'] = $softwareName;
                $screenshots[$softwareId]['screenshots'][] = $screenshotData;
            }
//            0 => array [     "id" => 225
//                            "software_id" => 12
//                            "url_directory" => "files/Screenshots"
//                            "created" => "2016-06-14T16:28:00+00:00"
//                            "modified" => "2016-06-14T16:28:00+00:00"
//                            "name" => "Screenshot_xemelios_export.png"
//                            "photo" => "1465914480_Screenshot_xemelios_export.png"
//                            "software" => array [
//                              "id" => 12
//                              "softwarename" => "Xemelios"
        }
        // dump($screenshots);
        $filesystem->dumpFile("$cacheConsolidateDataFilePath", json_encode($screenshots));
        //////////////////////////////////// END CONSOLIDATE JSON DATA ///////////////////////////////////////////////

        //// 2. DOWLOAD ALL IMAGES FILES /////////////////////////////////////////////////////////////////////////////
        $screenshots = $this->getJsonDataFromFile("$cacheConsolidateDataFilePath");
        dump($screenshots);
        $baseScreenshotsUrls = 'https://comptoir-du-libre.org/img/files/Screenshots/';
        foreach ($screenshots as $softwareData) {
            dump($softwareData);
            /*     array:3 [▼
                  "software_id" => 2
                  "software_name" => "Pastell"
                  "screenshots" => array:15 [▼
                    0 => array:8 [▼
                      "id" => 178
                      "software_id" => 2
                      "url_directory" => "files/Screenshots"
                      "created" => "2016-06-14T11:58:52+00:00"
                      "modified" => "2016-06-14T11:58:52+00:00"
                      "name" => "Screenshot_pastell_rechercheavancée.jpg"
                      "photo" => "1465898332_Screenshot_pastell_rechercheavancée.jpg"     */

            $imageDirectoryPath = "$cacheDirectory/images";
            if (!is_dir("$imageDirectoryPath")) {
                dump("----> Get " . strtoupper($type) . " data, and cache it.");
                try {
                    $filesystem->mkdir("$imageDirectoryPath", 0770);
                } catch (IOExceptionInterface $exception) {
                    echo "An error occurred while creating your directory at " . $exception->getPath();
                }
            }
            foreach ($softwareData['screenshots'] as $screenshotData) {
                $imageFile = $screenshotData['photo'];
                $url = $baseScreenshotsUrls . $screenshotData['photo'];
                $response = $this->httpClient->request('GET', "$url");
                $statusCode = $response->getStatusCode(); // 200
                dump($url);
                dump($statusCode);
                if ($statusCode === 200) {
                    $contentType = $response->getHeaders()['content-type'][0]; // application/json
                    dump($contentType);

                    $filesystem->dumpFile("$imageDirectoryPath/$imageFile", $response->getContent());
                } elseif ($statusCode === 404) {
                }
            }
        }
        exit();
    }

    /**
     * @throws JsonException
     */
    private function getJsonDataFromRawContent(string $jsonRaw, string $jsonErrorContext = ''): array
    {
        try {
            $jsonData = \json_decode($jsonRaw, true, 512, \JSON_BIGINT_AS_STRING | \JSON_THROW_ON_ERROR);
        } catch (\JsonException $e) {
            throw new JsonException($e->getMessage() . sprintf(' for "%s".', $jsonErrorContext), $e->getCode());
        }
        if (!\is_array($jsonData)) {
            throw new JsonException(sprintf(
                'JSON content was expected to decode to an array, "%s" returned for "%s".',
                get_debug_type($jsonData),
                $jsonErrorContext
            ));
        }
        return $jsonData;
    }

    /**
     * @throws JsonException
     */
    private function getJsonDataFromFile(string $jsonFilePath): array
    {
        $jsonRaw = \file_get_contents("$jsonFilePath");
        return $this->getJsonDataFromRawContent($jsonRaw);
    }

    /**
     * @throws TransportExceptionInterface
     */
    #[Route(
        path: '/sysamdin/migration/recup_old_data_of_all_software',
        name: 'app_sysamdin_migration_recup_old_data_of_all_software',
        methods: ['GET']
    )]
    public function importOldDataSoftwareFromOpenData(
        HttpClientInterface $httpClient,
        SoftwareRepository $softwareRepository,
        SoftwareI18nRepository $softwareI18nRepository,
        TagRepository $tagRepository,
        #[MapQueryParameter] bool $clearCache = false,
    ): Response {
        \set_time_limit(300);

        $this->httpClient = $httpClient;
        $type = "software_via-opendata_private";
        $this->cacheOldDataFromCustomApi(type: $type, clearCache: $clearCache,);

        $cacheDirectory = $this->getCacheDirectoryJsonPath($type);
        $jsonFilePath = "$cacheDirectory/$type.json";
        $data = $this->getJsonDataFromFile($jsonFilePath);

        // Logiciels déjà existants en DB
        $allSoftware = $softwareRepository->findAll();

        // 1er import : créer à vide les logiciels en DB (pour avoir des ID identiques)
        if (count($allSoftware) === 0) {
            $oldIds = [];
            foreach ($data['softwares'] as $softwareData) {
                $oldIds[$softwareData['id']] = $softwareData['id'] . " - " . $softwareData['name'];
            }
            ksort($oldIds); //
            dump(end($oldIds));
            dump(array_key_last($oldIds));
            dump($oldIds);
            $max = array_key_last($oldIds);
            for ($i = 1; $i <= $max; $i++) {
                $software = new Software();
                $software->setName("Logiciel $i");
                $software->setSlug("logiciel_deja_existant_$i");
                if (!isset($oldIds[$i])) {
                    $software->setSlug("IMPORT_A_SUPPRIMER");
                }
                $software->setCreatedAt(new \DateTimeImmutable());
                $software->setUpdatedAt(new \DateTimeImmutable());
                $software->setWebsiteUrl("https://$i.example.org");
                $software->setRepositoryUrl("https://$i.source.example.org");
                $softwareRepository->save($software, true);
            }

            $listOfsoftwareToBeDeleted = $softwareRepository->findBy(['slug' => 'IMPORT_A_SUPPRIMER']);
            foreach ($listOfsoftwareToBeDeleted as $softwareToBeDeleted) {
                $softwareRepository->remove($softwareToBeDeleted, true);
            }
            dump(count($listOfsoftwareToBeDeleted));
            dump($listOfsoftwareToBeDeleted);
        } else {
            dump(count($allSoftware) . " logiciels déjà présent DB");
        }

//              "api_documentation" => array:4 [▶]
//              "data_documentation" => array:1 [▶]
//              "data_statistics" => array:4 [▶]
//              "data_date" => "2024-07-22T16:38:01+00:00"
//              "date_of_export" => "2024-07-22T16:38:01+00:00"
//              "number_of_software" => 627
//              "softwares" => array:627 [▼
//                        0 => array:11 [▼
//                          "id" => 72
//                          "name" => "7-zip"
//                          "url" => "https://comptoir-du-libre.org/fr/softwares/72"
//                          "i18n_url" => array:2 [ …2]
//                          "licence" => "GNU LGPL"
//                          "license" => "GNU LGPL"
//                          "created" => "2017-02-07T22:46:57+00:00"
//                          "modified" => "2024-05-26T11:13:45+00:00"
//                          "external_resources" => array:7 [ …7]
//                                  "website" => "https://www.centreon.com/"
//                                  "repository" => "https://github.com/centreon/centreon"
//                                  "framalibre" => []
//                                  "cnll" => []
//                                  "sill" => array:3 [▶]
//                                  "wikidata" => array:3 [▶]
//                                  "wikipedia" => array:2 [▶]
//                          "providers" => array:1 [ …1]
//                          "users" => array:47 [ …47]


        $tags = [];
        foreach ($data['softwares'] as $softwareData) {
            $oldId = $softwareData['id'];
            $name = $softwareData['name'];
            $description = $softwareData['description'];
            $createdAt = $softwareData['created'];
            $updatedAt = $softwareData['modified'];
            $license = $softwareData['license'];
            $websiteUrl = $softwareData['external_resources']['website'];
            $repositoryUrl = $softwareData['external_resources']['repository'];

            $slugger = new AsciiSlugger();
            $slug = u($slugger->slug("$name")->toString())->lower()->toString();

            $createdAt = new \DateTimeImmutable($createdAt);
            $updatedAt = new \DateTimeImmutable($updatedAt);




            dump('---------------------------------');
//          $software = $softwareRepository->findOneBy(['old_comptoir_id' => $oldId]);
            $software = $softwareRepository->findOneBy(['id' => $oldId]);
            dump($software);
            if (\is_null($software)) {
                $software = new Software();
                $software->setName($name);
                $software->setSlug($slug);
                $software->setCreatedAt($createdAt);
                $software->setUpdatedAt($updatedAt);
                $software->setWebsiteUrl($websiteUrl);
                $software->setRepositoryUrl($repositoryUrl);
                $software->setOldComptoirId($oldId);
                $software->setOldLicenseName($license);
                $softwareRepository->save($software, true);
                dump('----->  ADD');
                dump($software);
                continue;
            }

            $softwareDescriptionFR = $softwareI18nRepository->findOneBy([
                'software' => $software->getId(),
                'locale' => 'fr',
                'type' => 1
            ]);

            dump($software);
            if (\is_null($softwareDescriptionFR)) {
                $softwareDescriptionFR = new SoftwareI18n();
                $softwareDescriptionFR->setSoftware($software);
                $softwareDescriptionFR->setLocale('fr');
                $softwareDescriptionFR->setType(1);
//                $softwareDescriptionFR->setText("$description");
//                $softwareI18nRepository->save($softwareDescriptionFR, true);
                dump('----->  ADD Software description');
                dump($softwareDescriptionFR);
            } elseif ($softwareDescriptionFR->getText() !== $description) { // Fixme
//                $softwareDescriptionFR->setText("$description");
//                $softwareI18nRepository->save($softwareDescriptionFR, true);
            }
            $softwareDescriptionFR->setText("$description");
            $softwareI18nRepository->save($softwareDescriptionFR, true);


//            $softwareDescription = $softwareI18nRepository->findOneBy([
//                'software' => $software->getId(),
//                'locale' => $_locale,
//                'type' => 1
//            ]);
//
            $update = false;
            if ($software->getName() !== $name) {
                $software->setName($name);
                $update = true;
            }
            if ($software->getSlug() !== $slug) {
                $software->setSlug($slug);
                $update = true;
            }
            if ($software->getWebsiteUrl() !== $websiteUrl) {
                $software->setWebsiteUrl($websiteUrl);
                $update = true;
            }
            if ($software->getRepositoryUrl() !== $repositoryUrl) {
                $software->setRepositoryUrl($repositoryUrl);
                $update = true;
            }
            if ($software->getUpdatedAt()->format('Y-m-d H:i:s') !== $updatedAt->format('Y-m-d H:i:s')) {
                $software->setUpdatedAt($updatedAt);
                $update = true;
            }
            if ($software->getCreatedAt()->format('Y-m-d H:i:s') !== $createdAt->format('Y-m-d H:i:s')) {
                $software->setCreatedAt($createdAt);
                $update = true;
            }
            if (is_null($software->getOldComptoirId())) {
                $software->setOldComptoirId($oldId);
                $update = true;
            }
            if (is_null($software->getOldLicenseName())) {
                $software->setOldLicenseName($license);
                $update = true;
            }


            if (true === $update) {
                $softwareRepository->save($software, true);
                dump('----->  UPDATE' . $software->getName());
                // dump($software);
            }

            if (count($softwareData['tags']) > 0) {
                foreach ($softwareData['tags'] as $currentTag) {
                    if (!isset($tags[$currentTag['id']])) {
                        $tags[$currentTag['id']] = $currentTag;
                    }
                    $tags[$currentTag['id']]['softwareCollection'][$oldId] = $software;
                }
            }
        }

        // Import des tags
        foreach ($tags as $tagData) {
            $oldTagId = $tagData['id'];
            $tagName = $tagData['name'];
            $tagCreated = $tagData['created'];
            $tagModified = $tagData['modified'];
            $descriptionFr = $tagData['description_i18n_fr'];
            $descriptionEn = $tagData['description_i18n_en'];
            $softwareCollection = $tagData['softwareCollection'];
            $slugger = new AsciiSlugger();
            $tagSlug = u($slugger->slug("$tagName")->toString())->lower()->toString();

            dump($tagName);
            dump($softwareCollection);
            $tag = $tagRepository->findOneBy(['old_comptoir_id' => $oldTagId]);
            dump($tag);
            if (\is_null($tag)) {
                $tag = new Tag();
                dump('----->  ADD TAG :' . $tagName);
            } else {
                dump('----->  UPDATE TAG : ' . $tagName);
            }
            $tag->setName($tagName);
            $tag->setSlug($tagSlug);
            $tag->setOldComptoirId($oldTagId);
//              $tag->setCreatedAt($createdAt);
//              $tag->setUpdatedAt($updatedAt);
            if (count($softwareCollection) > 0) {
                foreach ($softwareCollection as $software) {
                    $tag->addSoftware($software);
                }
            }
            $tagRepository->save($tag, true);
        }

        exit();
        return new Response(
            content: "$statusCode - $contentType - <pre>$content</pre>",
            status: 200
        );
    }


    #[Route(
        path: '/sysamdin/migration/recup_old_data_users_accounts',
        name: 'app_sysamdin_migration_recup_old_data_users_accounts',
        methods: ['GET']
    )]
    public function getOldDataUsersAccount(
        Filesystem $filesystem,
        HttpClientInterface $httpClient,
        UserRepository $userRepository,
        #[MapQueryParameter] bool $clearCache = false,
    ) {
        $type = "users_via-opendata_private";
        $jsonFilePath = '../../comptoir_users_dev.json';
        if (false === $filesystem->exists("$jsonFilePath")) {
            throw new IOException(sprintf('Failed to read "%s": ', $jsonFilePath));
        }
        $data = $this->getJsonDataFromFile($jsonFilePath);
        $roles = [];
        foreach ($data['users'] as $userData) {
//            dump($userData);
            // Extract data
            $userOldComptoirId = $userData['user_id'];
            $userCreated = $userData['user_created'];
            $userModified = $userData['user_modified'];
            $userEmail = trim($userData['user_email']);
            $userPassword = $userData['user_password'];
            $userOldComptoirRole = $userData['user_role'];

            // Compute data
            $accountCreateAt = new \DateTimeImmutable($userCreated);
            $accountUpdateAt = new \DateTimeImmutable($userModified);
            if (!isset($roles[$userOldComptoirRole])) {
                $roles[$userOldComptoirRole] = 0;
            }
            $roles[$userOldComptoirRole] += 1;
            $userRoles = [];
            if ($userOldComptoirRole === 'admin') {
                $userRoles = ['ROLE_SUPERADMIN'];
            }

            // Import new data ---> TODO use OLD user ID instead of user EMAIL
            $user = $userRepository->findOneBy(['email' => $userEmail]);
//          $user = $userRepository->findOneBy(['old_comptoir_id' => $userOldComptoirId]);
            if (\is_null($user)) {
                $user = new User();
                $user->setEmail($userEmail);
                $user->setPassword($userPassword);
                $user->setOldComptoirId($userOldComptoirId);
                $user->setCreatedAt($accountCreateAt);
                $user->setUpdatedAt($accountUpdateAt);
                $user->setRoles($userRoles);
                $userRepository->save($user, true);
                echo "<br>----->  ADD $userEmail";
                continue;
            }

            // Update data
            if ($user->getEmail() !== $userEmail) {
                $user->setEmail($userEmail);
                $update = true;
            }
            if ($user->getPassword() !== $userPassword) {
                $user->setPassword($userPassword);
                $update = true;
            }
            if ($user->getOldComptoirId() !== $userOldComptoirId) {
                $user->setOldComptoirId($userOldComptoirId);
                $update = true;
            }
            if ($user->getCreatedAt()->format('Y-m-d H:i:s') !== $accountCreateAt->format('Y-m-d H:i:s')) {
                $user->setCreatedAt($accountCreateAt);
                $update = true;
            }
            if ($user->getUpdatedAt()->format('Y-m-d H:i:s') !== $accountUpdateAt->format('Y-m-d H:i:s')) {
                $user->setUpdatedAt($accountUpdateAt);
                $update = true;
            }
            if ($user->getRoles() !== $userRoles) {
                $user->setRoles($userRoles);
                $update = true;
            }

            if (true === $update) {
                $userRepository->save($user, true);
                echo "<br>----->  UPDATE $userEmail";
            }
        }
        dump($user);
        dump($roles);
        exit();
    }


    #[Route(
        path: '/sysamdin/migration/recup_old_data_organizations',
        name: 'app_sysamdin_migration_recup_old_data_organizations',
        methods: ['GET']
    )]
    public function getOldDataOrganizations(
        Filesystem $filesystem,
        HttpClientInterface $httpClient,
        UserRepository $userRepository,
        OrganizationRepository $organizationRepository,
        OrganizationTypeRepository $organizationTypeRepository,
        OrganizationI18nRepository $organizationI18nRepository,
        #[MapQueryParameter] bool $clearCache = false,
    ) {
        $type = "users_via-opendata_private";
        $jsonFilePath = '../../comptoir_users_dev_2024.10.30_11h00.38.json';
        if (false === $filesystem->exists("$jsonFilePath")) {
            throw new IOException(sprintf('Failed to read "%s": ', $jsonFilePath));
        }
        $data = $this->getJsonDataFromFile($jsonFilePath);
        $roles = [];
        foreach ($data['users'] as $orgData) {
//           dump($orgData);
//           exit();

            // Extract data
            $userEmail = $orgData['user_email'];
            $orgOldComptoirId = $orgData['user_id'];
            $orgTypeName = $orgData['user_type'];
            $orgCreated = $orgData['user_created'];
            $orgModified = $orgData['user_modified'];
            $orgName = trim($orgData['user_name']);
            $orgWebsite = trim($orgData['user_website']);
            $orgDescription = trim($orgData['user_description']);
//          $orgLogo = trim($orgData['user_logo_image']);

            if ($orgTypeName === 'Person') {
                continue;
            }

            // Compute data
            $slugger = new AsciiSlugger();
            $orgSlug = u($slugger->slug("$orgName")->toString())->lower()->toString();
            $accountCreateAt = new \DateTimeImmutable($orgCreated);
            $accountUpdateAt = new \DateTimeImmutable($orgModified);


            // Import new data ---> TODO use OLD user ID instead of user EMAIL
            $user = $userRepository->findOneBy(['email' => $userEmail]);
//          $user = $userRepository->findOneBy(['old_comptoir_id' => $userOldComptoirId]);
            if (\is_null($user)) {
                echo "ERROR : $userEmail account not found";
                // TODO log
            }

            if ($orgTypeName === 'Company') {
                $orgTypeName = 'company';
            } elseif ($orgTypeName === 'Administration') {
                $orgTypeName = 'public_sector';
            } elseif ($orgTypeName === 'Association') {
                $orgTypeName = 'no_profit';
            } else {
                throw new \Exception("Organization type [ $orgTypeName ] not yet define");
            }
            $orgType = $organizationTypeRepository->findOneBy(['name' => $orgTypeName]);
            if (\is_null($orgType)) {
                $orgType = new OrganizationType();
                $orgType->setName($orgTypeName);
                $organizationTypeRepository->save($orgType, true);
                echo "<br>----->  ADD [ $orgTypeName ] type";
            }

            $organization = $organizationRepository->findOneBy(['old_comptoir_id' => $orgOldComptoirId]);
            if (\is_null($organization)) {
                $organization = new Organization();
                $organization->setName($orgName);
                $organization->setSlug($orgSlug);
//              $organization->setLogo($orgLogo);
                $organization->setWebsite($orgWebsite);
                $organization->setOldComptoirId($orgOldComptoirId);
                $organization->setCreatedAt($accountCreateAt);
                $organization->setUpdatedAt($accountUpdateAt);
                $organization->setType($orgType);
                $organizationRepository->save($organization, true);
                if (!empty($orgDescription)) {
                    $organizationI18n = new OrganizationI18n();
                    $organizationI18n->setOrganization($organization);
                    $organizationI18n->setLocale('fr');
                    $organizationI18n->setType(1);
                    $organizationI18n->setText($orgDescription);
                    $organizationI18nRepository->save($organizationI18n, true);
                }
                echo "<br>----->  ADD $orgOldComptoirId - $orgName";
                continue;
            }


            // Update data
//            if ($user->getEmail() !== $userEmail) {
//                $user->setEmail($userEmail);
//                $update = true;
//            }
//            if ($user->getPassword() !== $userPassword) {
//                $user->setPassword($userPassword);
//                $update = true;
//            }
//            if ($user->getOldComptoirId() !== $userOldComptoirId) {
//                $user->setOldComptoirId($userOldComptoirId);
//                $update = true;
//            }
//            if ($user->getCreatedAt()->format('Y-m-d H:i:s') !== $accountCreateAt->format('Y-m-d H:i:s')) {
//                $user->setCreatedAt($accountCreateAt);
//                $update = true;
//            }
//            if ($user->getUpdatedAt()->format('Y-m-d H:i:s') !== $accountUpdateAt->format('Y-m-d H:i:s')) {
//                $user->setUpdatedAt($accountUpdateAt);
//                $update = true;
//            }
//            if ($user->getRoles() !== $userRoles) {
//                $user->setRoles($userRoles);
//                $update = true;
//            }
//
//            if (true === $update) {
//                $userRepository->save($user, true);
//                echo "<br>----->  UPDATE $userEmail";
//            }
        }
        dump($user);
        dump($roles);
        exit();
    }



    #[Route(
        path: '/sysamdin/migration/import_old_data_of_user_photo',
        name: 'app_sysamdin_migration_import_old_data_of_user_photo',
        methods: ['GET']
    )]
    public function getImportOldUserPhoto(
        PublicDirectory $publicDirectory,
        Filesystem $filesystem,
        HttpClientInterface $httpClient,
        UserRepository $userRepository,
        OrganizationRepository $organizationRepository,
        OrganizationTypeRepository $organizationTypeRepository,
        OrganizationI18nRepository $organizationI18nRepository,
        #[MapQueryParameter] bool $clearCache = false,
    ) {
        $type = "users_via-opendata_private";
        $jsonFilePath = '../../comptoir_users_dev_2024.10.30_11h00.38.json';
        if (false === $filesystem->exists("$jsonFilePath")) {
            throw new IOException(sprintf('Failed to read "%s": ', $jsonFilePath));
        }

        $cacheDirectory = $this->getCacheDirectoryBase($type);
        $cacheImageDirectoryPath = "$cacheDirectory/logos";
        echo $cacheImageDirectoryPath . "<hr>";
        print_r($cacheImageDirectoryPath);

        $data = $this->getJsonDataFromFile($jsonFilePath);
        $roles = [];
        $slugger = new AsciiSlugger();
        foreach ($data['users'] as $orgData) {
//           dump($orgData);
//           exit();

            // Extract data
            $userOldComptoirId = $orgData['user_id'];
            $userTypeName = $orgData['user_type'];
            $userName = trim($orgData['user_name']);
            $userLogo = trim($orgData['user_logo_image']);
            $srcLogoFile = $userLogo;
            $srcLogoPath = "$cacheImageDirectoryPath/$userOldComptoirId/$userLogo";
            $srcLogoInfo =  pathinfo($srcLogoPath);
            echo "<hr>$userOldComptoirId - $userTypeName - $userName<br>";
            echo "<pre>" . print_r($srcLogoInfo, true) . "</pre>";

            if (!isset($srcLogoInfo['extension'])) {  // TODO fixme
                continue;
            } elseif (!is_file($srcLogoPath)) {
                continue;
            }

            if ($userTypeName === 'Person') {
                $user = $userRepository->findOneBy(['old_comptoir_id' => $userOldComptoirId]);
                continue;
            }


            // Import new data
//            if (\is_null($user)) {
//                echo "ERROR : $userEmail account not found";
//                exit();
//            }

            $organization = $organizationRepository->findOneBy(['old_comptoir_id' => $userOldComptoirId]);
            dump($organization);
            dump($srcLogoFile);
            dump(!\is_null($organization));
            dump(!empty($srcLogoFile));
            dump(!\is_null($organization) && !empty($srcLogoFile));

            if (!\is_null($organization) && !empty($srcLogoFile)) {
                dump(!\is_null($organization) && !empty($srcLogoFile));
                exit();
                $organizationId = $organization->getId();
                $publicDirectory->createPublicDirectory(
                    $this->getContentImagesDirectoryBasePath() . "organization/$organizationId/"
                );
                $targetDirectory = $this->getContentImagesDirectoryBasePath() . "organization/$organizationId/logo";
                $publicDirectory->createPublicDirectory(pathDirectory: "$targetDirectory", clean: true);

                $slug = $slugger->slug($srcLogoFile);
                $newImageName = $slugger->slug($srcLogoInfo['filename'])->toString() . '.' . $srcLogoInfo['extension'] ;
                $filesystem->copy(
                    originFile: "$srcLogoPath",
                    targetFile: "$targetDirectory/$newImageName"
                );
                $organization->setLogo("$newImageName");
                $organizationRepository->save($organization, true);
                echo "<pre>" . print_r(pathinfo("$targetDirectory/$newImageName"), true) . "</pre>";
            }
        }
        exit();
    }
}
