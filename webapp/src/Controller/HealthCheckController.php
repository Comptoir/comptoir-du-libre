<?php

/*
 * This file is part of the Comptoir-du-Libre software.
 * <https://gitlab.adullact.net/Comptoir/comptoir-du-libre>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Comptoir-du-Libre is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Controller;

use Doctrine\DBAL\Connection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\String\Slugger\AsciiSlugger;

class HealthCheckController extends AbstractController
{
    #[Route(
        path:'/health-check',
        name: 'app_health_check',
        methods: ['GET', 'HEAD']
    )]
    public function healthCheck(Request $request, Connection $dbConnection): Response
    {
        $displayRelease = $this->getParameter('app.software.release.display.public');
        $softwareRelease = $this->getParameter('app.software.release');
        $softwareName = $this->getParameter('app.software.name');

        $httpCode = Response::HTTP_OK;
        $dbStatusCode = "SUCCESSFUL";
        try {
            $dbConnection->fetchAllAssociative('SELECT * FROM doctrine_migration_versions');
        } catch (\Exception $e) {
            $httpCode = Response::HTTP_SERVICE_UNAVAILABLE;
            $dbStatusCode = "FAILED";
        }

        $slugSoftwareName = strtolower((new AsciiSlugger())->slug("$softwareName")->toString());
        $headers = [];
        $headers["x-$slugSoftwareName-database-status"] = "DB_CONNECTION_$dbStatusCode";
        if ($displayRelease === true) {
            $headers["x-$slugSoftwareName-version"] = $softwareRelease;
        }

        if ($request->getMethod() === "HEAD") {
            return new Response(status: $httpCode, headers: $headers);
        } else {
            return $this->render(
                view: 'healthcheck.html.twig',
                parameters: [
                    'dbStatusCode' => $dbStatusCode,
                ],
                response: new Response(status: $httpCode, headers: $headers)
            );
        }
    }
}
