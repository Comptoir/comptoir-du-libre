<?php

/*
 * This file is part of the Comptoir-du-Libre software.
 * <https://gitlab.adullact.net/Comptoir/comptoir-du-libre>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Comptoir-du-Libre is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Controller;

use App\Entity\SoftwareScreenshot;
use App\Repository\OrganizationRepository;
use App\Repository\OrganizationTypeRepository;
use App\Repository\SoftwareRepository;
use App\Repository\SoftwareScreenshotRepository;
use App\Repository\TagRepository;
use App\Repository\UserRepository;
use Nelmio\SecurityBundle\ExternalRedirect\ExternalRedirectResponse;
use Symfony\Bridge\Twig\Attribute\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\String\Slugger\AsciiSlugger;

class HomeController extends AbstractController
{
    #[Route(
        path: '/admin/configuration/phpinfo',
        name: 'app_admin_config_phpinfo',
        methods: ['GET']
    )]
    public function displayPhpInfo(): Response
    {
        ob_start();
        phpinfo();
        $strPhpInfo = ob_get_contents();
        ob_clean();
        $response = new Response();
        $response->setContent($strPhpInfo);
        return $response;
    }

    /**
     * @throws TransportExceptionInterface
     */
    #[Route(
        path: '/admin/configuration/check-sending-email',
        name: 'app_admin_config_send_testing_email',
        methods: ['GET']
    )]
    public function checkSendingEmail(MailerInterface $mailer): Response
    {
        $emailFrom = $this->getParameter('app.email.from');
        $emailTo =  $this->getParameter('app.email.alerting_to');
        $appName =  $this->getParameter('app.name');
        $appShortName =  $this->getParameter('app.shortname');
        $email = (new Email())
            ->from(new Address($emailFrom))
            ->to(new Address($emailTo))
            ->subject("$appShortName - checkSendingEmail")
            ->text("$appName - checkSendingEmail")
            ->html("<p>$appName - checkSendingEmail</p>");
        $mailer->send($email);
        return new Response(
            content: "$appName / - checkSendingEmail : a email was send from $emailFrom to $emailTo",
            status: 200
        );
    }

    #[Route(
        '/',
        name: 'app_home',
        methods: ['GET', 'HEAD']
    )]
    public function homePage(Request $request): RedirectResponse
    {
//      dump($this->getParameter('app.i18n.default_locale'));
//      dump($request->getLocale());
//      dump($request->getDefaultLocale());
        $defaultLocale = $this->getParameter('app.i18n.default_locale');
        $softwareName = $this->getParameter('app.software.name');
        $slugSoftwareName = strtolower((new AsciiSlugger())->slug("$softwareName")->toString()) ;
        $headers = [
            "x-$slugSoftwareName-webapp" => true,
        ];
        return new RedirectResponse(
            url: $this->generateUrl(route: 'app_home_i18n', parameters: ['_locale' => "$defaultLocale"],),
            status: Response::HTTP_PERMANENTLY_REDIRECT,
            headers: $headers,
        );
//        $this->redirect(url:'/fr/', status: 302);
//        $this->redirectToRoute(
//            route: 'app_home_i18n',
//            parameters: ['lang' => 'fr'],
//            status: Response::HTTP_PERMANENTLY_REDIRECT
//        );
    }

    #[Route(
        path: '/{_locale}/',
        name: 'app_home_i18n',
        requirements: [
            '_locale' => 'en|fr',
        ],
        methods: ['GET', 'HEAD'],
    )]
    public function i18nHomePage(
        string $_locale,
        UserRepository $userRepository,
        OrganizationRepository $organizationRepository,
        OrganizationTypeRepository $organizationTypeRepository,
        SoftwareRepository $softwareRepository,
        TagRepository $tagRepository,
        SoftwareScreenshotRepository $screenshotRepository,
    ): Response {
        $numberOfUsers = $userRepository->count();
        $numberOfOrganizations = $organizationRepository->count();
        $numberOfCompany = 0;
        $numberOfPublicSector = 0;
        $numberOfNoProfit = 0;
        $organizationTypeCompany = $organizationTypeRepository->findOneBy([ 'name' => 'company']);
        $organizationTypeNoProfit = $organizationTypeRepository->findOneBy([ 'name' => 'no_profit']);
        $organizationTypePublicSector = $organizationTypeRepository->findOneBy([ 'name' => 'public_sector']);
        if (!\is_null($organizationTypeCompany)) {
            $numberOfCompany = $organizationTypeCompany->getOrganizations()->count();
        }
        if (!\is_null($organizationTypeNoProfit)) {
            $numberOfNoProfit = $organizationTypeNoProfit->getOrganizations()->count();
        }
        if (!\is_null($organizationTypePublicSector)) {
            $numberOfPublicSector = $organizationTypePublicSector->getOrganizations()->count();
        }

        $numberOfSoftware = $softwareRepository->count();
        $numberOfTags = $tagRepository->count();
        $numberOfScreenshots = $screenshotRepository->count();

        $softwareName = $this->getParameter('app.software.name');
        $slugSoftwareName = strtolower((new AsciiSlugger())->slug("$softwareName")->toString()) ;
        $headers = [
            "x-$slugSoftwareName-webapp" => true,
        ];
        return $this->render(
            view: 'home.html.twig',
            parameters: [
                '_locale' => $_locale,
                'number_of_users' => $numberOfUsers,
                'number_of_organizations' => $numberOfOrganizations,
                'number_of_org_company' => $numberOfCompany,
                'number_of_org_public_sector' => $numberOfPublicSector,
                'number_of_org_no_profit' => $numberOfNoProfit,
                'number_of_software' => $numberOfSoftware,
                'number_of_tags' => $numberOfTags,
                'number_of_screenshots' => $numberOfScreenshots,
            ],
            response: new Response(headers: $headers)
        );
    }


    #[Route(
        '/{_locale}/pages/accessibility',
        name: 'app_page_accessibility',
        methods: ['GET', 'HEAD']
    )]
    #[Template('public_static_pages/accessibility.html.twig')]
    public function accessibilityPage($_locale): array
    {
        return ['_locale' => $_locale];
        // When using the #[Template] attribute, you only need to return an array with the parameters
        // to pass to the template (the attribute is the one which will create and return the Response object).
    }

    #[Route(
        '/{_locale}/pages/legal',
        name: 'app_page_legal',
        methods: ['GET', 'HEAD']
    )]
    #[Template('public_static_pages/legal.html.twig')]
    public function legalPage($_locale): array
    {
        return ['_locale' => $_locale];
        // When using the #[Template] attribute, you only need to return an array with the parameters
        // to pass to the template (the attribute is the one which will create and return the Response object).
    }

    #[Route(
        '/{_locale}/pages/about',
        name: 'app_page_about',
        methods: ['GET', 'HEAD']
    )]
    #[Template('public_static_pages/about.html.twig')]
    public function aboutPage($_locale): array
    {
        return ['_locale' => $_locale];
        // When using the #[Template] attribute, you only need to return an array with the parameters
        // to pass to the template (the attribute is the one which will create and return the Response object).
    }

    #[Route(
        '/{_locale}/pages/opendata',
        name: 'app_page_opendata',
        methods: ['GET', 'HEAD']
    )]
    #[Template('public_static_pages/opendata.html.twig')]
    public function opendataPage($_locale): array
    {
        return ['_locale' => $_locale];
        // When using the #[Template] attribute, you only need to return an array with the parameters
        // to pass to the template (the attribute is the one which will create and return the Response object).
    }


    #[Route(
        '/{_locale}/pages/contact',
        name: 'app_page_contact',
        methods: ['GET', 'HEAD']
    )]
    #[Template('public_static_pages/contact.html.twig')]
    public function contactPage($_locale): ExternalRedirectResponse
    {
        ////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Nelmio\SecurityBundle\ExternalRedirect\ExternalRedirectResponse
        // see: https://symfony.com/bundles/NelmioSecurityBundle/current/index.html#external-redirects-detection
        ////////////////////////////////////////////////////////////////////////////////////////////////////////
        return new ExternalRedirectResponse(
            url:'https://adullact.org/contact',
            allowedHosts: ['adullact.org'],  # TODO extract to $url
            status: Response::HTTP_TEMPORARY_REDIRECT, // 307 = HTTP_TEMPORARY_REDIRECT
        );
    }
}
