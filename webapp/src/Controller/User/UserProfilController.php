<?php

/*
 * This file is part of the Comptoir-du-Libre software.
 * <https://gitlab.adullact.net/Comptoir/comptoir-du-libre>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Comptoir-du-Libre is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Controller\User;

use App\Entity\User;
use App\Form\User\ChangePasswordFormType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Contracts\Translation\TranslatorInterface;

#[\Symfony\Component\Routing\Attribute\Route(
    requirements: [
        '_locale' => 'en|fr',
    ],
)]
class UserProfilController extends AbstractController
{
    #[Route(
        path: '/{_locale}/user/account',
        name: 'app_user_account',
        methods: ['GET']
    )]
    public function userDisplayMyProfile(AuthenticationUtils $authenticationUtils, string $_locale): Response
    {
        // $user = $this->getUser();
        return $this->render('user/account.html.twig', ['_locale' => $_locale]);
    }

    #[Route(
        path: '/{_locale}/user/account/password',
        name: 'app_user_account_change_password',
        methods: ['GET', 'POST']
    )]
    public function userChangeMyPassowrd(
        Request $request,
        TranslatorInterface $translator,
        UserPasswordHasherInterface $userPasswordHasher,
        UserRepository $userRepository
    ): Response {
        $minPasswordLength = $this->getParameter('app.user_config.min_password_length');
        $form = $this->createForm(
            type: ChangePasswordFormType::class,
            options: ['min_password_length' => $minPasswordLength]
        );
        $form->handleRequest($request);

        // Check form errors
        $saveNewPasword = false;
        $isInvalidCurrentPassword = false;
        if ($form->isSubmitted()) {
            $user = $this->getUser();
            $currentPlainPassword = $form->get('currentPlainPassword')->getData();
            if ($form->isValid() === false | !$userPasswordHasher->isPasswordValid($user, $currentPlainPassword)) {
                $message = $translator->trans('common.validator.help.password.error');
                $this->addFlash('danger', "$message");
            }

            if (!$userPasswordHasher->isPasswordValid($user, $currentPlainPassword)) {
                $isInvalidCurrentPassword = true;
            } elseif ($form->isValid()) {
                $saveNewPasword = true;
            }
        }

        // Update user password
        if ($saveNewPasword === true) {
            // Encode the plain password
            $user->setPassword($userPasswordHasher->hashPassword($user, $form->get('newPlainPassword')->getData()));

            // Update user account datetimes
            $now = new \DateTime();
            $user->setLastPassowrdUpdate($now);
            $user->setUpdatedAt($now);

            $userRepository->save($user, true);
            $successMsg =  $translator->trans('user.change_password.success');
            $this->addFlash('success', "$successMsg");
            return $this->redirectToRoute('app_user_account', [], Response::HTTP_SEE_OTHER);
        }

        // Display empty form or form with error messages
        return $this->render('user/account_change-password.html.twig', [
            'changePasswordForm' => $form->createView(),
            'isInvalidCurrentPassword' => $isInvalidCurrentPassword,
        ]);
    }
}
