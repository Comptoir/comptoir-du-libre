<?php

/*
 * This file is part of the Comptoir-du-Libre software.
 * <https://gitlab.adullact.net/Comptoir/comptoir-du-libre>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Comptoir-du-Libre is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Controller\User;

use App\Entity\User;
use App\Form\User\SignupFormType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

#[\Symfony\Component\Routing\Attribute\Route(
    requirements: [
        '_locale' => 'en|fr',
    ],
)]
class SignupController extends AbstractController
{
    #[Route(
        path: '/{_locale}/account/signup',
        name: 'app_account_signup_init',
        methods: ['GET']
    )]
    public function signupInit(
        Request $request,
        TokenGeneratorInterface $tokenGenerator,
        AuthorizationCheckerInterface $authChecker,
    ): RedirectResponse {
        if (true === $authChecker->isGranted('ROLE_USER')) {
            return $this->redirectToRoute(route: 'app_user_account', status: Response::HTTP_SEE_OTHER);
        }

        $session = $request->getSession();
        if ($session->has('signup_url_token') === true) {
            $signupAntispamToken = $session->get('signup_url_token');
        } else {
            $signupAntispamToken = $tokenGenerator->generateToken();
            $session->set('signup_url_token', $signupAntispamToken);
            $session->set('signup_form_token', $tokenGenerator->generateToken());
        }

        return $this->redirectToRoute(
            route: 'app_account_signup',
            parameters: ['signupAntispamToken' => $signupAntispamToken],
            status: Response::HTTP_SEE_OTHER
        );
    }

    #[Route(
        path: '/{_locale}/account/signup/t/{signupAntispamToken}',
        name: 'app_account_signup',
        methods: ['GET', 'POST']
    )]
    public function signup(
        string $signupAntispamToken,
        Request $request,
        FormFactoryInterface $formFactory,
        UserRepository $userRepository,
        UserPasswordHasherInterface $userPasswordHasher,
        TranslatorInterface $translator,
        AuthorizationCheckerInterface $authChecker,
    ): Response|RedirectResponse {
        if (true === $authChecker->isGranted('ROLE_USER')) {
             return $this->redirectToRoute(route: 'app_user_account', status: Response::HTTP_SEE_OTHER);
        }

        // Check session signup_*_token
        $session = $request->getSession();
        $validTokenInbUrl = true;
        if ($session->has('signup_url_token') === false) {
            $validTokenInbUrl = false;
        } elseif ($session->get('signup_url_token') !== $signupAntispamToken) {
            $validTokenInbUrl = false;
            $session->remove('signup_url_token');
            $session->remove('signup_form_token');
        }

        // If signup_url_token is not valid
        if ($validTokenInbUrl === false) {
            return $this->redirectToRoute(route: 'app_account_signup_init', status: Response::HTTP_SEE_OTHER);
        }

        // Generate form
        $signupFormToken =  $session->get('signup_form_token');
        $minPasswordLength = $this->getParameter('app.user_config.min_password_length');
        $form = $formFactory->createNamed(
            name: "signup_$signupFormToken",
            type: SignupFormType::class,
            options: ['min_password_length' => $minPasswordLength]
        );
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $userEmail = $form->get('login')->getData();

            // Check if the user already exists in the database
            $user = $userRepository->findOneByEmail($form->get('login')->getData());
            if ($user instanceof User) { // TODO improve this case
                $session->remove('signup_url_token');  // Cleanup session data
                $session->remove('signup_form_token'); // Cleanup session data
                $errorMsgId =  'signup.error.account_already_exists';
                $this->addFlash('danger', $translator->trans("$errorMsgId"));
                return $this->redirectToRoute(route: 'app_account_login', status: Response::HTTP_SEE_OTHER);
            }

            // Add new user
            $now = new \DateTimeImmutable();
            $user = new User();
            $user->setUpdatedAt($now);
            $user->setCreatedAt($now);
            $user->setLastPassowrdUpdate($now);
            $user->setEmail($userEmail);
            $user->setPassword($userPasswordHasher->hashPassword($user, $form->get('plainPassword')->getData()));
            $userRepository->save($user, true);  // TODO add new User property [ enabled account ]

            // Cleanup session data
            $session->remove('signup_url_token');
            $session->remove('signup_form_token');

            // Redirect user and display success message
            $this->addFlash('success', $translator->trans('signup.success'));
            return $this->redirectToRoute('app_account_login', [], Response::HTTP_SEE_OTHER);
        }

        // Display registration form (first display or user errors in form)
        return $this->render('user/signup.html.twig', ['signupForm' => $form->createView(),]);
    }
}
