<?php

/*
 * This file is part of the Comptoir-du-Libre software.
 * <https://gitlab.adullact.net/Comptoir/comptoir-du-libre>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Comptoir-du-Libre is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Controller\Software;

use App\Entity\Software;
use App\Repository\SoftwareI18nRepository;
use App\Repository\SoftwareRepository;
use App\Repository\SoftwareScreenshotRepository;
use Nelmio\SecurityBundle\ExternalRedirect\ExternalRedirectResponse;
use Symfony\Bridge\Twig\Attribute\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\String\Slugger\AsciiSlugger;

#[Route(
    requirements: [
        '_locale' => 'en|fr',
    ],
)]
class SoftwareDisplayController extends AbstractController
{
    #[Route(
        path: [
            'en' => '/{_locale}/software/{id}/{slug}/',
            'fr' => '/{_locale}/logiciel/{id}/{slug}/',
        ],
        name: 'app_anonymous_software_display_one_software',
        methods: ['GET', 'HEAD']
    )]
    public function displayOneSoftware(
        string $_locale,
        string $slug,
        Software $software,
        SoftwareScreenshotRepository $screenshotRepository,
        SoftwareI18nRepository $softwareI18nRepository,
    ): Response|RedirectResponse {
        if ($slug !== $software->getSlug()) {
            $newUrl = $this->generateUrl('app_anonymous_software_display_one_software', [
                '_locale' => $_locale,
                'id' => $software->getId(),
                'slug' => $software->getSlug(),
            ]);
            return new RedirectResponse(
                url: $newUrl,
                status: Response::HTTP_PERMANENTLY_REDIRECT // 308 Permanent Redirect
            );
        }
        $screenshots = $screenshotRepository->findBy(['software' => $software->getId()]);
        $softwareDescription = $softwareI18nRepository->findOneBy([
            'software' => $software->getId(),
            'locale' => $_locale,
            'type' => 1
        ]); // @@@TODO improve it: if empty --> get other local
        return $this->render(
            view: 'webapp/software/display_one_software.html.twig',
            parameters: [
                '_locale' => $_locale,
                'software' => $software,
                'screenshots' => $screenshots,
                'softwareDescription' => $softwareDescription,
            ],
            response: new Response(),
        );
    }


    #[Route(
        path: [
            'en' => '/{_locale}/software/',
            'fr' => '/{_locale}/logiciels/',
        ],
        name: 'app_anonymous_software_display_all_software',
        methods: ['GET', 'HEAD']
    )]
    #[Template('webapp/software/display_all_software.html.twig')]
    public function displayAllSoftware(string $_locale, SoftwareRepository $softwareRepository): array
    {
        $softwareCollection = $softwareRepository->findAllOrderingByName();
        return [
            '_locale' => $_locale,
            'software_count'  => count($softwareCollection),
            'software_collection' => $softwareCollection,
        ];
        // When using the #[Template] attribute, you only need to return an array with the parameters
        // to pass to the template (the attribute is the one which will create and return the Response object).
    }
}
