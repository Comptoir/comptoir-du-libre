<?php

/*
 * This file is part of the Comptoir-du-Libre software.
 * <https://gitlab.adullact.net/Comptoir/comptoir-du-libre>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Comptoir-du-Libre is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\PasswordHasherFactory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasher;

class AppUserFixtures extends Fixture
{
    public const PASSWORD_1_PLAINTEXT = "superadmin-webapp_PaSsWord_IsNotSoSecretChangeIt";
    public const PASSWORD_1_HASH = '$2y$13$7C4MvPgCNUzWP3kiS/fqPeUr2BBjrEut1lbKqhXxGnBji71l0YE1e';

    public const USER_REFERENCE_0 = 'test_user_0_webapp@example.org';
    public const USER_REFERENCE_1 = 'test_user_1_webapp@example.org';
    public const USER_REFERENCE_2 = 'test_user_2_webapp@example.org';
    public const USER_REFERENCE_3 = 'test_user_3_webapp@example.org';
    public const USER_REFERENCE_4 = 'test_user_4_webapp@example.org';
    public const USER_REFERENCE_5 = 'test_user_5_webapp@example.org';
    public const USER_REFERENCE_6 = 'test_user_6_webapp@example.org';
    public const USER_REFERENCE_7 = 'test_user_7_webapp@example.org';
    public const USER_REFERENCE_8 = 'test_user_8_webapp@example.org';
    public const USER_REFERENCE_9 = 'test_user_9_webapp@example.org';


    public const USER_REFERENCE_ADMIN_1 = 'test_admin_1_webapp@example.org';
    public const USER_REFERENCE_ADMIN_2 = 'test_admin_2_webapp@example.org';
    public const USER_REFERENCE_ADMIN_3 = 'test_admin_3_webapp@example.org';

    public function load(ObjectManager $manager): void
    {
        $user = new User();
        $user->setEmail("superadmin_webapp@example.org");
        $user->setPassword(self::PASSWORD_1_HASH);
//        $user->setIsActive(true);
        $user->setRoles(['ROLE_SUPERADMIN']);
        $manager->persist($user);

        // create 3 user with ROLE_USER
        $users = [];
        for ($i = 0; $i < 11; $i++) {
            $user = new User();
            $user->setEmail("test_user_${i}_webapp@example.org");
//          $user->setPassword("user_${i}_password");
            $user->setPassword(self::PASSWORD_1_HASH); // identical to test_superadmin_webapp@example.org pass

//            $user->setIsActive(false);
            $manager->persist($user);
            $manager->flush();
            $users[$i] = $user;
        }
        $this->addReference(self::USER_REFERENCE_0, $users[1]);
        $this->addReference(self::USER_REFERENCE_1, $users[1]);
        $this->addReference(self::USER_REFERENCE_2, $users[2]);
        $this->addReference(self::USER_REFERENCE_3, $users[3]);
        $this->addReference(self::USER_REFERENCE_4, $users[4]);
        $this->addReference(self::USER_REFERENCE_5, $users[5]);
        $this->addReference(self::USER_REFERENCE_6, $users[6]);
        $this->addReference(self::USER_REFERENCE_7, $users[7]);
        $this->addReference(self::USER_REFERENCE_8, $users[8]);
        $this->addReference(self::USER_REFERENCE_9, $users[9]);

        // create 3 user with ROLE_ADMIN
        $users = [];
        for ($i = 1; $i < 4; $i++) {
            $user = new User();
            $user->setEmail("test_admin_${i}_webapp@example.org");
//          $user->setPassword("admin_${i}_password");
            $user->setPassword(self::PASSWORD_1_HASH); // identical to test_superadmin_webapp@example.org pass
//            $user->setIsActive(true);
            $user->setRoles(['ROLE_ADMIN']);
            $manager->persist($user);
            $manager->flush();
            $users[$i] = $user;
        }
        $this->addReference(self::USER_REFERENCE_ADMIN_1, $users[1]);
        $this->addReference(self::USER_REFERENCE_ADMIN_2, $users[2]);
        $this->addReference(self::USER_REFERENCE_ADMIN_3, $users[3]);
    }
}
