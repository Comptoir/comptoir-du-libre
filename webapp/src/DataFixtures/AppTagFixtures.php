<?php

/*
 * This file is part of the Comptoir-du-Libre license.
 * <https://gitlab.adullact.net/Comptoir/comptoir-du-libre>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Comptoir-du-Libre is free license: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free License Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this license. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Tag;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppTagFixtures extends Fixture
{
    public const TAG_COUNT = 5;
    public const TAG_0 = 'Bureautique';
    public const TAG_1 = 'Framework PHP';
    public const TAG_2 = 'Base de données';
    public const TAG_3 = 'SGBD';
    public const TAG_4 = 'Dataviz';

    private ObjectManager $manager ;

    public function load(ObjectManager $manager): void
    {
        $this->manager = $manager;
        $tagsList = [];
        $tagsList[0] = [
            'name' => 'Bureautique',
            'slug' => 'bureautique',
        ];
        $tagsList[1] = [
            'name' => 'Framework PHP',
            'slug' => 'framework-php',
        ];
        $tagsList[2] = [
            'name' => 'Base de données',
            'slug' => 'base-de-donnees',
        ];
        $tagsList[3] = [
            'name' => 'SGBD',
            'slug' => 'sgbd',
        ];
        $tagsList[4] = [
            'name' => 'Dataviz',
            'slug' => 'dataviz',
            'description' => "La data visualization, dataviz ou encore visualisation des données
            est une forme de communication  qui consiste à traduire les informations
            contenues dans des données à l'aide d'outils visuels.",
        ];

        foreach ($tagsList as $key => $licenseData) {
            $tagsList[$key] = $this->addTag($licenseData);
        }
        $this->addReference(self::TAG_0, $tagsList[0]);
        $this->addReference(self::TAG_1, $tagsList[1]);
        $this->addReference(self::TAG_2, $tagsList[2]);
        $this->addReference(self::TAG_3, $tagsList[3]);
        $this->addReference(self::TAG_4, $tagsList[3]);
    }

    public function addTag(array $tagsList): Tag
    {
        $tag = new Tag();
        $tag->setName($tagsList['name']);
        $tag->setSlug($tagsList['slug']);
        $this->manager->persist($tag);
        $this->manager->flush();
        return $tag;
    }
}
