<?php

/*
 * This file is part of the Comptoir-du-Libre software.
 * <https://gitlab.adullact.net/Comptoir/comptoir-du-libre>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Comptoir-du-Libre is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Software;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppSoftwareFixtures extends Fixture
{
    public const SOFTWARE_0 = 'Apache Superset';
    public const SOFTWARE_1 = 'API Platform';

    private ObjectManager $manager ;

    public function load(ObjectManager $manager): void
    {
        $this->manager = $manager;
        $softwareList = [];
        $softwareList[0] = [
            'name' => 'Apache Superset',
            'slug' => 'apache-superset',
            'createdAt' => '2021-02-26 02:15:20',
            'updatedAt' => '2021-02-26 02:15:20',
            'websiteUrl' => 'https://superset.apache.org',
            'repositoryUrl' => 'https://github.com/apache/superset',
            'oldId' => 418,
        ];
        $softwareList[1] = [
            'name' => 'API Platform',
            'slug' => 'api-platform',
            'createdAt' => '2023-06-19 13:31:24',
            'updatedAt' => '2023-06-19 13:31:24',
            'websiteUrl' => 'https://api-platform.com/',
            'repositoryUrl' => 'https://github.com/api-platform/api-platform,api-platform',
            'oldId' => 629,
        ];

        foreach ($softwareList as $key => $softwareData) {
            $softwareList[$key] = $this->addSoftware($softwareData);
        }
        $this->addReference(self::SOFTWARE_0, $softwareList[0]);
        $this->addReference(self::SOFTWARE_1, $softwareList[1]);
    }

    public function addSoftware(array $softwareData): Software
    {
        $createdAt = new \DateTimeImmutable($softwareData['createdAt']);
        $updatedAt = new \DateTimeImmutable($softwareData['updatedAt']);
        $software = new Software();
        $software->setName($softwareData['name']);
        $software->setSlug($softwareData['slug']);
        $software->setCreatedAt(new \DateTimeImmutable($softwareData['createdAt']));
        $software->setUpdatedAt(new \DateTimeImmutable($softwareData['updatedAt']));
        $software->setWebsiteUrl($softwareData['websiteUrl']);
        $software->setRepositoryUrl($softwareData['repositoryUrl']);
        $software->setOldComptoirId($softwareData['oldId']);
        $this->manager->persist($software);
        $this->manager->flush();
        return $software;
    }
}
