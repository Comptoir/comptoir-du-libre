<?php

/*
 * This file is part of the Comptoir-du-Libre license.
 * <https://gitlab.adullact.net/Comptoir/comptoir-du-libre>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Comptoir-du-Libre is free license: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free License Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this license. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\License;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppLicenceFixtures extends Fixture
{
    public const LICENSE_COUNT = 4;
    public const LICENSE_0 = 'agpl-3.0-or-later';
    public const LICENSE_1 = 'agpl-3.0-only';
    public const LICENSE_2 = 'mit';
    public const LICENSE_3 = 'postgresql';

    private ObjectManager $manager ;

    public function load(ObjectManager $manager): void
    {
        $this->manager = $manager;
        $licenseList = [];
        $licenseList[0] = [
            'name' => 'GNU Affero General Public License v3.0 or later',
            'spdx' => 'AGPL-3.0-or-later',
            'slug' => 'agpl-3.0-or-later',
            'isFsfLibre' => true,
            'isOsiApproved' => true,
        ];
        $licenseList[1] = [
            'name' => 'GNU Affero General Public License v3.0 only',
            'spdx' => 'AGPL-3.0-only',
            'slug' => 'agpl-3.0-only',
            'isFsfLibre' => true,
            'isOsiApproved' => true,
        ];
        $licenseList[2] = [
            'name' => 'MIT License',
            'spdx' => 'MIT',
            'slug' => 'mit',
            'isFsfLibre' => true,
            'isOsiApproved' => true,
        ];
        $licenseList[3] = [
            'name' => 'PostgreSQL License',
            'spdx' => 'PostgreSQL',
            'slug' => 'postgresql',
            'isFsfLibre' => false,
            'isOsiApproved' => true,
        ];

        foreach ($licenseList as $key => $licenseData) {
            $licenseList[$key] = $this->addLicense($licenseData);
        }
        $this->addReference(self::LICENSE_0, $licenseList[0]);
        $this->addReference(self::LICENSE_1, $licenseList[1]);
        $this->addReference(self::LICENSE_2, $licenseList[2]);
        $this->addReference(self::LICENSE_3, $licenseList[3]);
    }

    public function addLicense(array $licenseData): License
    {
        $license = new License();
        $license->setName($licenseData['name']);
        $license->setSpdx($licenseData['spdx']);
        $license->setSlug($licenseData['slug']);
        $license->setFsfLibre($licenseData['isFsfLibre']);
        $license->setOsiApproved($licenseData['isOsiApproved']);
        $this->manager->persist($license);
        $this->manager->flush();
        return $license;
    }
}
