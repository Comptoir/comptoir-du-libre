<?php

namespace App\Entity;

use App\Repository\SoftwareRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: SoftwareRepository::class)]
class Software
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'SEQUENCE')]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 125)]
    private ?string $name = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $websiteUrl = null;

    #[ORM\Column(length: 255)]
    private ?string $repositoryUrl = null;

    #[ORM\Column(length: 150)]
    private ?string $slug = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $created_at = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $updated_at = null;

    #[ORM\Column(nullable: true, unique: true)]
    private ?int $old_comptoir_id = null;

    /**
     * @var Collection<int, SoftwareScreenshot>
     */
    #[ORM\OneToMany(targetEntity: SoftwareScreenshot::class, mappedBy: 'software', orphanRemoval: true)]
    private Collection $softwareScreenshots;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $logo = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $old_license_name = null;

    /**
     * @var Collection<int, SoftwareI18n>
     */
    #[ORM\OneToMany(targetEntity: SoftwareI18n::class, mappedBy: 'software', orphanRemoval: true)]
    private Collection $softwareI18ns;

    /**
     * @var Collection<int, Tag>
     */
    #[ORM\ManyToMany(targetEntity: Tag::class, mappedBy: 'Softwares')]
    private Collection $tags;

    public function __construct()
    {
        $this->softwareScreenshots = new ArrayCollection();
        $this->softwareI18ns = new ArrayCollection();
        $this->tags = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getWebsiteUrl(): ?string
    {
        return $this->websiteUrl;
    }

    public function setWebsiteUrl(?string $websiteUrl): static
    {
        $this->websiteUrl = $websiteUrl;

        return $this;
    }

    public function getRepositoryUrl(): ?string
    {
        return $this->repositoryUrl;
    }

    public function setRepositoryUrl(string $repositoryUrl): static
    {
        $this->repositoryUrl = $repositoryUrl;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): static
    {
        $this->slug = $slug;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeImmutable $created_at): static
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeImmutable $updated_at): static
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getOldComptoirId(): ?int
    {
        return $this->old_comptoir_id;
    }

    public function setOldComptoirId(?int $old_comptoir_id): static
    {
        $this->old_comptoir_id = $old_comptoir_id;

        return $this;
    }

    /**
     * @return Collection<int, SoftwareScreenshot>
     */
    public function getSoftwareScreenshots(): Collection
    {
        return $this->softwareScreenshots;
    }

    public function addSoftwareScreenshot(SoftwareScreenshot $softwareScreenshot): static
    {
        if (!$this->softwareScreenshots->contains($softwareScreenshot)) {
            $this->softwareScreenshots->add($softwareScreenshot);
            $softwareScreenshot->setSoftware($this);
        }

        return $this;
    }

    public function removeSoftwareScreenshot(SoftwareScreenshot $softwareScreenshot): static
    {
        if ($this->softwareScreenshots->removeElement($softwareScreenshot)) {
            // set the owning side to null (unless already changed)
            if ($softwareScreenshot->getSoftware() === $this) {
                $softwareScreenshot->setSoftware(null);
            }
        }

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(?string $logo): static
    {
        $this->logo = $logo;

        return $this;
    }

    public function getOldLicenseName(): ?string
    {
        return $this->old_license_name;
    }

    public function setOldLicenseName(?string $old_license_name): static
    {
        $this->old_license_name = $old_license_name;

        return $this;
    }

    /**
     * @return Collection<int, SoftwareI18n>
     */
    public function getSoftwareI18ns(): Collection
    {
        return $this->softwareI18ns;
    }

    public function addSoftwareI18n(SoftwareI18n $softwareI18n): static
    {
        if (!$this->softwareI18ns->contains($softwareI18n)) {
            $this->softwareI18ns->add($softwareI18n);
            $softwareI18n->setSoftware($this);
        }

        return $this;
    }

    public function removeSoftwareI18n(SoftwareI18n $softwareI18n): static
    {
        if ($this->softwareI18ns->removeElement($softwareI18n)) {
            // set the owning side to null (unless already changed)
            if ($softwareI18n->getSoftware() === $this) {
                $softwareI18n->setSoftware(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Tag>
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tag $tag): static
    {
        if (!$this->tags->contains($tag)) {
            $this->tags->add($tag);
            $tag->addSoftware($this);
        }

        return $this;
    }

    public function removeTag(Tag $tag): static
    {
        if ($this->tags->removeElement($tag)) {
            $tag->removeSoftware($this);
        }

        return $this;
    }
}
