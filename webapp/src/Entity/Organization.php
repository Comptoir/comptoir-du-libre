<?php

namespace App\Entity;

use App\Repository\OrganizationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: OrganizationRepository::class)]
class Organization
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'SEQUENCE')]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(nullable: true)]
    private ?int $old_comptoir_id = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $created_at = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $updated_at = null;

    #[ORM\Column(length: 255)]
    private ?string $slug = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $logo = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $website = null;

    #[ORM\ManyToOne(inversedBy: 'organizations')]
    #[ORM\JoinColumn(nullable: false)]
    private ?OrganizationType $type = null;

    /**
     * @var Collection<int, OrganizationI18n>
     */
    #[ORM\OneToMany(targetEntity: OrganizationI18n::class, mappedBy: 'Organization', orphanRemoval: true)]
    private Collection $organizationI18ns;

    public function __construct()
    {
        $this->organizationI18ns = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getOldComptoirId(): ?int
    {
        return $this->old_comptoir_id;
    }

    public function setOldComptoirId(?int $old_comptoir_id): static
    {
        $this->old_comptoir_id = $old_comptoir_id;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeImmutable $created_at): static
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): static
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): static
    {
        $this->slug = $slug;

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(?string $logo): static
    {
        $this->logo = $logo;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): static
    {
        $this->website = $website;

        return $this;
    }

    public function getType(): ?OrganizationType
    {
        return $this->type;
    }

    public function setType(?OrganizationType $type): static
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection<int, OrganizationI18n>
     */
    public function getOrganizationI18ns(): Collection
    {
        return $this->organizationI18ns;
    }

    public function addOrganizationI18n(OrganizationI18n $organizationI18n): static
    {
        if (!$this->organizationI18ns->contains($organizationI18n)) {
            $this->organizationI18ns->add($organizationI18n);
            $organizationI18n->setOrganization($this);
        }

        return $this;
    }

    public function removeOrganizationI18n(OrganizationI18n $organizationI18n): static
    {
        if ($this->organizationI18ns->removeElement($organizationI18n)) {
            // set the owning side to null (unless already changed)
            if ($organizationI18n->getOrganization() === $this) {
                $organizationI18n->setOrganization(null);
            }
        }

        return $this;
    }
}
