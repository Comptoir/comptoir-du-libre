<?php

namespace App\Entity;

use App\Repository\LicenseRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: LicenseRepository::class)]
class License
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'SEQUENCE')]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $shortName = null;

    #[ORM\Column(length: 255)]
    private ?string $spdx = null;

    #[ORM\Column(length: 255)]
    private ?string $slug = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $logo = null;

    #[ORM\Column]
    private ?bool $isOsiApproved = null;

    #[ORM\Column]
    private ?bool $isFsfLibre = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $osiNotes = null;

    /**
     * @var Collection<int, LicenseExternalResources>
     */
    #[ORM\OneToMany(targetEntity: LicenseExternalResources::class, mappedBy: 'license', orphanRemoval: true)]
    private Collection $externalRessources;

    public function __construct()
    {
        $this->externalRessources = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getShortName(): ?string
    {
        return $this->shortName;
    }

    public function setShortName(?string $shortName): static
    {
        $this->shortName = $shortName;

        return $this;
    }

    public function getSpdx(): ?string
    {
        return $this->spdx;
    }

    public function setSpdx(string $spdx): static
    {
        $this->spdx = $spdx;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): static
    {
        $this->slug = $slug;

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(?string $logo): static
    {
        $this->logo = $logo;

        return $this;
    }

    public function isOsiApproved(): ?bool
    {
        return $this->isOsiApproved;
    }

    public function setOsiApproved(bool $isOsiApproved): static
    {
        $this->isOsiApproved = $isOsiApproved;

        return $this;
    }

    public function isFsfLibre(): ?bool
    {
        return $this->isFsfLibre;
    }

    public function setFsfLibre(bool $isFsfLibre): static
    {
        $this->isFsfLibre = $isFsfLibre;

        return $this;
    }

    public function getOsiNotes(): ?string
    {
        return $this->osiNotes;
    }

    public function setOsiNotes(?string $osiNotes): static
    {
        $this->osiNotes = $osiNotes;

        return $this;
    }

    /**
     * @return Collection<int, LicenseExternalResources>
     */
    public function getExternalRessources(): Collection
    {
        return $this->externalRessources;
    }

    public function addExternalRessource(LicenseExternalResources $externalRessource): static
    {
        if (!$this->externalRessources->contains($externalRessource)) {
            $this->externalRessources->add($externalRessource);
            $externalRessource->setLicense($this);
        }

        return $this;
    }

    public function removeExternalRessource(LicenseExternalResources $externalRessource): static
    {
        if ($this->externalRessources->removeElement($externalRessource)) {
            // set the owning side to null (unless already changed)
            if ($externalRessource->getLicense() === $this) {
                $externalRessource->setLicense(null);
            }
        }

        return $this;
    }
}
