<?php

namespace App\Entity;

use App\Repository\TagRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TagRepository::class)]
class Tag
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'SEQUENCE')]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    private ?string $slug = null;

    /**
     * @var Collection<int, Software>
     */
    #[ORM\ManyToMany(targetEntity: Software::class, inversedBy: 'tags')]
    private Collection $Softwares;

    #[ORM\Column(nullable: true)]
    private ?int $old_comptoir_id = null;

    public function __construct()
    {
        $this->Softwares = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): static
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return Collection<int, Software>
     */
    public function getSoftwares(): Collection
    {
        return $this->Softwares;
    }

    public function addSoftware(Software $software): static
    {
        if (!$this->Softwares->contains($software)) {
            $this->Softwares->add($software);
        }

        return $this;
    }

    public function removeSoftware(Software $software): static
    {
        $this->Softwares->removeElement($software);

        return $this;
    }

    public function getOldComptoirId(): ?int
    {
        return $this->old_comptoir_id;
    }

    public function setOldComptoirId(?int $old_comptoir_id): static
    {
        $this->old_comptoir_id = $old_comptoir_id;

        return $this;
    }
}
