<?php

/*
 * This file is part of the Comptoir-du-Libre software.
 * <https://gitlab.adullact.net/Comptoir/comptoir-du-libre>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Comptoir-du-Libre is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Service\Images;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOException;

class PublicDirectory
{
    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __construct(
        private readonly Filesystem $filesystem,
        private readonly ContainerBagInterface $containerBag,
    ) {
        // $this->containerBag->get('app.shortname')
    }


    /**
     * @throws IOException
     */
    public function createPublicDirectory(
        string $pathDirectory,
        bool $clean = false
    ): void {
        $filesystem = $this->filesystem;
//      $filesystem = new Filesystem();
        if ($clean === true && is_dir($pathDirectory)) {
            $filesystem->remove($pathDirectory);
        }

        if (!is_dir($pathDirectory)) {
            $filesystem->mkdir($pathDirectory, 0770);
        }

        if (!is_file("$pathDirectory/index.html")) {
            $filesystem->dumpFile("$pathDirectory/index.html", '');
        }
    }
}
