####### NelmioSecurityBundle #####################################################################################
#   https://symfony.com/bundles/NelmioSecurityBundle/current/index.html
#   https://github.com/nelmio/NelmioSecurityBundle
##################################################################################################################
# Documentation
#   https://developer.mozilla.org/fr/docs/Web/HTTP/CSP
#
# Online tools
#   https://observatory.mozilla.org
#   https://securityheaders.com
##################################################################################################################
nelmio_security:
    # prevents redirections outside the website's domain
    external_redirects:
        abort: true
        log: true

    # prevents inline scripts, unsafe eval, external scripts/images/styles/frames, etc
    csp:
        enabled: true
        report_logger_service: monolog.logger.security
#       report_logger_service: logger
        hosts: []
        content_types: []
        enforce:
            # Provides compatibility with CSP level 1 (old / non-yet-compatible browsers) when using CSP level 2
            # features likes hash and nonce. It adds a 'unsafe-inline' source to a directive whenever a nonce or hash
            # is used.
            # From RFC: " If 'unsafe-inline' is not in the list of allowed style sources, or if at least one
            #             nonce-source or hash-source is present in the list of allowed style sources "
            # See https://www.w3.org/TR/CSP2/#directive-style-src and https://www.w3.org/TR/CSP2/#directive-script-src
            level1_fallback: false
            # only send directives supported by the browser, defaults to false
            # this is a port of https://github.com/twitter/secureheaders/blob/83a564a235c8be1a8a3901373dbc769da32f6ed7/lib/secure_headers/headers/policy_management.rb#L97
            browser_adaptive:
                enabled: false
            report-uri: '%router.request_context.base_url%/csp/report'
#           block-all-mixed-content: true # defaults to false, blocks HTTP content over HTTPS transport
#           upgrade-insecure-requests: true # defaults to false, upgrades HTTP requests to HTTPS transport


    # prevents framing of the entire site
    clickjacking:
        paths:
            '^/.*': DENY
        content_types: []  # TODO via Tajine --> check documentation
        hosts: []          # TODO via Tajine --> check documentation

    # disables content type sniffing for script resources
    content_type:
        nosniff: true

    # forces Microsoft's XSS-Protection with its block mode
#   xss_protection: # Deprecated since nelmio/security-bundle 3.4.0
#       enabled: true
#       mode_block: true
#       report_uri: '%router.request_context.base_url%/nelmio/xss/report'

    # Send a full URL in the `Referer` header when performing a same-origin request,
    # only send the origin of the document to secure destination (HTTPS->HTTPS),
    # and send no header to a less secure destination (HTTPS->HTTP).
    # If `strict-origin-when-cross-origin` is not supported, use `no-referrer` policy,
    # no referrer information is sent along with requests.
    referrer_policy:
        enabled: true
        policies:
            - 'no-referrer'
            - 'strict-origin-when-cross-origin'

#     # signs/verifies all cookies
#        names: ['*']
#        secret: this_is_very_secret # defaults to global %secret% parameter
#        hash_algo: sha256 # defaults to sha256, see ``hash_algos()`` for available algorithms

    # forces HTTPS handling, don't combine with flexible mode
    # and make sure you have SSL working on your site before enabling this
    forced_ssl:
        enabled: false
#       hsts_max_age: 2592000 # 30 days
        hsts_max_age: 31536000 # 1 year
        hsts_subdomains: false
#       hsts_preload: true
        redirect_status_code: 302 # default, switch to 301 for permanent redirects

when@prod:
    nelmio_security:
        forced_ssl:
            enabled: true
        csp:
            enforce:
                default-src: [ 'none' ]
                style-src: ['self']
                script-src:  ['%env(trim:WEBAPP_STAT_MATOMO_HOST)%']
                img-src:  ['self', data:, '%env(trim:WEBAPP_STAT_MATOMO_HOST)%']
                connect-src:  ['%env(trim:WEBAPP_STAT_MATOMO_HOST)%']
                form-action: ['self']
                base-uri: ['self']
                frame-src: ['none']
                frame-ancestors: ['none']

when@dev:
    nelmio_security:
        forced_ssl:
            enabled: false
        csp:
            enforce:
                default-src: [ 'self' ]
#               style-src: ['self', 'unsafe-eval', 'unsafe-inline']
                style-src: ['self', 'unsafe-inline']
                script-src:  ['self',  '%env(trim:WEBAPP_STAT_MATOMO_HOST)%']
                img-src:  ['self', data:, '%env(trim:WEBAPP_STAT_MATOMO_HOST)%']
                connect-src:  ['self', '%env(trim:WEBAPP_STAT_MATOMO_HOST)%']
                form-action: ['self']
                base-uri: ['self']
                frame-src: ['none']
                frame-ancestors: ['none']

when@test:
    nelmio_security:
        forced_ssl:
            enabled: false
